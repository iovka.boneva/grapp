/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises

import fr.ulille.grapp.graph.Graph
import fr.ulille.grapp.graph.Graphs
import fr.ulille.grapp.graph.GrappType
import java.io.File

class AssignmentExercise {

    /** The array must be a square. First column and first line contain the vertices' names. */
    fun bipartiteGraphFromArray (array: List<Array<String>>) : Graph {
        val size = array.size
        val lineNodes = mutableListOf<String>("")
        val columnNodes = mutableListOf<String>("")
        for (i in 1 until size) {
            columnNodes.add(array[0][i])
            lineNodes.add(array[i][0])
        }
        val graph = Graphs.graphFromNodeSet(GrappType.uwgraph,
            lineNodes.union(columnNodes).minus(""))
        for (line in 1 until size)
            for (column in 1 until size)
                Graphs.addEdge(graph, lineNodes[line], columnNodes[column], array[line][column].toDouble())

        return graph
    }

    /** Assumes that the number of lines is equal to the number of columns */
    fun parseCsvTextToArray (csvLines : List<String>) : List<Array<String>> {
        TODO()
    }
}

fun main () {
    val graph = AssignmentExercise().bipartiteGraphFromArray(listOf(
        arrayOf("", "X5", "Y6", "Z7"),
        arrayOf("A1", "15", "16", "17"),
        arrayOf("B2", "25", "26", "27"),
        arrayOf("C3", "35", "36", "37")))
    println(graph)
}