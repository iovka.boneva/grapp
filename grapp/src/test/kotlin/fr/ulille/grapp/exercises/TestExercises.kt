/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises

import org.junit.Test

class TestExercises {

    //@Test
    fun test () {
        val outputFolderName = "c:/Users/iovka/Downloads/tmp/"
        // DijkstraExercise(outputFolderName, "g", true).generate()
        // PertExercises(outputFolderName, "exo").generate(5)
        MaximumFlowExercise(outputFolderName, "flow").generateAugmentFlowQuestion(1)
    }

    //@Test
    fun testPertProblemGenerator () {
        val problem = PertProblemGenerator.generate(listOf("a", "b", "c", "d", "e", "f", "g"))
        println(problem.entries.joinToString(separator="\n") { "${it.key} -> ${it.value}" })
    }

    @Test
    fun testGrade() {
        val p = PertExerciseGrading("C:/Users/iovka/Documents/Documents/Teaching/Graphes/2023/interro-potentiel-taches/fichiers/",
            "C:/Users/iovka/Documents/Documents/Teaching/Graphes/2022/interro-potentiel-taches/correction/",
            1.0, 2.0)
        p.grade()
    }

}