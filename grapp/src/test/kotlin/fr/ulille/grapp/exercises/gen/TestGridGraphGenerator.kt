/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises.gen

import fr.ulille.grapp.exercises.GridGraphGenerator
import fr.ulille.grapp.graph.GrappType
import kotlin.test.Test
import kotlin.test.assertEquals

class TestGridGraphGenerator {

    @Test
    fun testGridGraphGen () {
        val gen3x4 = GridGraphGenerator(3, 4, setOf())
        val graph = gen3x4.generateGraph("g",20, GrappType.uwgraph)

        assertEquals(setOf("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"), graph.vertexSet())
        assertEquals(20, graph.edgeSet().size)
        assertEquals((1..20).map { it.toDouble() }.toSet(), graph.edgeSet().map{ it.weight }.toSet())
    }
}