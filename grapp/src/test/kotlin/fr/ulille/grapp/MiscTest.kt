/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp

import org.jgrapht.graph.*
import java.lang.UnsupportedOperationException
import kotlin.test.Test
import kotlin.test.assertEquals


class MiscTest {

    @Test
    fun testJgraphEdgeType() {
        val g = DefaultDirectedWeightedGraph<String, Any>(Any::class.java)

        g.addVertex("a")
        g.addVertex("b")
        g.addEdge("a", "b")
        g.setEdgeWeight("a", "b", 1.0)

        assertEquals(1.0, g.getEdgeWeight(g.getEdge("a", "b")), 0.0)
    }

    @Test(expected = UnsupportedOperationException::class)
    fun testSetWeightOnUnweightedGraph() {
        val g = DefaultDirectedGraph<String, Any>(Any::class.java)

        g.addVertex("a")
        g.addVertex("b")
        g.addEdge("a", "b")
        g.setEdgeWeight("a", "b", 1.0)
    }
}
