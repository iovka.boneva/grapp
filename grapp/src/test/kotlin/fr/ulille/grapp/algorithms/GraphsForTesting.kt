/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.io.YamlParser

object GraphsForTesting {

    val grapheNonOrienteNonValueYaml =
        """
            type: ugraph
            name: G_u
            nodes: [a, b, c, d, e, f, g]
            edges:
              a: [b, c, d]
              b: [c, d, e]
              c: [e, g]
              d: [f]
              e: [f]
              f: []
              g: []
        """.trimIndent()
    val grapheNonOrienteNonValue = YamlParser.parse(grapheNonOrienteNonValueYaml.byteInputStream())

    val grapheNonOrienteValueYaml =
        """
            type: uwgraph
            name: G_uw
            nodes: [a, b, c, d, e, f]
            wedges:
              a: {b: 8, c: 4}
              b: {c: 2}
              c: {d: 1}
              d: {e: 7, b: 3, f: 3}
              e: {c: 1}
              f: {e: 2}
        """.trimIndent()
    val grapheNonOrienteValue = YamlParser.parse(grapheNonOrienteValueYaml.byteInputStream())

    val grapheNonOrienteValue2Yaml =
        """
            type: uwgraph
            name: Guwgraph
            nodes: [a, b, c, d]
            wedges:
              a: {b: 8, c: 4}
              b: {c: 2}
              c: {d: 1}
              d: {b: 3}
        """.trimIndent()
    val grapheNonOrienteValue2 = YamlParser.parse(grapheNonOrienteValue2Yaml.byteInputStream())



    val grapheOrientePoidsPositifsYaml =
        """
            type: diwgraph
            name: G_diw_pos
            nodes: [a, b, c, d, e, f]
            wedges:
              a: {b: 8, c: 4}
              b: {c: 2}
              c: {d: 1}
              d: {e: 7, b: 2, f: 3}
              e: {c: 1}
              f: {e: 2}
        """.trimIndent()
    val grapheOrientePoidsPositifs = YamlParser.parse(grapheOrientePoidsPositifsYaml.byteInputStream())

    val graphOrienteSansCyclesYaml =
        """
            type: diwgraph
            name: G_diw_acyclic
            nodes: [a, b, c, d, e, f]
            wedges:
              a: {b: 8, c: 4}
              b: {c: 2, d: 2}
              c: {d: -1, e: 1}
              d: {f: 3}
              e: {d: 7, f: 2}
              f: {}
        """.trimIndent()
    val grapheOrienteSansCycles = YamlParser.parse(graphOrienteSansCyclesYaml.byteInputStream())

    val graphOrienteSansCyclesPoidsPositifsYaml =
        """
            type: diwgraph
            name: G_diw_acyclic
            nodes: [a, b, c, d, e, f]
            wedges:
              a: {b: 8, c: 4}
              b: {c: 2, d: 2}
              c: {d: 1, e: 1}
              d: {f: 3}
              e: {d: 7, f: 0}
              f: {}
        """.trimIndent()
    val grapheOrienteSansCyclesPoidsPositifs = YamlParser.parse(graphOrienteSansCyclesPoidsPositifsYaml.byteInputStream())

    val grapheOrienteQuelconqueYaml =
        """
            type: diwgraph
            name: G_diw
            nodes: [a, b, c, d, e, f]
            wedges:
              a: {b: 8, c: 4}
              b: {c: -2}
              c: {d: 1}
              d: {b: 2, e: 7, f: 3}
              e: {c: -1}
              f: {e: -2}
        """.trimIndent()
    val grapheOrienteQuelconque = YamlParser.parse(grapheOrienteQuelconqueYaml.byteInputStream())

    val grapheOrienteCycleNegatifYaml =
        """
            type: diwgraph
            name: G_diw_negcycle
            nodes: [a, b, c, d, e, f]
            wedges:
              a: {b: 8, c: 4}
              b: {c: -2}
              c: {d: -1}
              d: {b: 2, e: 7, f: 3}
              e: {c: -1}
              f: {e: -2}
        """.trimIndent()
    val grapheOrienteCycleNegatif = YamlParser.parse(grapheOrienteCycleNegatifYaml.byteInputStream())

    val grapheOrienteValueNonConnecteYaml =
        """
            type: diwgraph
            name: G_diw_nconnected
            nodes: [a, b, c, d]
            wedges:
              a: {b: 8}
              b: {}
              c: {d: 1}
              d: {}
        """.trimIndent()
    val grapheOrienteValueNonConnecte = YamlParser.parse(grapheOrienteValueNonConnecteYaml.byteInputStream())


    val grapheSimple4SommetsYaml =
        """
            type: diwgraph
            name: G_diw_4
            nodes: [o, p, a, b]
            wedges:
              o: {a: 5, b: 4}
              p: {}
              a: {p: 5, b: 2}
              b: {p: 3}
        """.trimIndent()
    val grapheSimple4sommets = YamlParser.parse(grapheSimple4SommetsYaml.byteInputStream())

    val grapheFlotSujetTpYaml =
        """
            type: diwgraph
            name: G_diw_flow
            nodes: [o, p, a, b, c]
            wedges:
              o: {b: 20, a: 13}
              p: {}
              a: {b: 11, p: 2, c: 10}
              b: {p: 11}
              c: {p: 9}
        """.trimIndent()
    val grapheFlotSujetTp = YamlParser.parse(grapheFlotSujetTpYaml.byteInputStream())

    val grapheFlotIncorrectNoSourceYaml =
        """
            type: diwgraph
            name: G_diw_floterreur1
            nodes: [o, p, a, b, c]
            wedges:
              o: {b: 20, a: 13}
              p: {}
              a: {b: 11, p: 2, c: 10}
              b: {p: 11}
              c: {p: 9, o: 9}
        """.trimIndent()
    val grapheFlotIncorrectNoSource = YamlParser.parse(grapheFlotIncorrectNoSourceYaml.byteInputStream())

    val grapheFlotIncorrectCapaciteNegativeYaml =
        """
            type: diwgraph
            name: G_diw_floterreur2
            nodes: [o, p, a, b, c]
            wedges:
              o: {b: 20, a: -13}
              p: {}
              a: {b: 11, p: 2, c: 10}
              b: {p: 11}
              c: {p: 9}
        """.trimIndent()
    val grapheFlotIncorrectCapaciteNegative =
        YamlParser.parse(grapheFlotIncorrectCapaciteNegativeYaml.byteInputStream())


    val grapheG1Yaml =
        """
            type: ugraph
            name: G1
            nodes: [a]
            edges: {a: []}
        """.trimIndent()
    val grapheG1 = YamlParser.parse(grapheG1Yaml.byteInputStream())

    val grapheG1bisYaml =
        """
            type: ugraph
            name: G1
            nodes: [b]
            edges: {b: []}
        """.trimIndent()
    val grapheG1bis = YamlParser.parse(grapheG1bisYaml.byteInputStream())

    val grapheBugDijkstraYaml =
        """
            name: g
            type: uwgraph
            nodes: [a, b, d, e, f, h, i]
            wedges:
              a: {d: 2}
              b: {a: 8, e: 4}
              d: {e: 7, b: 5}
              e: {i: 3, h: 9}
              f: {e: 10, i: 6}
              h: {}
              i: {h: 1}
        """.trimIndent()
    val grapheBugDijkstra = YamlParser.parse(grapheBugDijkstraYaml.byteInputStream())
}