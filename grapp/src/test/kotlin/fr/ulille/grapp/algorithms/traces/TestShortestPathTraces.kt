/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.traces

import de.m3y.kformat.Table
import de.m3y.kformat.table
import fr.ulille.grapp.algorithms.GraphsForTesting
import fr.ulille.grapp.io.TraceTableIO
import org.junit.Test

class TestShortestPathTraces {

    @Test
    fun testDijkstra () {
        val (et, _, _) = DijkstraTraces.distSlashPredTrace(
            GraphsForTesting.grapheNonOrienteValue,
            "a"
        )
        //println(TraceTableIO.toTableString(et))
    }

}