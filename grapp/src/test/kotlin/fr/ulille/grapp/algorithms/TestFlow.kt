/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.graph.Edge
import fr.ulille.grapp.graph.Vertex
import org.junit.Assert.*
import org.junit.Test
import kotlin.test.assertFailsWith

class TestFlow {

    @Test
    fun testEdmondsKarp() {
        val algorithm = EdmondsKarpAlgorithm(GraphsForTesting.grapheFlotSujetTp)
        val flow = mutableMapOf<Edge, Double>()
        val sourcePart = mutableSetOf<Vertex>()
        val sinkPart = mutableSetOf<Vertex>()
        algorithm.compute("o", "p", flow, sourcePart, sinkPart)

        assertEquals(11.0, flow[GraphsForTesting.grapheFlotSujetTp.getEdge("o", "b")])
        assertEquals(11.0, flow[GraphsForTesting.grapheFlotSujetTp.getEdge("o", "a")])
        assertEquals(0.0, flow[GraphsForTesting.grapheFlotSujetTp.getEdge("a", "b")])
        assertEquals(11.0, flow[GraphsForTesting.grapheFlotSujetTp.getEdge("b", "p")])
        assertEquals(2.0, flow[GraphsForTesting.grapheFlotSujetTp.getEdge("a", "p")])
        assertEquals(9.0, flow[GraphsForTesting.grapheFlotSujetTp.getEdge("c", "p")])
        assertEquals(9.0, flow[GraphsForTesting.grapheFlotSujetTp.getEdge("a", "c")])

        assertEquals(setOf("o", "b", "a", "c"), sourcePart)
        assertEquals(setOf("p"), sinkPart)
    }
//
//    @Test(expected=IllegalArgumentException::class)
//    fun testFlowReseauDeTransportIncorrectSourceIncorrecte() {
//        val graphe = TestGraphs.grapheFlotIncorrectNoSource
//        val algorithm = EdmondsKarpAlgorithm(graphe)
//        val flow = mutableMapOf<Edge, Double>()
//        val sourcePart = mutableSetOf<Vertex>()
//        val sinkPart = mutableSetOf<Vertex>()
//
//        algorithm.compute("o", "p", flow, sourcePart, sinkPart)
//        println(flow)
//    }

    @Test
    fun testFlowReseauDeTransportIncorrectCapaciteNegative() {
        val graphe = GraphsForTesting.grapheFlotIncorrectCapaciteNegative
        assertFailsWith<IllegalArgumentException> {
            EdmondsKarpAlgorithm(graphe)
        }
    }

//    @Test(expected=IllegalArgumentException::class)
//    fun testFlowReseauDeTransportIncorrectGrapheNonOriente() {
//        val graphe = TestGraphs.grapheNonOrienteValue
//        EdmondsKarpAlgorithm(graphe)
//    }


}