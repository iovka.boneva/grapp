/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.graph.Vertex
import org.junit.Assert
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TestShortestPath {

    @Test
    fun testDijkstra () {
        val dijkstra = Dijkstra(GraphsForTesting.grapheOrientePoidsPositifs);
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        dijkstra.compute("a", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("a" to 0.0, "b" to 7.0, "c" to 4.0, "d" to 5.0, "e" to 10.0, "f" to 8.0)
        val predAttendu = mutableMapOf("b" to "d", "c" to "a", "d" to "c", "e" to "f", "f" to "d")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testDijkstra2 () {
        val dijkstra = Dijkstra(GraphsForTesting.grapheOrientePoidsPositifs);
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        dijkstra.compute("c", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("a" to Double.POSITIVE_INFINITY, "b" to 3.0, "c" to 0.0, "d" to 1.0, "e" to 6.0, "f" to 4.0)
        val predAttendu = mutableMapOf("d" to "c", "b" to "d", "e" to "f", "f" to "d")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun netsDijkstra3 () {
        val dijkstra = Dijkstra(GraphsForTesting.grapheOrienteValueNonConnecte);
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        dijkstra.compute("a", dist=dist, pred=pred)

        val distAttendu = mutableMapOf("a" to 0.0, "b" to 8.0, "c" to Double.POSITIVE_INFINITY, "d" to Double.POSITIVE_INFINITY)
        val predAttendu = mutableMapOf("b" to "a")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testDijkstraGrapheNonOriente () {
        val dijkstra = Dijkstra(GraphsForTesting.grapheNonOrienteValue)
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        dijkstra.compute("f", dist=dist, pred=pred)

        val distAttendu = mutableMapOf("a" to 7.0, "b" to 5.0, "c" to 3.0, "d" to 3.0, "e" to 2.0, "f" to 0.0)
        val predAttendu = mutableMapOf("e" to "f", "d" to "f", "c" to "e", "b" to "c", "a" to "c")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testDijkstraWrongGraph () {
        val dijkstra = Dijkstra(GraphsForTesting.grapheOrienteSansCycles)
        assertFailsWith<AlgorithmError> {
            dijkstra.compute("a", mutableMapOf(), mutableMapOf())
        }
    }

    @Test
    fun testBellmanWrongGraph () {
        assertFailsWith<AlgorithmError> {
            Bellman(GraphsForTesting.grapheOrientePoidsPositifs)
        }
    }

    @Test
    fun testBellmanShortestPath () {
        val bellman = Bellman(GraphsForTesting.grapheOrienteSansCycles)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        bellman.run("a", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("a" to 0.0, "b" to 8.0, "c" to 4.0, "d" to 3.0, "e" to 5.0, "f" to 6.0)
        val predAttendu = mutableMapOf("b" to "a", "c" to "a", "d" to "c", "e" to "c", "f" to "d")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testBellmanLongestPath () {
        val bellman = Bellman(GraphsForTesting.grapheOrienteSansCycles, optimalityCriterion = OptimalityCriterion.Max)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        bellman.run("a", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("a" to 0.0, "b" to 8.0, "c" to 10.0, "d" to 18.0, "e" to 11.0, "f" to 21.0)
        val predAttendu = mutableMapOf("b" to "a", "c" to "b", "d" to "e", "e" to "c", "f" to "d")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testBellmanLongestPathMultiplication () {
        val bellman = Bellman(GraphsForTesting.grapheOrienteSansCyclesPoidsPositifs,
            pathCombinationOperation = PathCombinationOperation.Mult, optimalityCriterion = OptimalityCriterion.Max)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        bellman.run("a", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("a" to 1.0, "b" to 8.0, "c" to 16.0, "d" to 112.0, "e" to 16.0, "f" to 336.0)
        val predAttendu = mutableMapOf("b" to "a", "c" to "b", "d" to "e", "e" to "c", "f" to "d")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testBellmanShortestPathMultiplication () {
        val bellman = Bellman(GraphsForTesting.grapheOrienteSansCyclesPoidsPositifs,
            pathCombinationOperation = PathCombinationOperation.Mult, optimalityCriterion = OptimalityCriterion.Min)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        bellman.run("a", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("a" to 1.0, "b" to 8.0, "c" to 4.0, "d" to 4.0, "e" to 4.0, "f" to 0.0)
        val predAttendu = mutableMapOf("b" to "a", "c" to "a", "d" to "c", "e" to "c", "f" to "e")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testBellmanLongestPathMultiplicationNegativeWeight () {
        val bellman = Bellman(GraphsForTesting.grapheOrienteSansCycles,
            pathCombinationOperation = PathCombinationOperation.Mult, optimalityCriterion = OptimalityCriterion.Min)
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        assertFailsWith<AlgorithmError> {
            bellman.run("a", dist = dist, pred = pred)
        }
    }

    @Test
    fun testBellmanFord () {
        val bellmanFord = BellmanFord(GraphsForTesting.grapheOrienteQuelconque)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        bellmanFord.run("a", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("a" to 0.0, "b" to 7.0, "c" to 4.0, "d" to 5.0, "e" to 6.0, "f" to 8.0)
        val predAttendu = mutableMapOf("b" to "d", "c" to "a", "d" to "c", "e" to "f", "f" to "d")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testBellmanFordGrapheNonOriente () {
        val bellmanFord = BellmanFord(GraphsForTesting.grapheNonOrienteValue)
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        bellmanFord.run("f", dist=dist, pred=pred)

        val distAttendu = mutableMapOf("a" to 7.0, "b" to 5.0, "c" to 3.0, "d" to 3.0, "e" to 2.0, "f" to 0.0)
        val predAttendu = mutableMapOf("e" to "f", "d" to "f", "c" to "e", "b" to "c", "a" to "c")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testBellmanFordGrapheNonOriente2 () {
        val bellmanFord = BellmanFord(GraphsForTesting.grapheNonOrienteValue2)
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        bellmanFord.run("a", dist=dist, pred=pred)

        val distAttendu = mutableMapOf("a" to 0.0, "b" to 6.0, "c" to 4.0, "d" to 5.0)
        val predAttendu = mutableMapOf("d" to "c", "c" to "a", "b" to "c")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testBellmanFordWrongGraph() {
        val bellmanFord = BellmanFord(GraphsForTesting.grapheOrienteCycleNegatif)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        assertFailsWith<AlgorithmError> {
            bellmanFord.run("a", dist = dist, pred = pred)
        }
    }

    @Test
    fun testDijkstraBug () {
        val dijkstra = Dijkstra(GraphsForTesting.grapheBugDijkstra)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        dijkstra.run("a", dist, pred)

        val distAttendu = mapOf("a" to 0.0, "b" to 7.0, "d" to 2.0, "e" to 9.0, "f" to 18.0, "h" to 13.0, "i" to 12.0)
        assertEquals(distAttendu, dist)
    }

}