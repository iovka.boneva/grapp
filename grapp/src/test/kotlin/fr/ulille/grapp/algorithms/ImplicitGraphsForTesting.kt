/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.graph.ImplicitGraph

object grapheImpliciteSommetInitialA : ImplicitGraph {

    override fun type(): String = "diwgraph"
    override fun name(): String = "graphe implicite pour tests"
    override fun initialNodes(): List<String> = listOf("a")

    override fun successors(node: String?): List<String> {
        return when (node) {
            "a" -> listOf("b:8")
            "b" -> listOf("c:2")
            else -> listOf()
        }
    }

}

object grapheImplicitePoidsNegatifs : ImplicitGraph {

    override fun type(): String = "diwgraph"
    override fun name(): String = "graphe implicite pour tests"
    override fun initialNodes(): List<String> = listOf("a")

    override fun successors(node: String?): List<String> {
        return when (node) {
            "a" -> listOf("b:8", "c:4")
            "b" -> listOf("c:-2")
            "c" -> listOf("d:1")
            "d" -> listOf("b:2", "e:7", "f:3")
            "e" -> listOf("c:-1")
            "f" -> listOf("e:2")
            else -> listOf()
        }
    }
}


object grapheImplicitePoidsPositifs : ImplicitGraph {

    override fun type(): String = "diwgraph"
    override fun name(): String = "graphe implicite pour tests"
    override fun initialNodes(): List<String> = listOf("c")

    override fun successors(node: String?): List<String> {
        return when (node) {
            "a" -> listOf("b:8", "c:4")
            "b" -> listOf("c:2")
            "c" -> listOf("d:1")
            "d" -> listOf("b:2", "e:7", "f:3")
            "e" -> listOf("c:1")
            "f" -> listOf("e:2")
            else -> listOf()
        }
    }
}

object grapheInvestissement : ImplicitGraph {
    override fun type(): String = "diwgraph"
    override fun name(): String = "programmation dynamique investissement"
    override fun initialNodes(): List<String> = listOf("A0")

    override fun successors(node: String): List<String> {
        val region = node[0]
        val somme = (node[1] - '0') as Int
        return if (region == 'A' || region == 'B' || region == 'C')
            successeurs(region, somme). map { "${(region + 1)}${it.first}:${it.second}"}
        else
            listOf()
    }

    val benefices = mutableMapOf<Char, Map<Int, Int>>()
    init {
        benefices['A'] = mapOf(0 to 0, 1 to 2, 2 to 5, 3 to 9, 4 to 10)
        benefices['B'] = mapOf(0 to 0, 1 to 1, 2 to 2, 3 to 8, 4 to 10)
        benefices['C'] = mapOf(0 to 0, 1 to 4, 2 to 5, 3 to 6, 4 to 7)
    }

    // Retourne les paires (nouvelle somme investie, gain)
    fun successeurs (region : Char, sommeDejaInvestie: Int) : List<Pair<Int, Int>> {
        return (0 .. 4).map { Pair(sommeDejaInvestie + it, benefices[region]!![it]!! ) }. filter { it.first <= 6}. toList()
    }
}

object grapheLCK : ImplicitGraph {
    const val BERGER = 0
    const val LOUP = 1
    const val CHEVRE = 2
    const val CHOU = 3

    override fun type(): String = "digraph"
    override fun name(): String = "Probleme loup chevre chou"
    override fun initialNodes() = listOf("GGGG")

    override fun successors(vertex: String): List<String> {
        val coteBerger = vertex[BERGER]
        val coteOppose = if (coteBerger == 'G') 'D' else 'G'
        return successors(vertex, coteBerger, coteOppose)
    }

    // de,vers valent 'G','D' ou bien 'D','G', et de est le côté du berger
    fun successors(vertex: String, de: Char, vers: Char) : List<String> {
        val resultat = mutableListOf<String>()
        // berger seul
        val bergerSeul = "$vers${vertex.substring(1)}"
        resultat.add(bergerSeul)
        for (i in 1 .. 3)
            if (vertex[i]==de)
                resultat.add(bergerSeul.substring(0,i) + vers + bergerSeul.substring(i+1))
        return resultat.filter{ !estInterdit(it) }
    }

    private fun estInterdit (sommet: String) : Boolean {
        val berger = sommet[BERGER]
        val loup = sommet[LOUP]
        val chevre = sommet[CHEVRE]
        val chou = sommet[CHOU]

        return loup=='G' && chevre=='G' && berger=='D'
            || loup=='D' && chevre=='D' && berger=='G'
            || chevre=='G' && chou=='G' && berger=='D'
            || chevre=='D' && chou=='D' && berger=='G'
    }
}

object grapheCavaliers : ImplicitGraph {
    val deplacements = mapOf(
        '0' to setOf('3','6'),
        '1' to setOf('7','9'),
        '2' to setOf('8'),
        '3' to setOf('0','5','9'),
        '4' to setOf('6'),
        '5' to setOf('3'),
        '6' to setOf('0','4'),
        '7' to setOf('1','8'),
        '8' to setOf('2','7'),
        '9' to setOf('1','3')
    )

    override fun type(): String = "digraph"
    override fun name(): String = "Probleme_des_cavaliers"
    override fun initialNodes() = listOf("0546")

    override fun successors(vertex: String): List<String> {
        val resultat = mutableListOf<String>()
        for (i in 0 .. 3) {
            for (d in deplacements[vertex[i]]!!) {
                val v = "${vertex.substring(0,i)}$d${vertex.substring(i+1)}"
                if (!estInterdit(v))
                    resultat.add(v)
            }
        }
        return resultat
    }

    private fun estInterdit(vertex: String) : Boolean {
        val positions = mutableSetOf<Char>()
        vertex.toCollection(positions)
        return positions.size != 4
    }
}
