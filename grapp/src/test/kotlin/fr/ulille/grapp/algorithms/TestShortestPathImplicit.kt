/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.graph.ImplicitGraphInterface
import fr.ulille.grapp.graph.ImplicitGraphsUtil
import fr.ulille.grapp.graph.Vertex
import org.junit.Assert
import org.junit.Test
import kotlin.test.assertEquals

class TestShortestPathImplicit {

    @Test
    fun testInvestissement () {
        val graph = ImplicitGraphsUtil.getGraph(grapheInvestissement)
        (graph as ImplicitGraphInterface).explore(graph)
        val dijkstra = Dijkstra(graph)

        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        dijkstra.compute("A0", dist = dist, pred = pred)
        assertEquals(9.0, dist["D6"])
    }

    @Test
    fun testDijkstra () {
        val graph = ImplicitGraphsUtil.getGraph(grapheImplicitePoidsPositifs)
        (graph as ImplicitGraphInterface).explore(graph)
        val dijkstra = Dijkstra(graph)
        val dist = mutableMapOf<Vertex,Double>()
        val pred = mutableMapOf<Vertex,Vertex>()
        dijkstra.compute("c", dist = dist, pred = pred)

        val distAttendu = mutableMapOf("b" to 3.0, "c" to 0.0, "d" to 1.0, "e" to 6.0, "f" to 4.0)
        val predAttendu = mutableMapOf("d" to "c", "b" to "d", "e" to "f", "f" to "d")

        Assert.assertEquals(distAttendu, dist)
        Assert.assertEquals(predAttendu, pred)
    }

    @Test
    fun testDijkstraFromNonInitialNode () {
        val graph = ImplicitGraphsUtil.getGraph(grapheImpliciteSommetInitialA)
        (graph as ImplicitGraphInterface).explore(graph)
        val dijkstra = Dijkstra(graph)
        dijkstra.compute("b", mutableMapOf(), mutableMapOf())
    }
}



