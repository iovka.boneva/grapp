/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.graph.ImplicitGraphsUtil
import fr.ulille.grapp.io.LatexSerializer
import fr.ulille.grapp.io.YamlSerializer
import fr.ulille.grapp.ui.AlgorithmsUtil
import fr.ulille.grapp.ui.GenYaml
import fr.ulille.grapp.ui.Messages
import fr.ulille.grapp.ui.TraversalAlgorithmName
import org.junit.Test
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse

class TestTraversalImplicit {

    @Test
    fun testTraversalFromNonInitialNode () {
        val bfs = BreadthFirstSearch(ImplicitGraphsUtil.getGraph(grapheImpliciteSommetInitialA))
        assertFailsWith<AlgorithmError> {
            bfs.compute("b", mutableMapOf())
        }
    }


    @Test
    fun testLoupChevreChou() {
        val result = AlgorithmsUtil.runGraphTraversalAlgorithm(TraversalAlgorithmName.BFS,
            ImplicitGraphsUtil.getGraph(grapheLCK), "GGGG")
        val o = StringBuilder()
        YamlSerializer.serialize(result.graph, o)
        File("lcc.yaml").writeText(o.toString())

        val tree = result.tree

        val path = mutableListOf<String>()
        var v = "DDDD"
        path.add(v)
        while (v != "GGGG") {
            v = tree.incomingEdgesOf(v).first()!!.src
            path.add(v)
        }
        path.reverse()
        assertEquals(8, path.size)
        assert(path.contains("DDDG"))
        assertFalse(path.contains("DGGG"))
    }

    @Test
    fun testCavaliers () {
        val result = AlgorithmsUtil.runGraphTraversalAlgorithm(TraversalAlgorithmName.BFS,
            ImplicitGraphsUtil.getGraph(grapheCavaliers), "0546")
        val tree = result.tree

        val path1 = mutableListOf<String>()
        var v = "4605"
        path1.add(v)
        while (v != "0546") {
            v = tree.incomingEdgesOf(v).first()!!.src
            path1.add(v)
        }

        val path2 = mutableListOf<String>()
        v = "6405"
        path2.add(v)
        while (v != "0546") {
            v = tree.incomingEdgesOf(v).first()!!.src
            path2.add(v)
        }

        val path = if (path1.size < path2.size) path1 else path2

        path.reverse()
        assertEquals(41,path.size)
        assertEquals("0546", path[0])

        /*
        println("${path.size} moves")
        for (i in 0 until path.size-1) {
            val current = path[i]
            val next = path[i+1]
            print("from $current : ")
            for (j in 0 .. 3)
                if (current[j] != next[j]) {
                    val color = if (j <= 1) "white" else "black"
                    println("move $color from ${current[j]} to ${next[j]}")
                }
        }
        */
    }
}


