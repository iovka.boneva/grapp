/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.graph.Vertex
import org.junit.Test
import kotlin.test.assertEquals

class TestTraversal {

    @Test
    fun testParcoursLargeur () {
        val bfs = BreadthFirstSearch(GraphsForTesting.grapheNonOrienteNonValue)
        val predecessorsMap = mutableMapOf<Vertex, Vertex>()
        bfs.compute("a", predecessorsMap)
        val attendu = mapOf("b" to "a", "c" to "a", "d" to "a", "e" to "b", "g" to "c", "f" to "d")
        assertEquals(attendu, predecessorsMap)
    }

    @Test
    fun testParcoursProfondeur () {
        val dfs = DepthFirstSearch(GraphsForTesting.grapheNonOrienteNonValue);
        val predecessorsMap = mutableMapOf<Vertex, Vertex>()
        dfs.compute("a", predecessorsMap)

        val attendu = mapOf("b" to "a", "c" to "b", "e" to "c", "f" to "e", "d" to "f", "g" to "c")
        assertEquals(attendu, predecessorsMap)
    }

}