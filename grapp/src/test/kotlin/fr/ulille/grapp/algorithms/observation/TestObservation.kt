/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.observation

import org.junit.Test
import java.lang.Exception
import org.junit.Assert.*

class TestObservation {

    @Test
    fun testObservableObservedCorrectUsage () {
        val observer = ToyObserver()
        val observable = ToyObservable()
        observable.initObservation(observable, observer, "some name")

        // the observable is correctly associated with the observer
        assertEquals(observable, observer.getObserved("some name"))
        assertEquals(observer, observable.__obs.observer)

        // It is possible to notify the observer
        observable.doSomething()

        assertEquals(observable, observer.processCallSource)
    }

    @Test (expected=UninitializedPropertyAccessException::class)
    fun testObservableObservedNoInitialization () {
        val observer = ToyObserver()
        val observable = ToyObservable()
        val event = ToyEvent(observable)
        observer.process(event) // Exception because no initialization for observable
    }

    @Test (expected=UninitializedPropertyAccessException::class)
    fun testObservableObservedWrongInitialization () {
        val observer = ToyObserver()
        val observable = ToyObservable()
        observer.addObserved(observable)
        observable.doSomething() // Exception because no initialization for observable
    }

    @Test (expected=IllegalArgumentException::class)
    fun testObservableObservedDoubleInitialization () {
        val observer = ToyObserver()
        val observable = ToyObservable()
        observable.initObservation(observable, observer, "some name")
        observer.addObserved(observable) // Exception because the observation was already intitialized
    }

    @Test (expected=IllegalStateException::class)
    fun testObservableObservedDoubleInitialization2 () {
        val observer1 = ToyObserver()
        val observer2 = ToyObserver()
        val observable = ToyObservable()
        observable.initObservation(observable, observer1, "some name")
        observable.initObservation(observable, observer2, "another name") // Exception
    }

    @Test (expected=Exception::class)
    fun testObservableObservedIncorrectProcessCall () {
        val observer = ToyObserver()
        val observable1 = ToyObservable()
        val observable2 = ToyObservable()
        observable1.initObservation(observable1, observer, "some name")
        val event = ToyEvent(observable2)

        observer.process(event)
    }

    @Test (expected=Exception::class)
    fun testObservableObservedWhatHappensHere () {
        val observer = ToyObserver()
        val observable1 = ToyObservable()
        val observable2 = ToyObservable()
        observable1.initObservation(observable2, observer, "some name")
        val event = ToyEvent(observable2)

        observer.process(event)
    }

}

class ToyEvent(o: Observable) : Event(o) {
}

class ToyObservable : Observable by ObservableImpl() {
    fun doSomething () {
        __notify(ToyEvent(this))
    }
}


class ToyObserver : Observer () {
    lateinit var processCallSource : Observable

    override fun process(e: Event) {
        super.process(e)
        processCallSource = e.source
    }
}