/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.observation

import fr.ulille.grapp.graph.*
import kotlin.test.Test
import kotlin.test.assertEquals

class TestGraphs {

    @Test fun digraphWithSelfEdge () {
        val g = DirectedGraph()
        g.addVertex("a")
        Graphs.addEdge(g, "a", "a")
        assertEquals(setOf("a"), g.vertexSet())
        assertEquals(1, g.edgeSet().size)
    }

    @Test fun ugraphWithSelfEdge () {
        val g = UndirectedGraph()
        g.addVertex("a")
        Graphs.addEdge(g, "a", "a")
        assertEquals(setOf("a"), g.vertexSet())
        assertEquals(1, g.edgeSet().size)
    }

    @Test fun ugraphAddingEdgeTwice() {
        val g = UndirectedGraph()
        g.addVertex("a")
        g.addVertex("b")
        Graphs.addEdge(g, "a", "b")
        Graphs.addEdge(g, "a", "b")
        assertEquals(1, g.edgeSet().size)
    }

    @Test fun digraphAddingEdgeTwice() {
        val g = DirectedGraph()
        g.addVertex("a")
        g.addVertex("b")
        Graphs.addEdge(g, "a", "b")
        Graphs.addEdge(g, "a", "b")
        assertEquals(1, g.edgeSet().size)
    }

    @Test fun uwgraphAddingEdgeTwiceWithDifferentWeight() {
        val g = UndirectedWeightedGraph()
        g.addVertex("a")
        g.addVertex("b")
        Graphs.addEdge(g, "a", "b", 1.0)
        Graphs.addEdge(g, "a", "b", 2.0)
        assertEquals(1, g.edgeSet().size)
        assertEquals(1.0, g.edgeSet().first()!!.weight)
    }

    @Test fun diwgraphAddingEdgeTwiceWithDifferentWeight() {
        val g = DirectedWeightedGraph()
        g.addVertex("a")
        g.addVertex("b")
        Graphs.addEdge(g, "a", "b", 1.0)
        Graphs.addEdge(g, "a", "b", 2.0)
        assertEquals(1, g.edgeSet().size)
        assertEquals(1.0, g.edgeSet().first()!!.weight)
    }
}