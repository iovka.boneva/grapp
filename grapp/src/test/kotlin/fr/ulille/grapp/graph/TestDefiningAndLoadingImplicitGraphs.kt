/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.graph

import fr.ulille.grapp.io.ImplicitGraphFormatError
import org.junit.BeforeClass
import org.junit.Test
import java.lang.ClassCastException
import java.nio.file.Files
import java.nio.file.Paths
import javax.tools.JavaCompiler
import javax.tools.JavaFileObject
import javax.tools.StandardJavaFileManager
import javax.tools.ToolProvider
import kotlin.io.path.extension
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TestDefiningAndLoadingImplicitGraphs {

    companion object {
        fun completeExploreByTraversal (ig: Graph) {
            (ig as ImplicitGraphInterface).explore(ig)
        }

        // The test classes are in folder test/resources
        @JvmStatic
        val javaClassesPackageName = "fr.ulille.grapp.graph."
        @JvmStatic
        val javaSourceFilesURLPrefix = javaClassesPackageName.replace(".", "/")


        @BeforeClass
        @JvmStatic
        fun compileAllSourceFiles () {
            val classLoader = TestDefiningAndLoadingImplicitGraphs::class.java.classLoader
            val compiler: JavaCompiler = ToolProvider.getSystemJavaCompiler()
            val fileManager: StandardJavaFileManager = compiler.getStandardFileManager(null, null, null)

            val singleJavaClassName = "ImplicitGraph_wrong_type"
            val sourceFileURL = classLoader.getResource("${javaSourceFilesURLPrefix}${singleJavaClassName}.java")
            val sourceFilesFolder = Paths.get(sourceFileURL.toURI()).parent

            val sourceFiles = Files.list(sourceFilesFolder)
                .filter { it.extension == "java"}
                .map { it.toFile() }
                .toList()
            val compilationUnits: Iterable<JavaFileObject?> = fileManager.getJavaFileObjectsFromFiles(sourceFiles)
            compiler.getTask(null, fileManager, null, null, null, compilationUnits).call()
            fileManager.close()
        }
    }

    fun loadImplicitGraph (javaClassName : String) : ImplicitGraph {
        val classLoader = TestDefiningAndLoadingImplicitGraphs::class.java.classLoader
        val c = classLoader.loadClass("${javaClassesPackageName}${javaClassName}")
        return c.getDeclaredConstructor().newInstance() as ImplicitGraph
    }


    @Test
    fun testDigraph2nodes2edges () {
        val ig = loadImplicitGraph("ImplicitGraph_digraph_2nodes_2edges")
        val graph = ImplicitGraphsUtil.getGraph(ig)
        assert(graph is DirectedGraph)
        assertEquals(1, graph.vertexSet().size)
        assertEquals(0, graph.edgeSet().size)

        completeExploreByTraversal(graph)
        assertEquals(2, graph.vertexSet().size)
        assertEquals(2, graph.edgeSet().size)
    }

    @Test
    fun testDiwgraph2nodes2edges () {
        val ig = loadImplicitGraph("ImplicitGraph_diwgraph_2nodes_2edges")
        val graph = ImplicitGraphsUtil.getGraph(ig)
        assert(graph is DirectedWeightedGraph)
        assertEquals("petit graphe", graph.getName())
        assertEquals(1, graph.vertexSet().size)
        assertEquals(0, graph.edgeSet().size)

        completeExploreByTraversal(graph)
        assertEquals(2, graph.vertexSet().size)
        assertEquals(2, graph.edgeSet().size)
        assertEquals(1.0, graph.getEdge("a", "b").weight)
        assertEquals(10.0, graph.getEdge("b", "a").weight)
    }

    @Test
    fun testDiwgraph3nodes3edges () {
        val ig = loadImplicitGraph("ImplicitGraph_diwgraph_3nodes_3edges")
        val graph = ImplicitGraphsUtil.getGraph(ig)
        assert(graph is DirectedWeightedGraph)
        assertEquals("petit graphe", graph.getName())
        assertEquals(1, graph.vertexSet().size)
        assertEquals(0, graph.edgeSet().size)

        completeExploreByTraversal(graph)
        assertEquals(3, graph.vertexSet().size)
        assertEquals(3, graph.edgeSet().size)
        assertEquals(1.0, graph.getEdge("a", "b").weight)
        assertEquals(2.0, graph.getEdge("b", "c").weight)
        assertEquals(3.0, graph.getEdge("c", "a").weight)
    }

    @Test
    fun testNullName () {
        val ig = loadImplicitGraph("ImplicitGraph_null_name")
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            ImplicitGraphsUtil.getGraph(ig)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.nameShouldNotBeNull))
    }

    @Test
    fun testEmptyName () {
        val ig = loadImplicitGraph("ImplicitGraph_empty_name")
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            ImplicitGraphsUtil.getGraph(ig)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.nameShouldNotBeNull))
    }

    @Test
    fun testNullType () {
        val ig = loadImplicitGraph("ImplicitGraph_null_type")
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            ImplicitGraphsUtil.getGraph(ig)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.typeShouldNotBeNull))
    }

    @Test
    fun testWrongType () {
        val ig = loadImplicitGraph("ImplicitGraph_wrong_type")
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            ImplicitGraphsUtil.getGraph(ig)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.wrongType))
    }

    @Test
    fun testIncorrectFormatWeightedSuccessors1 () {
        val ig = loadImplicitGraph("ImplicitGraph_incorrect_format_weighted_successors1")
        val graph = ImplicitGraphsUtil.getGraph(ig)
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            completeExploreByTraversal(graph)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.incorrectFormatWeightedSuccessors))
    }

    @Test
    fun testIncorrectFormatWeightedSuccessors2 () {
        val ig = loadImplicitGraph("ImplicitGraph_incorrect_format_weighted_successors2")
        val graph = ImplicitGraphsUtil.getGraph(ig)
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            completeExploreByTraversal(graph)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.incorrectFormatWeightedSuccessors))
    }

    @Test
    fun testIncorrectFormatWeightedSuccessors3 () {
        val ig = loadImplicitGraph("ImplicitGraph_incorrect_format_weighted_successors3")
        val graph = ImplicitGraphsUtil.getGraph(ig)
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            completeExploreByTraversal(graph)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.incorrectFormatWeightedSuccessors))
    }

    @Test
    fun testNullSuccessor () {
        val ig = loadImplicitGraph("ImplicitGraph_null_successor")
        val graph = ImplicitGraphsUtil.getGraph(ig)
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            completeExploreByTraversal(graph)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.vertexShouldNotBeNull))
    }

    @Test
    fun testNullInitialVertex () {
        val ig = loadImplicitGraph("ImplicitGraph_null_initial_vertex")
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            ImplicitGraphsUtil.getGraph(ig)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.vertexShouldNotBeNull))
    }

    @Test
    fun testUndirectedType () {
        val ig = loadImplicitGraph("ImplicitGraph_undirected_type")
        val exc = assertFailsWith<ImplicitGraphFormatError> {
            ImplicitGraphsUtil.getGraph(ig)
        }
        assert(exc.toString().contains(ImplicitGraphsUtil.wrongType))
    }

    @Test
    fun testMissingImplements () {
        assertFailsWith<ClassCastException> {
            loadImplicitGraph("ImplicitGraph_missing_implements")
        }
    }

}