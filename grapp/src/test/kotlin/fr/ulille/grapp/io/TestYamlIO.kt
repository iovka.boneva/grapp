package fr.ulille.grapp.io

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue


class TestYamlIO {

    @Test fun testCorrectDigraph () {
        val s =
            """
                type: digraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: [d, a]
                  d: []
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(setOf("a", "b", "c", "d"), g.vertexSet())
        assertEquals(5, g.edgeSet().size)
        assertEquals(3, g.edgesOf("a").size)
    }

    @Test fun testCorrectUgraph () {
        val s =
            """
                type: ugraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: [d, a]
                  d: []
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(setOf("a", "b", "c", "d"), g.vertexSet())
        assertEquals(4, g.edgeSet().size)
        assertEquals(2, g.edgesOf("a").size)
    }

    @Test fun testCorrectDiwgraph () {
        val s =
            """
                type: diwgraph
                name: G1
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4, a: 5}
                  d: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(setOf("a", "b", "c", "d"), g.vertexSet())
        assertEquals(5, g.edgeSet().size)
        assertEquals(3, g.edgesOf("a").size)
        assertEquals(-3.0, g.getEdgeWeight(g.getEdge("b", "c")))
    }

    @Test fun testCorrectUwgraph () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(setOf("a", "b", "c", "d"), g.vertexSet())
        assertEquals(4, g.edgeSet().size)
        assertEquals(2, g.edgesOf("a").size)
        assertEquals(2.0, g.getEdgeWeight(g.getEdge("a", "c")))
    }

    @Test fun testCorrectEmptyDigraph () {
        val s =
            """
                type: digraph
                name: G1
                nodes: []
                edges: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(0, g.vertexSet().size)
        assertEquals(0, g.edgeSet().size)
    }

    @Test
    fun testParseErrorMissingGraphType () {
        val s =
            """
                name: G1
                nodes: []
                edges: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.missingTypeMessage))

    }

    @Test
    fun testParseErrorInexistingGraphType () {
        val s =
            """
                name: G1
                type: notexist
                nodes: []
                edges: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.inexistantGrappTypeMessage))
    }

    @Test
    fun testParseErrorMissingName () {
        val s =
            """
                type: digraph
                nodes: []
                edges: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.missingNameMessage))
    }

    @Test fun parseErrorIncorrectYamlNoSpaceAfterColumn () {
        val s =
            """
                type: diwgraph
                name: pcc/robot-energie
                nodes: [a,b]
                wedges:
                  a: {b:3}
                  b: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.yamlSyntaxErrorMessage))
        // Dans ce cas il est difficile de donner des informations plus précises, car l'erreur levée est NullPointerException
        // car on ne trouve pas de double pour les successeurs de a
    }

    @Test
    fun parseErrorIncorrectYamlNoSpaceAfterColumn2() {
        val s =
            """
            type: diwgraph
            name:g1
            nodes: []
            edges: {}
        """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.yamlSyntaxErrorMessage))
    }

    @Test fun nodeNameCannotBeNumber () {
        val s =
            """
            type: digraph
            name: g1
            nodes: [1, 2]
            edges: 
              1: [2]
              2: []
        """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.forbiddenNodeNameMessage))
    }

    @Test fun nodeNameForbiddenPattern () {
        val forbiddenNodeNames = arrayOf("s-", "1a", "a b", "a.b")
        val s =
            """
            type: digraph
            name: g1
            nodes: [#]
            edges: 
              #: []
        """.trimIndent()
        for (nodeName in forbiddenNodeNames) {
            val x = s.replace("#", nodeName)
            val exc = assertFailsWith<YAMLFormatError> {
                YamlParser.parse(x.byteInputStream()) }
            assert(exc.toString().contains(YamlGraph.forbiddenNodeNameMessage))
        }
    }

    @Test fun allowedNodeNames () {
        val allowedNodeNames = arrayOf("s", "a1", "a_b", "a_2_33_b_")
        val s =
            """
            type: digraph
            name: g1
            nodes: [#]
            edges: 
              #: []
        """.trimIndent()
        for (nodeName in allowedNodeNames) {
            val x = s.replace("#", nodeName)
            YamlParser.parse(x.byteInputStream())
        }
    }

    @Test fun testDigraphWithDuplicateEdge () {
        val s =
            """
                type: digraph
                name: G1
                nodes: [a, b]
                edges:
                  a: [b, b]
                  b: []
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(1, g.edgeSet().size)
    }

    @Test fun diwgraphWithDuplicatedEdgeDifferentWeight () {
        val s =
            """
                type: diwgraph
                name: G1
                nodes: [a, b]
                wedges:
                  a: {b: 1, b: 2}
                  b: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(1, g.edgeSet().size)
        // The yaml map takes the second value
        assertEquals(2.0, g.edgeSet().first()!!.weight)
    }

    @Test fun uwgraphWithDuplicatedEdgeDifferentWeight () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b]
                wedges:
                  a: {b: 1}
                  b: {a: 2}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals(1, g.edgeSet().size)
        // The graph keeps the edge added first
        assertEquals(1.0, g.edgeSet().first()!!.weight)
    }

    @Test
    fun testParseErrorMissingNodes () {
        val s =
            """
                type: uwgraph
                name: G1
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
                YamlParser.parse(s.byteInputStream())
            }
        assertTrue(exc.toString().contains(YamlGraph.missingNodesMessage))
    }

    @Test
    fun testParseErrorTypoNodes () {
        val s =
            """
                type: uwgraph
                name: G1
                notes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
                YamlParser.parse(s.byteInputStream())
            }
        assert(exc.toString().contains(YamlGraph.unknownPropertyMessage))
    }

    @Test
    fun testParseErrorWrongValueTypeNodes () {
        val s =
            """
                type: digraph
                name: G1
                nodes: {a, b, c, d}
                edges:
                  a: [b, c]
                  b: [c]
                  c: [d, a]
                  d: []
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
                YamlParser.parse(s.byteInputStream())
        }
        // "type incorrect"
        assert(exc.toString().contains(YamlGraph.incorrectValueTypeMessage))
    }

    @Test
    fun testParseErrorMissingWedges () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
                YamlParser.parse(s.byteInputStream())
            }
        assert(exc.toString().contains(YamlGraph.missingWedgesMessage))
    }

    @Test
    fun testParseErrorMissingEdges () {
        val s =
            """
                type: ugraph
                name: G1
                nodes: [a, b, c, d]
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.missingEdgesMessage))
    }

    @Test
    fun testParseErrorEdgesAndWedges () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: []
                  b: []
                  c: []
                  d: []
                wedges:
                  a: {}
                  b: {}
                  c: {}
                  d: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.forbiddenEdgesMessage))
    }


    @Test
    fun testParseErrorTypoEdges () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                edg:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.unknownPropertyMessage))
    }

    @Test
    fun testParseErrorWrongTypeValueSuccessorsMap () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                wedges:
                  a: [b, c]
                  b: {c: -3}
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.incorrectValueTypeMessage))
    }

    @Test
    fun testParseErrorWrongTypeValueSuccessorsList () {
        val s =
            """
                type: ugraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: {d: 4}
                  d: []
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.incorrectValueTypeMessage))
    }

    @Test
    fun testParseErrorNonValueInEdges () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: {b: 1, c: 2}
                  b: 
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.incorrectValueTypeMessage))
    }

    @Test
    fun testParseErrorMissingNodeInEdgeList () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.differentEdgesKeysNodesSetMessage))
    }

    @Test
    fun testParseErrorInexistingSourceNodeInEdgeList () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4}
                  d: {}
                  x: {}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.differentEdgesKeysNodesSetMessage))
    }

    @Test
    fun testParseErrorInexistingTargetNodeInEdgeList () {
        val s =
            """
                type: uwgraph
                name: G1
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4}
                  d: {x: 1}
            """.trimIndent()
        val exc = assertFailsWith<YAMLFormatError> {
            YamlParser.parse(s.byteInputStream())
        }
        assert(exc.toString().contains(YamlGraph.unknownNodeMessage))
    }


    @Test
    fun testGraphWithDrawingInstructions () {
        val s =
            """
                type: digraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: [d, a]
                  d: []
                drawInstructions:
                  a: 0,0
                  b: 0,1
                  c: 0,-1
                  d: -1,1
                  a/b: bend left=45;above
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        assertEquals("bend left=45;above", g.getDrawInstructions()!!["a/b"])
    }


}