/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import fr.ulille.grapp.algorithms.GraphsForTesting
import fr.ulille.grapp.ui.CommandLineUtil
import fr.ulille.grapp.ui.TestGrapp
import java.nio.file.Files
import kotlin.io.path.exists
import kotlin.test.AfterTest
import kotlin.test.Test
import kotlin.test.assertTrue

class TestDotIo {

    @AfterTest
    fun tearDown () {
        for (f in TestGrapp.tmpFileNames)
            if (Files.exists(f))
                Files.delete(f)
        if (TestGrapp.outputDrawDotFileName.exists())
            Files.delete(TestGrapp.outputDrawDotFileName)
        if (TestGrapp.outputDrawPngFileName.exists())
            Files.delete(TestGrapp.outputDrawPngFileName)
    }

    @Test
    fun testSerializeGraphDefaultProperties () {

        val filePath = TestGrapp.getOneFileName()
        DotIo(GraphsForTesting.grapheFlotSujetTp).serialize(filePath.toString())
    }

    @Test
    fun testJgraphtDotIo() {
        // Export the graph to dot format
        val filePathPng = TestGrapp.outputDrawPngFileName
        val filePathDot = TestGrapp.outputDrawDotFileName
        DotIo(GraphsForTesting.grapheFlotSujetTp).serialize(filePathDot.toString())

        // Draw the graph
        val isWindows = System.getProperty("os.name")
            .lowercase().startsWith("windows")

        val builder = ProcessBuilder()
        if (isWindows) {
            builder.command("cmd.exe", "/c", "dot.exe -Tpng -o $filePathPng $filePathDot")
        } else {
            builder.command("sh", "-c", "dot -Tpng -o $filePathPng $filePathDot")
        }

        builder.directory(filePathDot.toFile().parentFile)
        val process = builder.start()
        val exitCode = process.waitFor()
        assert(exitCode == 0)
    }

    @Test
    fun testExecuteDot () {
        val (inFilePath, outFilePath) = TestGrapp.getTwoFileNames()
        inFilePath.toFile().writeText("strict digraph G {\n" +
            "  1;\n" +
            "  2;\n" +
            "  3;\n" +
            "  4;\n" +
            "  1 -> 2;\n" +
            "  3 -> 4;\n" +
            "}")
        CommandLineUtil.executeDotToPng(inFilePath.toString(), outFilePath.toString())
        inFilePath.toFile().delete()
    }

    @Test
    fun testDotGraphNameWithAccents () {
        val graphString =
            """
                name: gé
                type: uwgraph 
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4, a: 5}
                  d: {}
            """.trimIndent()
        val graph = YamlParser.parse(graphString.byteInputStream())
        val outFilePath = TestGrapp.getOneFileName()
        DotIo(graph).serialize(outFilePath.toString())
        assertTrue(Files.readAllLines(outFilePath)[0].startsWith("strict graph g {"))
    }

    @Test
    fun testDotGraphNameWithDigits () {
        val graphString =
            """
                name: g1
                type: uwgraph 
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4, a: 5}
                  d: {}
            """.trimIndent()
        val graph = YamlParser.parse(graphString.byteInputStream())
        val outFilePath = TestGrapp.getOneFileName()
        DotIo(graph).serialize(outFilePath.toString())
        assertTrue(Files.readAllLines(outFilePath)[0].startsWith("strict graph g1 {"))
    }

    @Test
    fun testDotGraphNameDigitsOnly () {
        val graphString =
            """
                name: 123
                type: uwgraph 
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4, a: 5}
                  d: {}
            """.trimIndent()
        val graph = YamlParser.parse(graphString.byteInputStream())
        val outFilePath = TestGrapp.getOneFileName()
        DotIo(graph).serialize(outFilePath.toString())
        assertTrue(Files.readAllLines(outFilePath)[0].startsWith("strict graph 123 {"))
    }

    @Test
    fun testDotGraphNameAccentsOnly () {
        val graphString =
            """
                name: éé
                type: uwgraph 
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4, a: 5}
                  d: {}
            """.trimIndent()
        val graph = YamlParser.parse(graphString.byteInputStream())
        val outFilePath = TestGrapp.getOneFileName()
        DotIo(graph).serialize(outFilePath.toString())
        assertTrue(Files.readAllLines(outFilePath)[0].startsWith("strict graph graph"))
    }
}