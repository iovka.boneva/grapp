/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import kotlin.test.Test

class TestLatexIo {

    @Test
    fun testOutputUgraph () {
        val s =
            """
                type: ugraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: [d]
                  d: []
                drawInstructions:
                  a: 0,0
                  b: 1,0
                  c: 2,0
                  d: 3,0
                  a/c: bend left;
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val o = StringBuilder()
        LatexSerializer.serializeToFigure(g,o)
    }


}