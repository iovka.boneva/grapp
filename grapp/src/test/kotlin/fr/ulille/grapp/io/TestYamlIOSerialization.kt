/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import kotlin.test.Test
import kotlin.test.assertEquals

class TestYamlIOSerialization {

    @Test
    fun testSerializeDirectedGraph () {
        val s =
            """
                name: G1
                type: digraph
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: [a, d]
                  d: []
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val o = StringBuilder()
        YamlSerializer.serialize(g, o)
        assertEquals(s.trim(), o.toString().trim())
    }

    @Test fun testSerializeUgraph () {
        val s =
            """
                type: ugraph
                name: G1
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: [d, a]
                  d: []
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val o = StringBuilder()
        YamlSerializer.serialize(g, o)
        val sWithoutRepetedEdges =
            """
                name: G1
                type: ugraph
                nodes: [a, b, c, d]
                edges:
                  a: [b, c]
                  b: [c]
                  c: [d]
                  d: []
            """.trimIndent()
        assertEquals(sWithoutRepetedEdges.trim(), o.toString().trim())
    }

    @Test fun testSerializeDiwgraph () {
        val s =
            """
                name: G1
                type: wdigraph
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4, a: 5}
                  d: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val o = StringBuilder()
        YamlSerializer.serialize(g, o)
        val sWithNormalizedGrappType =
            """
                name: G1
                type: diwgraph
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {a: 5, d: 4}
                  d: {}
            """.trimIndent()
        assertEquals(sWithNormalizedGrappType.trim(), o.toString().trim())
    }

    @Test fun testSerializeUwgraph () {
        val s =
            """
                name: G1
                type: uwgraph
                nodes: [a, b, c, d]
                wedges:
                  a: {b: 1, c: 2}
                  b: {c: -3}
                  c: {d: 4}
                  d: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val o = StringBuilder()
        YamlSerializer.serialize(g, o)
        assertEquals(s.trim(), o.toString().trim())
    }

    @Test fun testSerializeEmptyDigraph () {
        val s =
            """
                name: G1
                type: digraph
                nodes: []
                edges: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val o = StringBuilder()
        YamlSerializer.serialize(g, o)
        assertEquals(s.trim(), o.toString().trim())
    }

}