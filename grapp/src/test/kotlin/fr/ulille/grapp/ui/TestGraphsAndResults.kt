/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import fr.ulille.grapp.graph.Graph
import org.mockito.Mockito.*
import kotlin.test.*

class TestGraphsAndResults {

    @Test
    fun testNormalizeGraphName () {
        assertTrue(".".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))
        assertTrue("$".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))
        assertTrue("_".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))
        assertFalse("a".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))
        assertFalse("B".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))
        assertFalse("1".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))
        assertFalse("é".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))
        assertFalse("É".matches(GraphsAndResults.forbiddenSubsequenceInGraphName))

        assertEquals("g_1", GraphsAndResults.normalizeGraphName("g.1"))
        assertEquals("c_est_un_beau_graphe", GraphsAndResults.normalizeGraphName("c'est un beau graphe"))
        assertEquals("gé", GraphsAndResults.normalizeGraphName("gé"))
        assertEquals("a_b", GraphsAndResults.normalizeGraphName("a  b"))
    }

    @Test
    fun testAddGetWithName () {
        val g1 = mock(Graph::class.java)
        `when`(g1.getName()).thenReturn("G1")

        val gr = GraphsAndResults()
        gr.add(g1,false)
        assertTrue(gr.checkInvariant())
        assertEquals(g1, gr.getGraph("G1"))
    }

    @Test
    fun testAddGetWithId () {
        val g1 = mock(Graph::class.java)
        `when`(g1.getName()).thenReturn("G1")

        val gr = GraphsAndResults()
        gr.add(g1,false)
        assertTrue(gr.checkInvariant())
        assertEquals(g1, gr.getGraph("g1"))
    }

    @Test
    fun testGetPriorityOnId () {
        val gNameXidg1 = mock(Graph::class.java)
        `when`(gNameXidg1.getName()).thenReturn("X")

        val gNameg1idg2 = mock(Graph::class.java)
        `when`(gNameg1idg2.getName()).thenReturn("g1")

        val gr = GraphsAndResults()
        gr.add(gNameXidg1, false)
        assertTrue(gr.checkInvariant())
        gr.add(gNameg1idg2, false)
        assertTrue(gr.checkInvariant())

        assertEquals(gNameXidg1, gr.getGraph("g1"))
    }

    @Test
    fun testGetUnknownGraph() {
        val gr = GraphsAndResults()
        assertNull(gr.getGraph("G1"))
    }

    @Test
    fun testAddGraphWithExistingNameHasNoEffectWithoutOverwrite() {
        val g1 = mock(Graph::class.java)
        `when`(g1.getName()).thenReturn("G1")
        val g1bis = mock(Graph::class.java)
        `when`(g1bis.getName()).thenReturn("G1")

        val gr = GraphsAndResults()
        gr.add(g1, false)
        assertTrue(gr.checkInvariant())
        assertEquals(g1, gr.getGraph("G1"))
        val added = gr.add(g1bis, false)
        assertTrue(gr.checkInvariant())
        assertFalse(added)
        assertEquals(g1, gr.getGraph("G1"))
    }

    @Test
    fun testAddGraphWithExistingNameReplacesWithOverwrite() {
        val g1 = mock(Graph::class.java)
        `when`(g1.getName()).thenReturn("G1")
        `when`(g1.toString()).thenReturn("g1")
        val g1bis = mock(Graph::class.java)
        `when`(g1bis.getName()).thenReturn("G1")
        `when`(g1bis.toString()).thenReturn("g1bis")

        val gr = GraphsAndResults()
        gr.add(g1, false)
        assertTrue(gr.checkInvariant())
        assertEquals(g1, gr.getGraph("G1"))
        val added = gr.add(g1bis, true)
        assertTrue(added)
        assertEquals(g1bis, gr.getGraph("G1"))
    }

    @Test
    fun testAddResult () {
        val g1 = mock(Graph::class.java)
        `when`(g1.getName()).thenReturn("G1")

        val r1 = mock(Result::class.java)
        `when`(r1.descr).thenReturn("R1")
        `when`(r1.graph).thenReturn(g1)

        val gr = GraphsAndResults()
        gr.add(g1,false)
        assertTrue(gr.checkInvariant())
        val added = gr.add(r1)
        assertTrue(gr.checkInvariant())
        assertTrue(added)

        assertEquals(g1, gr.getGraph("G1"))
        assertEquals(r1, gr.getResult("r1.1"))
    }

    @Test
    fun testAddResultUnknownGraph () {
        val g1 = mock(Graph::class.java)
        `when`(g1.getName()).thenReturn("G1")

        val r1 = mock(Result::class.java)
        `when`(r1.descr).thenReturn("R1")
        `when`(r1.graph).thenReturn(g1)

        val gr = GraphsAndResults()
        val added = gr.add(r1)
        assertTrue(gr.checkInvariant())
        assertFalse(added)
        assertNull(gr.getResult("r1.1"))
    }

}


