/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import fr.ulille.grapp.algorithms.GraphsForTesting
import fr.ulille.grapp.graph.Edge
import fr.ulille.grapp.graph.Graphs
import fr.ulille.grapp.graph.GrappType
import fr.ulille.grapp.io.YamlParser
import kotlin.test.*

class TestAlgorithmsUtil {

    @Test
    fun testRunDijkstra () {
        val s =
            """
                type: diwgraph
                name: G1
                nodes: [a, b, c]
                wedges:
                  a: {b: 1, c: 5}
                  b: {c: 2}
                  c: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val result = AlgorithmsUtil.runOptimalPathAlgorithm(ShortestPathAlgorithmName.Dijkstra, g, "a")
        assertEquals(0.0, result.distancesMap["a"])
        assertEquals(1.0, result.distancesMap["b"])
        assertEquals(3.0, result.distancesMap["c"])
        assertEquals("a", result.predecessorsMap["b"])
        assertEquals("b", result.predecessorsMap["c"])
        assertEquals(2, result.tree.edgeSet().size)
        assert(result.tree.edgeSet().contains(Edge("a", "b", 1.0)))
        assert(result.tree.edgeSet().contains(Edge("b", "c", 2.0)))
    }

    @Test
    fun testRunDijkstraInaccessibleNode () {
        val s =
            """
                type: diwgraph
                name: G1
                nodes: [a, b, c]
                wedges:
                  a: {b: 1, c: 5}
                  b: {c: 2}
                  c: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val result = AlgorithmsUtil.runOptimalPathAlgorithm(ShortestPathAlgorithmName.Dijkstra, g, "b")
        assertEquals(1, result.tree.edgeSet().size)
        assertEquals(Edge("b","c",2.0), result.tree.edgeSet().first())
        assertEquals(Double.POSITIVE_INFINITY, result.distancesMap["a"])
        assertEquals(0.0, result.distancesMap["b"])
        assertEquals(2.0, result.distancesMap["c"])
    }

    @Test
    fun testRunBellmanFord () {
        val s =
            """
                type: diwgraph
                name: G1
                nodes: [a, b, c]
                wedges:
                  a: {b: 1, c: -5}
                  b: {c: 2}
                  c: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val result = AlgorithmsUtil.runOptimalPathAlgorithm(ShortestPathAlgorithmName.BellmanFord, g, "a")
        assertEquals(0.0, result.distancesMap["a"])
        assertEquals(1.0, result.distancesMap["b"])
        assertEquals(-5.0, result.distancesMap["c"])
        assertEquals("a", result.predecessorsMap["b"])
        assertEquals("a", result.predecessorsMap["c"])
        assertEquals(2, result.tree.edgeSet().size)
        assertTrue(result.tree.edgeSet().contains(Edge("a", "b", 1.0)))
        assertTrue(result.tree.edgeSet().contains(Edge("a", "c", -5.0)))
    }

    @Test
    fun testRunBellmanFordInaccessibleNode () {
        val s =
            """
                type: diwgraph
                name: G1
                nodes: [a, b, c]
                wedges:
                  a: {b: 1, c: 5}
                  b: {c: 2}
                  c: {}
            """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val result = AlgorithmsUtil.runOptimalPathAlgorithm(ShortestPathAlgorithmName.BellmanFord, g, "b")
        assertEquals(1, result.tree.edgeSet().size)
        assertEquals(Edge("b","c",2.0), result.tree.edgeSet().first())
        assertEquals(Double.POSITIVE_INFINITY, result.distancesMap["a"])
        assertEquals(0.0, result.distancesMap["b"])
        assertEquals(2.0, result.distancesMap["c"])
    }

    @Test
    fun testBFS () {
        val s =
            """
            type: ugraph
            name: G1
            nodes: [a, b, c, d, e, f, g]
            edges:
              a: [b, c, d]
              b: [c, d, e]
              c: [e, g]
              d: [f]
              e: [f]
              f: []
              g: []
        """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val result = AlgorithmsUtil.runGraphTraversalAlgorithm(TraversalAlgorithmName.BFS, g, "a")
        assert(result.tree.isDirected() && !result.tree.isWeighted())

        val treeEdges = listOf(
            Edge("a", "b"), Edge("a", "c"), Edge("a", "d"),
            Edge("b", "e"), Edge("c", "g"), Edge("d", "f"))
        assertEquals(treeEdges.toSet(), result.tree.edgeSet())
    }
    @Test
    fun testDFS() {
        val s =
            """
            type: ugraph
            name: G1
            nodes: [a, b, c, d, e, f, g]
            edges:
              a: [b, c, d]
              b: [c, d, e]
              c: [e, g]
              d: [f]
              e: [f]
              f: []
              g: []
        """.trimIndent()
        val g = YamlParser.parse(s.byteInputStream())
        val result = AlgorithmsUtil.runGraphTraversalAlgorithm(TraversalAlgorithmName.DFS, g, "a")
        assert(result.tree.isDirected() && !result.tree.isWeighted())

        val treeEdges = listOf(
            Edge("a", "b"), Edge("b", "c"), Edge("c", "e"),
            Edge("e", "f"), Edge("f", "d"), Edge("c", "g")
        )
        assertEquals(treeEdges.toSet(), result.tree.edgeSet())
    }

    @Test
    fun testFindPathShortestPathUndirectedGraph1 () {
        val g = GraphsForTesting.grapheNonOrienteValue
        val r = AlgorithmsUtil.runOptimalPathAlgorithm(
            ShortestPathAlgorithmName.Dijkstra, g, "a"
        )
        val p = AlgorithmsUtil.runFindPathAlgorithm(r, "a", "b")
        assertEquals(2, p.pathEdges!!.size)
        assertEquals("a", p.pathEdges!![0].src)
        assertEquals("c", p.pathEdges!![0].tgt)
        assertEquals("c", p.pathEdges!![1].src)
        assertEquals("b", p.pathEdges!![1].tgt)
    }

    @Test
    fun testFindPathShortestPathUndirectedGraph2 () {
        val g = GraphsForTesting.grapheNonOrienteValue
        val r = AlgorithmsUtil.runOptimalPathAlgorithm(
            ShortestPathAlgorithmName.Dijkstra, g, "c"
        )
        val p = AlgorithmsUtil.runFindPathAlgorithm(r, "c", "a")
        assertEquals(1, p.pathEdges!!.size)
        assertEquals("c", p.pathEdges!![0].src)
        assertEquals("a", p.pathEdges!![0].tgt)
    }

    @Test
    fun testFindPathShortestPathUndirectedGraph3 () {
        val g = GraphsForTesting.grapheNonOrienteValue
        val r = AlgorithmsUtil.runOptimalPathAlgorithm(
            ShortestPathAlgorithmName.Dijkstra, g, "a"
        )
        val p = AlgorithmsUtil.runFindPathAlgorithm(r, "f", "d")
        assertNull(p.pathEdges)
    }

    @Test
    fun testFindPathTraversalDirectedGraph () {
        val g = GraphsForTesting.grapheOrientePoidsPositifs
        val r = AlgorithmsUtil.runGraphTraversalAlgorithm(
            TraversalAlgorithmName.DFS, g, "a"
        )
        val p = AlgorithmsUtil.runFindPathAlgorithm(r, "a", "c")
        assertEquals(2, p.pathEdges!!.size)
        assertEquals("a", p.pathEdges!![0].src)
        assertEquals("b", p.pathEdges!![0].tgt)
        assertEquals("b", p.pathEdges!![1].src)
        assertEquals("c", p.pathEdges!![1].tgt)
    }

    @Test
    fun testFindPathWrongNode () {
        val g = GraphsForTesting.grapheOrientePoidsPositifs
        val r = AlgorithmsUtil.runGraphTraversalAlgorithm(
            TraversalAlgorithmName.DFS, g, "a"
        )
        assertFailsWith<IllegalArgumentException> {
            AlgorithmsUtil.runFindPathAlgorithm(r, "g", "c")
        }
        assertFailsWith<IllegalArgumentException> {
            AlgorithmsUtil.runFindPathAlgorithm(r, "a", "g")
        }
    }

    @Test
    fun testFindEmptyPath () {
        val g = GraphsForTesting.grapheOrientePoidsPositifs
        val r = AlgorithmsUtil.runGraphTraversalAlgorithm(
            TraversalAlgorithmName.DFS, g, "a"
        )
        val p = AlgorithmsUtil.runFindPathAlgorithm(r, "e", "f")
        assertNull(p.pathEdges)
    }

    @Test
    fun testEqualityUgraph () {
        val g1 = Graphs.graphFromNodeSet(GrappType.ugraph, setOf("a", "b"))
        val g2 = Graphs.graphFromNodeSet(GrappType.ugraph, setOf("a", "b"))

        Graphs.addEdge(g1, "a", "b")
        Graphs.addEdge(g2, "b", "a")

        assertNotEquals(g1, g2)
    }
}