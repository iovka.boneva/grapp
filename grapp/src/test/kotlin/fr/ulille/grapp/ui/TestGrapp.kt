/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import com.github.ajalt.clikt.output.CliktConsole
import fr.ulille.grapp.algorithms.GraphsForTesting
import fr.ulille.grapp.graph.Graph
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.exists
import kotlin.random.Random
import kotlin.test.*

class TestGrapp {

    companion object {
        val tmpFileNames = mutableListOf<Path>(*(1..3).map { Paths.get("tmp${Random.nextInt()}.yaml") }.toTypedArray())
        val outputDrawFileNameBase = Paths.get("tmp${Random.nextInt()}")
        val outputDrawPngFileName = Paths.get(outputDrawFileNameBase.toString() + ".png")
        val outputDrawDotFileName = Paths.get(outputDrawFileNameBase.toString() + ".dot")
        val outputDrawFileNameInexistingFolder = Paths.get("tmp${Random.nextInt()}", "tmp${Random.nextInt()}")

        fun grappReinitialize() = Grapp.graphsAndResults.clear()
        fun grappAddGraph(g: Graph) = Grapp.graphsAndResults.add(g, false)
        fun grappKnowsGraph(graphIdOrName: String): Boolean = Grapp.graphsAndResults.getGraph(graphIdOrName) != null
        fun grappKnowsResult(resultId: String): Boolean = Grapp.graphsAndResults.getResult(resultId) != null

        fun getOneFileName(): Path {
            Files.createFile(tmpFileNames[0])
            return tmpFileNames[0]
        }

        fun getTwoFileNames(): Pair<Path, Path> {
            Files.createFile(tmpFileNames[0])
            Files.createFile(tmpFileNames[1])
            return Pair(tmpFileNames[0], tmpFileNames[1])
        }

        fun getThreeFileNames(): Triple<Path, Path, Path> {
            Files.createFile(tmpFileNames[0])
            Files.createFile(tmpFileNames[1])
            Files.createFile(tmpFileNames[2])
            return Triple(tmpFileNames[0], tmpFileNames[1], tmpFileNames[2])
        }
    }

    @AfterTest
    fun tearDown () {
        for (f in tmpFileNames)
            if (Files.exists(f))
                Files.delete(f)
        if (outputDrawDotFileName.exists())
            Files.delete(outputDrawDotFileName)
        if (outputDrawPngFileName.exists())
            Files.delete(outputDrawPngFileName)
    }

    @BeforeTest
    fun setUp() {
        grappReinitialize()
    }

    @Test
    fun testListOkNoGraphs () {
        val console = CliktConsoleForTesting(mapOf(1 to "list"))
        Grapp.run(console)
        val s = console.output[1]!!.trim()

        assertCommandOk(console, 1)
        assertTrue(s.contains(Messages.noLoadedGraphs))
    }

    @Test
    fun testListOk () {
        val g1 = GraphsForTesting.grapheOrienteValueNonConnecte
        val g2 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        grappAddGraph(g2)
        val console = CliktConsoleForTesting(mapOf(1 to "list"))
        Grapp.run(console)

        assertCommandOk(console, 1)
        assertTrue(grappKnowsGraph(g1.getName()))
        assertTrue(grappKnowsGraph(g2.getName()))

        val s = console.output[1]!!.trim()

        assertTrue(s.contains(g1.getName()))
        assertTrue(s.contains(g2.getName()))

        assertTrue(console.err[1]!!.isEmpty())
    }

    @Test
    fun testLoadOk() {
        val gyaml = GraphsForTesting.grapheOrienteValueNonConnecteYaml
        val g = GraphsForTesting.grapheOrienteValueNonConnecte

        val path = getOneFileName()
        Files.write(path, gyaml.toByteArray())

        val console = CliktConsoleForTesting(mapOf(1 to "load $path"))
        Grapp.run(console)

        assertCommandOk(console, 1)
        assertTrue(grappKnowsGraph(g.getName()))
        assertTrue(console.output[1]!!.contains(Messages.loadOk))
    }

    @Test
    fun testLoadDuplicateGraphName() {
        val g1yaml = GraphsForTesting.grapheG1Yaml
        val g1bisyaml = GraphsForTesting.grapheG1bisYaml

        val (path, pathbis) = getTwoFileNames()
        Files.write(path, g1yaml.toByteArray())
        Files.write(pathbis, g1bisyaml.toByteArray())

        val console = CliktConsoleForTesting(mapOf(1 to "load $path", 2 to "load $pathbis"))
        Grapp.run(console)

        assertCommandOk(console, 1)
        assertCommandHasError(console, 2)

        assertTrue(grappKnowsGraph("g1"))
        assertFalse(grappKnowsResult("g2"))
    }

    @Test
    fun testLoadFileNotExist() {
        val path = tmpFileNames[0]

        val console = CliktConsoleForTesting(mapOf(1 to "load $path"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
        assertTrue(console.err[1]!!.contains(Messages.errorFileNotFound))
        assertFalse(grappKnowsGraph("g1"))
    }

    @Test
    fun testLoadBadFileFormat () {
        val path = getOneFileName()
        Files.write(path, "this is some random text".toByteArray())

        val console = CliktConsoleForTesting(mapOf(1 to "load $path"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
        assertFalse(grappKnowsGraph("g1"))
    }

    @Test
    fun testLoadErrorsInGraphDefinition1 () {
        val incorrectGraphYaml =
            """
                type: ugraph
                name: G1
                nodes: [a,b]
                edges: 
                    a: [b]
            """.trimIndent()

        val path = getOneFileName()
        Files.write(path, incorrectGraphYaml.toByteArray())

        val console = CliktConsoleForTesting(mapOf(1 to "load $path"))
        Grapp.run(console)

        assertCommandHasError(console,1)
        assertFalse(grappKnowsGraph("g1"))
    }

    @Test
    fun testLoadErrorsInGraphDefinition2 () {
        val incorrectGraphYaml =
            """
                type: ugraph
                name: G1
                key: notexist
                nodes: [a,b]
                edges: 
                    a: [b]
            """.trimIndent()

        val path = getOneFileName()
        Files.write(path, incorrectGraphYaml.toByteArray())

        val console = CliktConsoleForTesting(mapOf(1 to "load $path"))
        Grapp.run(console)

        assertCommandHasError(console,1)
        assertFalse(grappKnowsGraph("g1"))
    }

    @Test
    fun testLoadErrorsInGraphDefinition3 () {
        val incorrectGraphYaml =
            """
                type: ugraph
                name: G1
                key: notexist
                nodes: [a,b]
                edges: 
                    a: [b]
                    b: [a]
                    a: []
            """.trimIndent()

        val path = getOneFileName()
        Files.write(path, incorrectGraphYaml.toByteArray())

        val console = CliktConsoleForTesting(mapOf(1 to "load $path"))
        Grapp.run(console)

        assertCommandHasError(console,1)
        assertFalse(grappKnowsGraph("g1"))
    }


    @Test
    fun testDrawGraphOk () {
        val g1 = GraphsForTesting.grapheOrienteValueNonConnecte
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "draw -graph ${g1.getName()} -to ${outputDrawFileNameBase}"))
        Grapp.run(console)

        assertCommandOk(console, 1)
        assertTrue(Files.exists(outputDrawPngFileName))
        assertTrue(Files.exists(outputDrawDotFileName))
    }

    @Test
    fun testDrawResultOk () {
        val g1 = GraphsForTesting.grapheOrienteValueNonConnecte
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(
            1 to "shortest-path -algorithm Dijkstra -on ${g1.getName()} -from a",
            2 to "draw -result r1.1 -to ${outputDrawFileNameBase}"))
        Grapp.run(console)

        assertCommandOk(console, 2)
        assertTrue(Files.exists(outputDrawPngFileName))
        assertTrue(Files.exists(outputDrawDotFileName))
    }

    @Test
    fun testDrawGraphNotExist () {
        val console = CliktConsoleForTesting(mapOf(1 to "draw -graph G -to ${outputDrawFileNameBase}"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
        assertFalse(Files.exists(outputDrawPngFileName))
        assertFalse(Files.exists(outputDrawDotFileName))
    }

    @Test
    fun testDrawFileWriteError () {
        val g1 = GraphsForTesting.grapheOrienteValueNonConnecte
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "draw -graph ${g1.getName()} -to $outputDrawFileNameInexistingFolder"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }

    @Test
    fun testTraversalOk () {
        val g1 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        assertTrue(grappKnowsGraph("g1"))
        assertFalse(grappKnowsResult("r1.1"))
        val console = CliktConsoleForTesting(mapOf(1 to "traversal -algorithm BFS -on ${g1.getName()} -from a"))
        Grapp.run(console)

        assertCommandOk(console, 1)
        assertTrue(grappKnowsResult("r1.1"))
    }

    @Test
    fun testTraversalGraphNotExist () {
        val console = CliktConsoleForTesting(mapOf(1 to "traversal -algorithm BFS -on G -from a"))
        Grapp.run(console)

        assertCommandHasError(console, 1)

    }

    @Test
    fun testTraversalNodeNotExist () {
        val g1 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "traversal -algorithm DFS -on ${g1.getName()} -from x"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }

    @Test
    fun testTraversalIncorrectCommandFormat () {
        val g1 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "traversal -algorithm DFS -from a"))
        Grapp.run(console)

        assertCommandHasErrorWithHelpOutput(console, 1)
    }

    @Test
    fun testShortestPathOk () {
        val g1 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        assertTrue(grappKnowsGraph("g1"))
        assertFalse(grappKnowsResult("r1.1"))
        val console = CliktConsoleForTesting(mapOf(1 to "shortest-path -algorithm BellmanFord -on ${g1.getName()} -from a"))
        Grapp.run(console)


        assertCommandOk(console, 1)
        assertTrue(grappKnowsResult("r1.1"))
    }

    @Test
    fun testShortestPathGraphNotExist () {
        val console = CliktConsoleForTesting(mapOf(1 to "shortest-path -algorithm BellmanFord -on G -from a"))
        Grapp.run(console)

        assertCommandHasError(console, 1)

    }
    @Test
    fun testShortestPathNodeNotExist () {
        val g1 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "shortest-path -algorithm BellmanFord -on ${g1.getName()} -from x"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }

    @Test
    fun testShortestPathIncorrectCommandFormat () {
        val g1 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "shortest-path -algorithm Belmondo -on ${g1.getName()} -from a"))
        Grapp.run(console)

        assertCommandHasErrorWithHelpOutput(console, 1)
    }

    @Test
    fun testShortestPathIncorrectGraph () {
        val g1 = GraphsForTesting.grapheNonOrienteNonValue
        grappAddGraph(g1)
        val console =
            CliktConsoleForTesting(mapOf(1 to "shortest-path -algorithm BellmanFord -on ${g1.getName()} -from a"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }

    @Test
    fun testMaxflowOk () {
        val g1 = GraphsForTesting.grapheFlotSujetTp
        grappAddGraph(g1)
        assertTrue(grappKnowsGraph("g1"))
        assertFalse(grappKnowsResult("r1.1"))
        val console = CliktConsoleForTesting(mapOf(1 to "maximum-flow -algorithm FordFulkerson -on ${g1.getName()} -source o -sink p"))
        Grapp.run(console)

        assertCommandOk(console, 1)
        assertTrue(grappKnowsResult("r1.1"))
    }

    @Test
    fun testMaxflowGraphNotExist () {
        val console = CliktConsoleForTesting(mapOf(1 to "maximum-flow -algorithm FordFulkerson -on G -source o -sink p"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }
    @Test
    fun testMaxflowNodeNotExist () {
        val g1 = GraphsForTesting.grapheFlotSujetTp
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "maximum-flow -algorithm FordFulkerson -on ${g1.getName()} -source x -sink p"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }

    @Test
    fun testMaxFlowIncorrectCommandFormat () {
        val g1 = GraphsForTesting.grapheFlotSujetTp
        grappAddGraph(g1)
        val console = CliktConsoleForTesting(mapOf(1 to "maximum-flow -algorithm FordFulkerson -on ${g1.getName()} -from o"))
        Grapp.run(console)

        assertCommandHasErrorWithHelpOutput(console, 1)
    }

    @Test
    fun testMaxflowIncorrectGraphType () {
        val g1 = GraphsForTesting.grapheNonOrienteNonValue
        grappAddGraph(g1)
        val console =
            CliktConsoleForTesting(mapOf(1 to "maximum-flow -algorithm FordFulkerson -on ${g1.getName()} -source a -sink b"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }

    @Test
    fun testMaxflowIncorrectGraph () {
        val g1 = GraphsForTesting.grapheFlotIncorrectCapaciteNegative
        grappAddGraph(g1)
        val console =
            CliktConsoleForTesting(mapOf(1 to "maximum-flow -algorithm FordFulkerson -on ${g1.getName()} -source o -sink p"))
        Grapp.run(console)

        assertCommandHasError(console, 1)
    }

    @Test
    fun testFindPath () {
        val g1 = GraphsForTesting.grapheNonOrienteValue
        grappAddGraph(g1)
        val console =
            CliktConsoleForTesting(mapOf(
                1 to "shortest-path -algorithm Dijkstra -on g1 -from a",
                2 to "find-path -on r1.1 -from a -to f"
            ))
        Grapp.run(console)
    }

    private fun assertCommandHasError (console: CliktConsoleForTesting, lineNumber: Int) {
        assertTrue(console.err[lineNumber]!!.isNotEmpty())
    }

    private fun assertCommandOk (console: CliktConsoleForTesting, lineNumber: Int, ) {
        assertTrue(console.output[lineNumber]!!.isNotEmpty())
        assertTrue(console.err[lineNumber]!!.isEmpty())
    }

    private fun assertCommandHasErrorWithHelpOutput (console: CliktConsoleForTesting, lineNumber: Int, ) {
        assertTrue(console.output[lineNumber]!!.isNotEmpty())
        assertTrue(console.err[lineNumber]!!.isNotEmpty())
        assertTrue(console.output[lineNumber]!!.contains(Messages.availableCommands))
    }
}



class CliktConsoleForTesting (private val input : Map<Int, String>): CliktConsole {
    override val lineSeparator: String = "\n"

    val output = mutableMapOf<Int, StringBuilder>(0 to StringBuilder())
    val err = mutableMapOf<Int, StringBuilder>(0 to StringBuilder())

    private var lineNumber = 0

    override fun print(text: String, error: Boolean) {
        if (error)
            err[lineNumber]!!.append(text)
        else
            output[lineNumber]!!.append(text)
    }

    override fun promptForLine(prompt: String, hideInput: Boolean): String? {
        ++lineNumber
        output[lineNumber] = StringBuilder()
        err[lineNumber] = StringBuilder()
        return input[lineNumber]
    }

}