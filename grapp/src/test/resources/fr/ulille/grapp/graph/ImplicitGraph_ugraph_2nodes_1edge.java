/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.graph;
import java.util.*;

public class ImplicitGraph_ugraph_2nodes_1edge implements fr.ulille.grapp.graph.ImplicitGraph {

    public String type () {
        return "ugraph";
    }
    public String name() {
        return "petit graphe";
    }

    public List<String> initialNodes () {
        return Collections.singletonList("a");
    }

    public List<String> successors (String node) {
        List<String> result = new ArrayList<>();
        switch (node) {
            case "a" : result.add("b"); break;
        }
        return result;
    }

}