/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.algorithms.observation.*
import fr.ulille.grapp.graph.Graph
import fr.ulille.grapp.graph.Vertex
import org.jgrapht.Graphs

interface GraphTraversalAlgorithm {
    /** Computes a graph traversal.
     * @param vertex The start vertex, input parameter.
     * @param visitedEdges The discovered edges in order of discovery, output parameter.
     */
    fun compute (vertex: Vertex, predecessorsMap: MutableMap<Vertex, Vertex>)
}

class DepthFirstSearch(graph: Graph) : GraphTraversalAlgorithm, GraphTraversal(graph) {
    override fun compute(vertex: Vertex, predecessorsMap: MutableMap<Vertex, Vertex>) {
        run(vertex, explorationQueue=Lifo(), predecessorsMap)
    }
}

class BreadthFirstSearch(graph: Graph) : GraphTraversalAlgorithm, GraphTraversal(graph) {
    override fun compute(vertex: Vertex, predecessorsMap: MutableMap<Vertex, Vertex>) {
        run(vertex, explorationQueue=Fifo(), pred=predecessorsMap)
    }
}

open class GraphTraversal (graph: Graph) : GraphAlgorithm(graph) {

    /** An observable implementation of a graph traversal.
     * @param vertex The start vertex, input parameter.
     * @param explorationQueue A possibly observable queue used during computation to store vertices that are to be explored. Typically a FIFO for BFS and a LIFO for DFS.
     * @param visitedEdges A possibly observable list which stores discovered edges in order of discovery, output parameter.
     * @param discoveredVertices A possibly observable set of vertices used during computation to store the already explored vertices.
     * @param vertexCurrentIteration A possibly observable vertex used during computation to store the currently explored vertex.
     * @param vertexJustDiscovered A possibly observable vertex used during computation to store the freshly discovered successor of the current vertex.
     */
    fun run (
        vertex: Vertex,
        explorationQueue: TraversalQueue<Vertex>,
        pred: MutableMap<Vertex, Vertex> = mutableMapOf(),
        discoveredVertices: MutableSet<Vertex> = mutableSetOf(),
        vertexCurrentIteration: Var<Vertex> = Var(),
        vertexJustDiscovered: Var<Vertex> = Var()
    ) {

        if (! graph.vertexSet().contains(vertex))
            throw AlgorithmError("Unknown vertex : $vertex")

        discoveredVertices.clear()
        explorationQueue.clear()
        pred.clear()

        explorationQueue.put(vertex)
        discoveredVertices.add(vertex)

        __notify(InitializationCompleteEvent(this))

        while (! explorationQueue.isEmpty()) {
            vertexCurrentIteration.value = explorationQueue.see()
            val next = graph.outgoingEdgesOf(vertexCurrentIteration.value)
                .map { Graphs.getOppositeVertex(graph, it, vertexCurrentIteration.value) }
                .firstOrNull { !discoveredVertices.contains(it) }
            if (next == null) {
                explorationQueue.get()
                continue
            }
            vertexJustDiscovered.value = next
            discoveredVertices.add(vertexJustDiscovered.value!!)

            pred[vertexJustDiscovered.value!!] = vertexCurrentIteration.value!!
            explorationQueue.put(vertexJustDiscovered.value as Vertex)

            __notify(LoopCompleteEvent(this))
        }
    }
}
