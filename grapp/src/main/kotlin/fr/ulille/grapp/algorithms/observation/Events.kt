/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.observation

// CLEAN
abstract class Event (val source: Observable)

abstract class DSEvent(source: Observable, val data: Any) : Event(source)

class DSInitEvent (source: Observable, data: Any) : DSEvent(source, data)
class DSAddEvent (source: Observable, data: Any) : DSEvent(source, data)
class DSRemoveEvent (source: Observable, data:Any) : DSEvent(source, data)

abstract class AlgorithmEvent (source: Observable) : Event(source)
class LoopCompleteEvent (source: Observable) : AlgorithmEvent(source)
class InitializationCompleteEvent (source: Observable) : AlgorithmEvent(source)
class PhaseChangeEvent (source: Observable) : AlgorithmEvent(source)

class VariableEvent (source: Observable, val newValue: Any?) : Event(source)