/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.observation

import java.lang.IllegalArgumentException

/** Notifies a unique {@see Observer} about some of its state changes.
 * The state changes are encoded by {@see Event}s.
 * Before observation can start, it must be initialized by #initObservation
 * This creates the {@see ObservationRelation} between the observed and the observer, and adds this observable to the observer.
 */
interface Observable {
    var __obs: ObservationRelation
    /** Initializes the observation.
     * Can be called at most once.
     * @param thisObservable : the observable that is observed. Needed in order to delegate the implementation of Observable.
     * @param observer: the observer
     * @param observableName: a name to identify this observable among the different observables observed by the observer
     */
    fun initObservation (thisObservable: Observable, observer: Observer, observableName: String)

    /** Notifies the observer. Has no effect if no observation was initialized. */
    fun __notify (e: Event)
}

class ObservableImpl : Observable {
    override lateinit var __obs: ObservationRelation

    override fun initObservation(thisObservable: Observable, observer: Observer, observableName: String) {
        if (::__obs.isInitialized)
            throw IllegalStateException("Initialization already done.")
        else {
            __obs = ObservationRelation(thisObservable, observer, observableName)
            observer.addObserved(thisObservable)
        }
    }
    override fun __notify(e: Event) {
        if (::__obs.isInitialized)
            __obs.observer.process(e)
    }
}

/** Observes a number of {@see Observable}, for which an observation relation has been defined with this observer.
 * They can be accessed by the name of the observation relation. */
abstract class Observer {
    val observed = mutableMapOf<String,Observable> ()

    /** Retrieves the observable with the given name.
     * @throws NoSuchElementException if no such observable is known
     */
    fun getObserved (observationName : String) : Observable {
        return observed[observationName] ?: throw NoSuchElementException()
    }

    /** Should not be called directly, but only from Observable#initObservation */
    internal fun addObserved (o: Observable) {
        if (o.__obs.observer != this)
            throw IllegalArgumentException("The observable was initialized for another observer.")
        if (observed.containsKey(o.__obs.observableName))
            throw IllegalArgumentException("Duplicate observable name : ${o.__obs.observableName}")
        (observed as MutableMap<String,Observable>)[o.__obs.observableName] = o
    }

    /** Should not be called directly, but only from Observable#__notify. */
    internal open fun process (e: Event) {
        if (e.source.__obs.observer != this)
            throw IllegalArgumentException("This is not the observer associated to the event source")
    }
}

/** Stores the different attributes of an observation relation.
 * Is used for factorization, so that {@see Observable}s need to store only one observation-related attribute.
 */
data class ObservationRelation (val observable: Observable,
                                val observer: Observer,
                                val observableName: String)







