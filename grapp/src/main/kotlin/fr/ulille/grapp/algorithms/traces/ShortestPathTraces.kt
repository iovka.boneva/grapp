/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.traces

import fr.ulille.grapp.algorithms.Bellman
import fr.ulille.grapp.algorithms.BellmanFord
import fr.ulille.grapp.algorithms.Dijkstra
import fr.ulille.grapp.algorithms.observation.*
import fr.ulille.grapp.graph.Graph
import fr.ulille.grapp.graph.Vertex
import fr.ulille.grapp.io.Util
import java.lang.IllegalArgumentException

object DijkstraTraces {

    /** The columns of the execution trace are:
     * - s : the vertex of the current iteration
     * - Dist(x)/Pred(x) for all nodes x
     * - possibly P : the set of permanent nodes
     */
    fun distSlashPredTrace (graph: Graph, startNode: Vertex, includePerm: Boolean = false)
            : Triple<ExecutionTrace, Map<Vertex, Double>, Map<Vertex, Vertex>> {
        val algorithm = Dijkstra(graph)
        val et = ExecutionTrace()

        val sommetCourant = OVar<Vertex>()
        val dist = OMap<Vertex, Double>()
        val pred = OMap<Vertex, Vertex>()
        val perm = OSet<Vertex>()

        algorithm.initObservation(algorithm, et, "Dijkstra algorithm")

        sommetCourant.initObservation(sommetCourant, et, "s")
        et.addColumn("s") { obs ->
            (obs.getObserved("s") as Var<Vertex>).value ?: ""
        }

        dist.initObservation(dist, et, "Dist")
        pred.initObservation(pred, et, "Pred")
        for (v in graph.vertexSet()) {
            et.addColumn("Dist($v)/Pred($v)") {
                val d = (et.getObserved("Dist") as Map<Vertex, Double>)[v]
                val p = (et.getObserved("Pred") as Map<Vertex, Vertex>)[v]
                val ds = if (d == Double.POSITIVE_INFINITY) "inf" else Util.weightToString(d!!)
                val ps = p ?: "-"
                "$ds/$ps"
            }
        }
        perm.initObservation(perm, et, "P")
        if (includePerm)
            et.addColumn("P") { obs ->
                val permSet = obs.getObserved("P") as OSet<Vertex>
                permSet.joinToString (prefix="{", separator=", ", postfix="}")
            }

        algorithm.run(startNode, dist, pred, perm, sommetCourant, Var<Vertex>())
        return Triple(et, dist, pred)
    }


}

object ShortestPathTraces {

    /** Generates an execution trace for a run of the Dijksta algorithm.
     * The string parameters <something>ObservedName allow to indicate the names of the columns that corresponds to the observed objects.
     * If such string starts with underscore, then this column would not be included in the execution trace table.
     */
    fun dijkstraExecutionTrace (graph: Graph,
                                startNode : Vertex,
                                permObservedName : String = "P",
                                distObservedName : String = "Dist",
                                predObservedName : String = "Pred",
                                currentVertexObservedName : String = "s") : ExecutionTrace {

        if (permObservedName.isEmpty() || distObservedName.isEmpty() || predObservedName.isEmpty()
                || currentVertexObservedName.isEmpty())
            throw IllegalArgumentException("the observed names cannot be empty")

        val algorithm = Dijkstra(graph)
        val observer = ExecutionTrace()

        val perm = OSet<Vertex>()
        val dist = OMap<Vertex, Double>()
        val pred = OMap<Vertex, Vertex>()
        val sommetCourant = OVar<Vertex>()
        val sommetSecondaire = Var<Vertex>()

        algorithm.initObservation(algorithm, observer, "Dijkstra algorithm")
        initObserver(observer, graph,
                Triple(ObservableOutputKind.TO_STRING, currentVertexObservedName, sommetCourant),
                Triple(ObservableOutputKind.NODE_DISTANCE, distObservedName, dist),
                Triple(ObservableOutputKind.TO_STRING, predObservedName, pred),
                Triple(ObservableOutputKind.TO_STRING, permObservedName, perm));

        algorithm.run(startNode, dist, pred, perm, sommetCourant, sommetSecondaire)
        return observer
    }




    /** {@see runDijkstraForExecutionTraceTable} */
    fun runBellmanForExecutionTraceTable (graph: Graph,
                                          startNode : Vertex,
                                          distObservedName : String = "dist",
                                          predObservedName : String = "predecessor",
                                          currentVertexObservedName : String = "s") : ExecutionTrace {

        TODO("The current implementation of Bellman does not correspond to the algorithm we teach.")

        if (distObservedName.isEmpty() || predObservedName.isEmpty()
                || currentVertexObservedName.isEmpty())
            throw IllegalArgumentException("the observed names cannot be empty")

        val algorithm = Bellman(graph)
        val observer = ExecutionTrace()

        val dist = OMap<Vertex, Double>()
        val pred = OMap<Vertex, Vertex>()
        val sommetCourant = OVar<Vertex>()

        algorithm.initObservation(algorithm, observer, "Bellman Algorithm")
        initObserver(observer, graph,
                Triple(ObservableOutputKind.TO_STRING, currentVertexObservedName, sommetCourant),
                Triple(ObservableOutputKind.NODE_DISTANCE, distObservedName, dist),
                Triple(ObservableOutputKind.TO_STRING, predObservedName, pred))

        algorithm.run(startNode, dist, pred, sommetCourant)
        return observer
    }


    /** {@see runDijkstraForExecutionTraceTable} */
    fun runBellmanFordForExecutionTraceTable (graph: Graph,
                                              startNode : Vertex,
                                              distObservedName : String = "dist",
                                              predObservedName : String = "predecessor",
                                              currentIterationObservedName : String = "iteration") : ExecutionTrace {

        TODO("Not tested")

        if (distObservedName.isEmpty() || predObservedName.isEmpty()
                || currentIterationObservedName.isEmpty())
            throw IllegalArgumentException("the observed names cannot be empty")

        val algorithm = BellmanFord(graph)
        val observer = ExecutionTrace()

        val dist = OMap<Vertex, Double>()
        val pred = OMap<Vertex, Vertex>()
        val iteration = OVar<Int>()

        algorithm.initObservation(algorithm, observer, "Bellman-Ford Algorithm")
        initObserver(observer, graph,
                Triple(ObservableOutputKind.TO_STRING, currentIterationObservedName, iteration),
                Triple(ObservableOutputKind.NODE_DISTANCE, distObservedName, dist),
                Triple(ObservableOutputKind.TO_STRING, predObservedName, pred))

        algorithm.run(startNode, dist, pred, iteration)
        return observer
    }

    private enum class ObservableOutputKind {NODE_DISTANCE, TO_STRING}

    private fun initObserver (observer: ExecutionTrace,
                              graph: Graph,
                              vararg observablesInfo: Triple<ObservableOutputKind, String, Observable>) {

        for ((kind, name, observable) in observablesInfo) {
            observable.initObservation(observable, observer, name)
            if (! name.startsWith("_")) {
                when (kind) {
                    ObservableOutputKind.NODE_DISTANCE ->
                        for (s in graph.vertexSet()) {
                            val (name, valueExtractor) = nodeDistanceValueExtractor(name)(s)
                            observer.addColumn(name, valueExtractor)
                        }
                    ObservableOutputKind.TO_STRING ->
                        observer.addColumn(name, toStringValueExtractor(name))
                }
            }
        }
    }

    private fun nodeDistanceValueExtractor (distanceObservableName : String) : (Vertex) -> Pair<String, (Observer)->String> =
            { node: Vertex ->
                Pair("$distanceObservableName(${node})") { o: Observer ->
                    val d = (o.getObserved(distanceObservableName) as Map<Vertex, Double>)[node];
                    if (d == Double.POSITIVE_INFINITY) "inf" else d.toString()
                }
            }

    private fun toStringValueExtractor (observedName : String) : (Observer) -> String =
            { o ->
                val result = o.getObserved(observedName).toString()
                if (result == "null") "" else result
            }
}