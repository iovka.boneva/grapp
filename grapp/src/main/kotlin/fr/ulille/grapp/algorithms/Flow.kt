/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */


package fr.ulille.grapp.algorithms

import fr.ulille.grapp.graph.*
import org.jgrapht.alg.flow.EdmondsKarpMFImpl

interface MaximumFlowAlgorithm {
    /** Computes a maximum flow and a minimum cut.
     * @param source The source node, input parameter.
     * @param sink The sink node, input parameter.
     * @param maximumFlow The resulting maximum flow map, output parameter.
     * @param sourcePartition The resulting source-side part of the minimum cut, output parameter.
     * @param sinkPartition The resulting sink-side part of the minimum cut, output parameter.
     * @return the value of the maximum flow
     */
    fun compute (source: Vertex, sink: Vertex,
                 maximumFlow: MutableMap<Edge, Double>,
                 sourcePartition : MutableSet<Vertex> = mutableSetOf(), sinkPartition : MutableSet<Vertex> = mutableSetOf()) : Double
}

/** Non observable implementation of the algorithm.
 * Delegates the computation to org.jgrapht.alg.flow.EdmondsKarpMFImpl.
 */
class EdmondsKarpAlgorithm(val graph: Graph) : MaximumFlowAlgorithm {
    val algorithm = EdmondsKarpMFImpl(graph)

    override fun compute(source: Vertex, sink: Vertex,
                         maximumFlow: MutableMap<Edge, Double>,
                         sourcePartition: MutableSet<Vertex>, sinkPartition : MutableSet<Vertex>) : Double {
        val flow = algorithm.getMaximumFlow(source, sink)
        maximumFlow.putAll(flow.flowMap)
        sourcePartition.addAll(algorithm.sourcePartition)
        sinkPartition.addAll(algorithm.sinkPartition)
        return flow.value
    }
}

