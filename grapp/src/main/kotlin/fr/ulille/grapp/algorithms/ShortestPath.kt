/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.algorithms.observation.InitializationCompleteEvent
import fr.ulille.grapp.algorithms.observation.LoopCompleteEvent
import fr.ulille.grapp.algorithms.observation.Var
import fr.ulille.grapp.graph.*
import org.jgrapht.Graphs
import java.lang.NullPointerException

interface OptimalPathAlgorithm {
    /** Computes a shortest path from given vertex to all other vertices of a graph.
     * @param vertex The initial vertex, input parameter.
     * @param dist The resulting distance map: with every vertex associates its distance from the initial vertex. Output parameter.
     * @param pred The resulting predecessors map: with every vertex associates its predecessor in the the shortest paths tree. Output parameter.
     */
    fun compute (
        vertex: Vertex,
        dist: MutableMap<Vertex,Double>,
        pred: MutableMap<Vertex,Vertex>)
}

enum class PathCombinationOperation {
    Add, Mult;

    fun combine (d1: Double, d2: Double) : Double {
        return when (this) {
            Add -> d1 + d2
            Mult -> d1 * d2
        }
    }
}
enum class OptimalityCriterion {
    Min, Max;

    /** Compares the two values wrt the criterion
     * @return 0 if the values are equivalent, a positive number if the first is optimal compared to the second, a negative number otherwise. */
    fun compare (d1: Double, d2: Double) : Int {
        return when (this) {
            Min -> if (d1 == d2) 0 else if (d1 < d2) 1 else -1
            Max -> if (d1 == d2) 0 else if (d1 > d2) 1 else -1
        }
    }
}

class Dijkstra (graph: Graph) : OptimalPathAlgorithm, GraphAlgorithm(graph) {

    override fun compute (vertex: Vertex, dist: MutableMap<Vertex, Double>, pred: MutableMap<Vertex, Vertex>) {
        run(vertex, dist=dist, pred=pred)
    }

    /** An observable implementation of the algorithm.
     * @param vertex The initial vertex, input parameter.
     * @param dist Possibly observable map to store the resulting distance map. Output parameter.
     * @param pred Possibly observable map to store the resulting predecessors map. Output parameter.
     * @param perm Possibly observable set used during computation to store the permanent vertices, ie those for which the distance is known.
     * @param vertexCurrentIteration Possibly observable vertex holder used to store the vertex of the current iteration.
     * @param vertexSuccessor Possibly observable vertex holder used to store the successors of the current iteration vertex that are being explored by the algorithm.
     */
    fun run (
        vertex: Vertex,
        dist: MutableMap<Vertex, Double> = mutableMapOf(),
        pred: MutableMap<Vertex, Vertex> = mutableMapOf(),
        perm: MutableSet<Vertex> = mutableSetOf(),
        vertexCurrentIteration: Var<Vertex> = Var(),
        vertexSuccessor: Var<Vertex> = Var()
    ) {

        if (!graph.vertexSet().contains(vertex))
            throw AlgorithmError("Unknown vertex : $vertex")

        perm.clear()
        dist.clear()
        pred.clear()

        dist.putAll(graph.vertexSet().associateWith { if (it == vertex) 0.0 else Double.POSITIVE_INFINITY })
        val temp = graph.vertexSet().minus(perm).toMutableSet()

        __notify(InitializationCompleteEvent(this))

        while (temp.isNotEmpty()) {
            vertexCurrentIteration.value = dist.entries.filter{ it.key in temp }.minByOrNull { it.value }!!.key
            perm.add(vertexCurrentIteration.value!!)
            temp.remove(vertexCurrentIteration.value)

            val edgeWeightMap = nonVisitedSuccessorsWithWeights(graph, vertexCurrentIteration.value!!) { v -> ! perm.contains(v) }
            for (edgeWeight in edgeWeightMap) {
                vertexSuccessor.value = edgeWeight.key
                val altDist = dist[vertexCurrentIteration.value]!! + edgeWeight.value
                if (dist[vertexSuccessor.value]!! > altDist) {
                    dist[vertexSuccessor.value as Vertex] = altDist
                    pred[vertexSuccessor.value as Vertex] = vertexCurrentIteration.value as Vertex
                }
            }
            __notify(LoopCompleteEvent(this))
        }
    }

    companion object {
        fun nonVisitedSuccessorsWithWeights (graph : Graph, vertex: Vertex, keepPredicate : (Vertex) -> Boolean) : Map<Vertex, Double> {
            val map = mutableMapOf<Vertex, Double>()
            val x = graph.outgoingEdgesOf(vertex)
            for (edge in graph.outgoingEdgesOf(vertex)) {
                if (edge.weight < 0)
                    throw AlgorithmError("Dijkstra algorithm cannot handle graph with negative weights")
                val opposite = Graphs.getOppositeVertex(graph, edge, vertex)
                if (keepPredicate(opposite))
                    map[opposite] = edge.weight
            }
            return map
        }
    }
}



class Bellman (graph: Graph,
               val pathCombinationOperation: PathCombinationOperation = PathCombinationOperation.Add,
               val optimalityCriterion: OptimalityCriterion = OptimalityCriterion.Min)
    : OptimalPathAlgorithm, GraphAlgorithm(graph) {

    val topologicalOrder = topologicalOrder(graph)

    override fun compute(vertex: Vertex, dist: MutableMap<Vertex, Double>, pred: MutableMap<Vertex, Vertex>) {
        run(vertex, dist=dist, pred=pred)
    }

    /** An observable implementation of the algorithm.
     * @param vertex The initial vertex, input parameter.
     * @param dist Possibly observable map to store the resulting distance map. Output parameter.
     * @param pred Possibly observable map to store the resulting predecessors map. Output parameter.
     * @param vertexCurrentIteration Possibly observable vertex holder used to store the vertex of the current iteration.
     */
    fun run(vertex: Vertex,
            dist: MutableMap<Vertex, Double> = mutableMapOf(),
            pred: MutableMap<Vertex, Vertex> = mutableMapOf(),
            vertexCurrentIteration: Var<Vertex> = Var()) {

        dist.clear()
        pred.clear()

        val initialDistance = when (optimalityCriterion) {
            OptimalityCriterion.Min -> Double.POSITIVE_INFINITY
            OptimalityCriterion.Max -> Double.NEGATIVE_INFINITY
        }
        val neutralElement = when (pathCombinationOperation) {
            PathCombinationOperation.Add -> 0.0
            PathCombinationOperation.Mult -> 1.0
        }
        val optimalityComparator = Comparator<Edge> { o1, o2 ->
            val d1 = pathCombinationOperation.combine(dist[o1.src]!!, o1.weight)
            val d2 = pathCombinationOperation.combine(dist[o2.src]!!, o2.weight)
            when (optimalityCriterion) {
                OptimalityCriterion.Min -> d1.compareTo(d2)
                OptimalityCriterion.Max -> - d1.compareTo(d2)
            }
        }


        dist.putAll(graph.vertexSet().associateWith { if (it == vertex) neutralElement else initialDistance })
        __notify(InitializationCompleteEvent(this))

        val nodesIterator = topologicalOrder.iterator()
        nodesIterator.next()
        for (s in topologicalOrder.iterator()) {
            vertexCurrentIteration.value = s

            val incomingEdges = graph.incomingEdgesOf(s)
            if (pathCombinationOperation == PathCombinationOperation.Mult && incomingEdges.any { it.weight < 0 })
                throw AlgorithmError("Optimization of product is impossible when the graph contains negative weights")

            val predecessorEdge = graph.incomingEdgesOf(s).minWithOrNull (optimalityComparator)
            if (predecessorEdge != null) {
                dist[vertexCurrentIteration.value!!] = pathCombinationOperation.combine(dist[predecessorEdge.src]!!, predecessorEdge.weight)
                pred[vertexCurrentIteration.value!!] = predecessorEdge.src
            }
            __notify(LoopCompleteEvent(this))
        }
    }

    companion object {
        fun topologicalOrder (graph: Graph) : List<Vertex> {
            val result = mutableListOf<Vertex>()
            val numberPredecessors = LinkedHashMap(graph.vertexSet().associateWith { graph.incomingEdgesOf(it).size })
            try {
                while (numberPredecessors.keys.isNotEmpty()) {
                    val s = numberPredecessors.entries.first { it.value == 0 }.key  // NoSuchElementException possible
                    for (it in graph.outgoingEdgesOf(s).map{ it.tgt })
                        numberPredecessors[it] = numberPredecessors[it]!! - 1
                    numberPredecessors.remove(s)
                    result.add(s)
                }
            } catch (e: NoSuchElementException) {
                throw AlgorithmError("The graph is not acyclic.")
            } catch (e: NullPointerException) {
                throw AlgorithmError("The graph is not acyclic.")
            }
            return result
        }
    }
}

class BellmanFord (graph: Graph) : OptimalPathAlgorithm, GraphAlgorithm(graph) {
    override fun compute(vertex: Vertex, dist: MutableMap<Vertex, Double>, pred: MutableMap<Vertex, Vertex>) {
        run(vertex, dist=dist, pred=pred)
    }

    /** An observable implementation of the algorithm.
     * @param vertex The initial vertex, input parameter.
     * @param dist Possibly observable map to store the resulting distance map. Output parameter.
     * @param pred Possibly observable map to store the resulting predecessors map. Output parameter.
     * @param iteration Possibly observable integer holder used during computation to store the iteration rank. The algorithm perform at most vertex size iterations.
     */
    fun run (vertex: Vertex,
             dist: MutableMap<Vertex,Double> = mutableMapOf(),
             pred: MutableMap<Vertex,Vertex> = mutableMapOf(),
             iteration: Var<Int> = Var()) {

        dist.clear()
        pred.clear()

        dist.putAll(graph.vertexSet().associateWith { if (it == vertex) 0.0 else Double.POSITIVE_INFINITY })
        __notify(InitializationCompleteEvent(this))

        val edgesSet = mutableListOf<Edge>()
        edgesSet.addAll(graph.edgeSet())

        if (!graph.isDirected())
            graph.edgeSet().forEach { edgesSet.add(Edge(it.tgt, it.src, it.weight))}


        val oldDist = mutableMapOf<Vertex,Double>()
        for (i in 1 .. graph.vertexSet().size) {
            iteration.value = i
            oldDist.clear(); oldDist.putAll(dist)
            for (a in edgesSet) {
                val o = a.src
                val b = a.tgt
                val altDist = oldDist[o]!! + a.weight
                if (oldDist[b]!! > altDist) {
                    dist[b] = altDist
                    pred[b] = o
                }
            }
            __notify(LoopCompleteEvent(this))
        }
        for (a in graph.edgeSet()) {
            val o = a.src
            val b = a.tgt
            val altDist = dist[o]!! + a.weight
            if (dist[b]!! > altDist) {
                throw AlgorithmError("Shortest path is not defined in a graph with a negative-weight cycle")
            }
        }
    }
}

