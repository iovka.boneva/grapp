/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.traces

import fr.ulille.grapp.algorithms.observation.*


/** Construct the execution trace for a run of an algorithm.
 * The execution trace is retrieved by {@see #getColumnNames} and {@see #getTableLines}.
 *
 * Usage: Add columns to the observer, init the observation, and run the algorithm. */
class ExecutionTrace : Observer() {

    /** column name -> function that allows to get a string representation of the value of the corresponding column during the execution of the algorithm
     * Typically a column is dedicated to some observed object. For instance, it can be the currently best known distance to some node.
     * The observer knows these objects, and the functions stored in this map define how to ask the observer for the particular value. */
    private val columns = mutableMapOf<String, (Observer)->String>()

    /** The lines of the table being generated.
     * Contains the value of each column for each line of the table. */
    private val table = mutableListOf<Map<String,String>>()

    /** Adds a column name and a function that allows to extract a value for this column at every point of the execution. */
    fun addColumn (name: String, valueExtractor: (Observer) -> String)  {
        columns[name] = valueExtractor
    }

    /** Typically, a new line is being generated for every iteration of the main loop. */
    override fun process(e: Event)  {
        super.process(e)

        val line = mutableMapOf<String,String>()
        if (e is LoopCompleteEvent || e is InitializationCompleteEvent) {
            for (c in columns.entries) {
                line[c.key] = c.value(this)
            }
            table.add(line)
        }
    }

    fun getColumnNames () : List<String> {
        return columns.keys.toList()
    }

    /** Returns the lines of the table and for any line, the value for any of the columns. */
    fun getTableLines () : List<Map<String,String>> {
        return table
    }

    companion object {
        /** Return trace's lines where cells are non empty only when there is a value change. */
        fun cleanLines (lines: List<Map<String,String>>) : List<Map<String,String>> {
            val result = mutableListOf<Map<String,String>>()
            result.add(lines[0])
            for (lineNumber in 1 until lines.size)
                result.add(lines[lineNumber]!!.entries.associate {
                    (k, v) -> Pair(k, if (v != lines[lineNumber-1][k]) v else "")
                })
            return result
        }
    }
}
