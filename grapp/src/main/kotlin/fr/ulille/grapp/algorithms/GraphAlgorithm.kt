/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms

import fr.ulille.grapp.algorithms.observation.Observable
import fr.ulille.grapp.algorithms.observation.ObservableImpl
import fr.ulille.grapp.graph.Graph

/** A superclass of observable graph algorithms.
 *
 * Such algorithms typically implement a run function that takes three kinds of parameters:
 * (1) input parameters (e.g. a start vertex for computing a shortest path);
 * (2) possibly observable output parameters, typically empty collections to store the end result (e.g. an empty map that a shortest path algorithm will populate as the distance map);
 * (3) possibly observable holders used during computation (e.g. a vertex holder that will be used as a variable to store the currently explored vertex in a shortest path algorithm).
 *
 * Whenever an output parameter or a holder parameter is observable, its registered observers are notified in case of state changes, thus allowing to construct e.g. a trace of the algorithm, or possibly an animation.
 * Additionally, the algorithm itself notifies its observers at every significant step, e.g. after initialization, at each loop of the main or of a secondary loop.
 *
 * Observable algorithms can also propose a compute function that accepts only input and output parameters, allowing a simpler use of the algorithm when observation is not needed.
 * +
 */
abstract class GraphAlgorithm (val graph: Graph) : Observable by ObservableImpl() {}

/** For all errors during the execution of algorithms. */
class AlgorithmError (message: String) : RuntimeException(message)