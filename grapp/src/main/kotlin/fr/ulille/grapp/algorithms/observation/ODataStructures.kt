/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.algorithms.observation

import java.util.ArrayDeque
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap
import kotlin.collections.LinkedHashSet

interface ODataStructure : Observable

/** A queue structure to be used in graph traversal algorithms.
 * Offers a common interface for both LIFO and FIFO queues.
 * Offers two implementations: Lifo and Fifo, for DFS and BFS respectively. */
abstract class TraversalQueue<E: Any>() : ArrayDeque<E>() {
    open fun put (e: E) = sqAddAbstract(e)
    open fun get() : E = removeFirst()
    fun see() : E = peekFirst()

    abstract fun sqAddAbstract (e: E)

    override fun toString(): String {
        val s = super.toString()
        return s.substring(0 until s.length-1) + "["
    }
}
class Lifo<E: Any> () : TraversalQueue<E>() {
    override fun sqAddAbstract(e: E) = addFirst(e)
}
class Fifo<E: Any> () : TraversalQueue<E>() {
    override fun sqAddAbstract(e: E) = addLast(e)
}

/** An observable version of the {@see TraversalQueue}, offering two implementations OLifo and OFifo. */
abstract class OTraversalQueue<E:Any>() : TraversalQueue<E>(), Observable by ObservableImpl(), ODataStructure {

    override fun put (e: E) {
        super.put(e)
        __notify(DSAddEvent(this, e))
    }

    override fun get() : E {
        val e = super.get()
        __notify(DSRemoveEvent(this, e))
        return e
    }
}


class OFifo<E:Any> () : OTraversalQueue<E>() {
    override fun sqAddAbstract(e: E) = addLast(e)
    override fun toString(): String {
        return "file"+super.toString()
    }
}
class OLifo<E:Any> () : OTraversalQueue<E>() {
    override fun sqAddAbstract(e: E) = addFirst(e)
    override fun toString(): String {
        return "pile"+super.toString()
    }
}




class OSet<E:Any>() : LinkedHashSet<E>(), Observable by ObservableImpl(), ODataStructure {

    override fun add(element: E) : Boolean {
        val result = super.add(element)
        if (result)
            __notify(DSAddEvent(this, element))
        return result
    }

    override fun remove (element: E) : Boolean {
        val result = super.remove(element)
        if (result) __notify(DSRemoveEvent(this, element))
        return result
    }

    override fun addAll (elements: Collection<E>) : Boolean{
        val result = super.addAll(elements)
        if (result)
            __notify(DSInitEvent(this, elements))
        return result
    }

    override fun toString(): String {
        return joinToString (", ", "{", "}")
    }
}


class OList<E:Any>() : ArrayList<E>(), Observable by ObservableImpl(), ODataStructure {

    override fun add(element: E) : Boolean {
        val result = super.add(element)
        if (result) __notify(DSAddEvent(this, element))
        return result;
    }

    override fun remove (element: E) : Boolean {
        val result = super.remove(element)
        if (result) __notify(DSRemoveEvent(this, element))
        return result
    }

    override fun addAll (elements: Collection<E>) : Boolean {
        val result = super.addAll(elements)
        if (result) __notify(DSInitEvent(this, elements))
        return result
    }
}

class OMap <K:Any,V:Any>() : LinkedHashMap<K,V>(), Observable by ObservableImpl(), ODataStructure {

    override fun put(key: K, value: V): V? {
        val result = super.put(key,value)
        __notify(DSAddEvent(this, Pair(key, value)))
        return result
    }

    override fun remove (key: K):  V? {
        val result = super.remove(key)
        __notify(DSRemoveEvent(this, key))
        return result
    }

    override fun putAll (from: Map<out K,V>) {
        super.putAll(from)
        __notify(DSInitEvent(this, from))
    }

    override fun toString(): String {
        return entries.joinToString { "(${it.key})=${it.value}" }
    }
}


open class Var<T> {
    open var value: T? = null

    override fun toString(): String {
        return value.toString()
    }

}

class OVar<T> () : Var<T>(), Observable by ObservableImpl(), ODataStructure {
    override var value: T? = null
        set(value) {
            field = value
            __notify(VariableEvent(this, value))
        }
}