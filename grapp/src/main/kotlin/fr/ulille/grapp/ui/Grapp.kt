/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.output.CliktConsole
import com.github.ajalt.clikt.output.TermUi
import com.github.ajalt.clikt.output.defaultCliktConsole
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.groups.mutuallyExclusiveOptions
import com.github.ajalt.clikt.parameters.groups.provideDelegate
import com.github.ajalt.clikt.parameters.groups.required
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.choice
import fr.ulille.grapp.algorithms.AlgorithmError
import fr.ulille.grapp.algorithms.OptimalityCriterion
import fr.ulille.grapp.algorithms.PathCombinationOperation
import fr.ulille.grapp.graph.*
import fr.ulille.grapp.io.*
import java.lang.ClassCastException
import java.net.URL
import java.net.URLClassLoader
import fr.ulille.grapp.ui.OutputFormatting.format
import java.io.*
import java.net.ServerSocket
import java.net.Socket
import java.util.regex.Pattern

/** Runs the application in REPL mode. */
object Grapp {
    // Notes on using clikt in repl mode.
    //
    // It would be preferable to have Grapp as the main command (inherits from CliktCommand),
    // but then we must call parse in run() in order to
    // execute the subcommands and run would enter infinite recursion
    // I do not see how to make Grapp a CliktCommand
    //
    // The solution adopted is to have a dummy main command, and its subcommands are the actual commands of Grapp.
    // The REPL loop is implemented manually by the Grapp.run function
    //
    // Also, it is not possible to use CliktCommand.main to parse and run the subcommands, as CliktCommand.main performs a system exit in case of error
    // The solution adopted is to reproduce the code of CliktCommand.main (exception handling), but w/o the system exit.
    // Then we need to call echo which is protected in CliktCommand, so we expose it by a public function myEcho in MainCommand

    val graphsAndResults = GraphsAndResults()
    var isStandalone = false

    fun run (c: CliktConsole) {

        MainCommand.context {
            console = c
            localization = GrappFrenchLocalization
        }

        MainCommand.subcommands(Help, Exit, Load, List, Draw, ShortestPath, OptimalPath, MaximumFlow, Traversal, FindPath, GenYaml)

        var line : String?
        while (true) {
            val prompt = if (isStandalone) "grapp:> " else ""
            line = c.promptForLine(prompt, false)
            if (line != null && line.startsWith("#")) continue
            if (line == null || line.trim() == "exit") break
            line = line.trim()
            try {
                MainCommand.parse(line.split(Regex("\\s+")))
            } catch (e: GrappError) {
                MainCommand.myEcho(e.message, err=true)
            } catch (e: ProgramResult) {
                // exitProcessMpp(e.statusCode)
            } catch (e: PrintHelpMessage) {
                MainCommand.myEcho(e.command.getFormattedHelp())
                // exitProcessMpp(if (e.error) 1 else 0)
            } catch (e: PrintCompletionMessage) {
                MainCommand.myEcho(e.message)
                // exitProcessMpp(0)
            } catch (e: PrintMessage) {
                MainCommand.myEcho(e.message)
                //exitProcessMpp(if (e.error) 1 else 0)
            } catch (e: UsageError) {
                MainCommand.myEcho("Commande inconnue \"${line}\"", err=true)
                Help.run()
                // exitProcessMpp(e.statusCode)
            } catch (e: CliktError) {
                MainCommand.myEcho(e.message, err = true)
                // exitProcessMpp(1)
            } catch (e: Abort) {
                MainCommand.myEcho(MainCommand.currentContext.localization.aborted(), err = true)
                //exitProcessMpp(if (e.error) 1 else 0)
            } catch (e: Exception) {
                when (e) {
                    is ImplicitGraphFormatError,
                    is AlgorithmError -> MainCommand.myEcho("Erreur: ${e.message}", err = true)
                    else -> {
                        MainCommand.myEcho("Une erreur inattendue s'est produite : ${e.javaClass} : ${e.message}", err=true)
                        e.printStackTrace()
                    }
                }

            }
        }
    }

    fun getGraph (idOrName: String) : Graph {
        return graphsAndResults.getGraph(idOrName) ?: throw GrappError(Messages.errorUnknownGraphOrResult)
    }

    fun knowsGraphWithName (name: String) : Boolean {
        return !name.matches(GraphsAndResults.graphIdentifierFormat) && graphsAndResults.getGraph(name) != null
    }
    fun getResult (id: String) : Result {
        return graphsAndResults.getResult(id) ?: throw GrappError(Messages.errorUnknownGraphOrResult)
    }

    fun getNode (g: Graph, n: String) : String {
        return if (g.containsVertex(n)) n else throw GrappError(Messages.errorUnknownNode)
    }
}

object MainCommand : CliktCommand (name="", epilog = Messages.helpEpilog) {

    override fun run() {}
    fun myEcho(message: String?, err: Boolean = false) {
        if (message != null) {
            super.echo(format("\n${message}\n"), true, err, currentContext.console.lineSeparator)
        }
    }
}

object Exit : CliktCommand (name="exit", help=Messages.exitHelp) {
    override fun run() {}
}

abstract class MyCliktCommand (help: String, printHelpOnEmptyArgs: Boolean = true)
    : CliktCommand (help=help, printHelpOnEmptyArgs=printHelpOnEmptyArgs) {
    init{
        context { localization = GrappFrenchLocalization }
    }

    fun fecho (message: String = "", err: Boolean = false) {
        super.echo(format("\n$message\n"), trailingNewline = true, err, currentContext.console.lineSeparator)
    }
}

object Help : MyCliktCommand (help=Messages.helpHelp, printHelpOnEmptyArgs = false) {

    override fun run() {
        val originalHelp = MainCommand.getFormattedHelp()
        val start = originalHelp.findAnyOf(listOf(Messages.availableCommands))!!.first
        val messageToPrint = originalHelp.subSequence(start until originalHelp.length)
        fecho(messageToPrint.toString())
    }
}

object Load : MyCliktCommand(help = Messages.loadHelp) {
    private val fileName by argument(help=Messages.loadFilenameHelp)
    private val reload by option("-reload", help=Messages.loadReloadHelp).flag()
    private val implicit by option("-implicit", help=Messages.loadImplicitHelp).flag()

    override fun run () {
        try {
            val g = if (!implicit)
                YamlParser.parse(File(fileName).inputStream())
            else
                loadImplicitGraph(fileName)
            g.setName(GraphsAndResults.normalizeGraphName(g.getName()))
            if (Grapp.graphsAndResults.add(g, reload))
                fecho(Messages.loadOk)
            else {
                if (!reload)
                    fecho(Messages.errorLoadGraphExistsUseReload + " (nom : ${g.getName()})")
                fecho(Messages.errorLoadAborted, err = true)
            }
        } catch (e: YAMLFormatError) {
            throw GrappError(e.message ?: "")
        } catch (e: FileNotFoundException) {
            throw GrappError(Messages.errorFileNotFound + " (${fileName})")
        }
    }

    private fun loadImplicitGraph (className: String) : Graph {
        val name = if (className.endsWith(".class")) className.substring(0 until className.length-5) else className

        val classLoader = Grapp::class.java.classLoader
        val urlClassLoader = URLClassLoader(arrayOf(URL("file:${System.getProperty("user.dir")}/")), classLoader)
        try {
            val c = urlClassLoader.loadClass(name)
            val graph = ImplicitGraphsUtil.getGraph(c.getDeclaredConstructor().newInstance() as ImplicitGraph)
            (graph as ImplicitGraphInterface).explore(graph)
            return graph
        } catch (e: ClassCastException) {
            throw ImplicitGraphFormatError(ImplicitGraphsUtil.missingImplements)
        } catch (e: IllegalAccessException) {
            throw GrappError(ImplicitGraphsUtil.errorImplicitGraphNonPublic)
        }

    }
}

object List : MyCliktCommand(help=Messages.listHelp, printHelpOnEmptyArgs = false) {
    override fun run() {
        if (Grapp.graphsAndResults.isEmpty()) {
            fecho(Messages.noLoadedGraphs)
        } else {
            fecho(Messages.listKnownGraphsWithResults)
            fecho(ListCommandOutputFormatting.prettyPrint(Grapp.graphsAndResults.graphsAndTheirResults()))
        }
    }
}

class OutputOptions : OptionGroup("Output options") {
    val csv by option("-csv", metavar=Messages.fileNameMeta, help=Messages.csvFileHelp)
    val silent by option("-silent", help=Messages.silentHelp).flag()

    fun writeCsv (result: Result) : String {
        val f = if (csv!!.endsWith(".csv")) csv!! else "${csv!!}.csv"
        try {
            CsvIo.csvPrint(result, f)
            return Messages.algorithmCsvOk
        } catch (e: FileNotFoundException) {
            throw GrappError(Messages.errorFileNotFound + " ($f)")
        } catch (e: IOException) {
            throw GrappError(Messages.errorIO + " ($f)")
        }
    }
}

open class CommonAlgorithmOptions(vararg allowedAlgorithms: String): OptionGroup("Algorithm options") {
    val algorithm by option("-algorithm", help=Messages.algorithmNameHelp)
        .choice(*allowedAlgorithms)
        .required()
    val on by option("-on", metavar=Messages.graphIdOrNameMeta, help=Messages.graphIdOrNameHelp)
        .required()
}

open class ShortestPathAndTraversalAlgorithmOptions (vararg allowedAlgorithms: String) : CommonAlgorithmOptions(*allowedAlgorithms) {
    val from by option("-from", metavar=Messages.nodeMeta, help=Messages.shortestPathOrTraversalStartNodeHelp)
        .required()
}

class OptimalPathAlgorithmOptions (vararg allowedAlgorithms: String) : ShortestPathAndTraversalAlgorithmOptions(*allowedAlgorithms) {

    val optimize by option("-optimize", help=Messages.optimalPathCriterionHelp)
        .choice(*OptimalityCriterion.values().map{ it.name }.toTypedArray())
        .required()
    val operator by option("-operation", help=Messages.optimalPathOperatorHelp)
        .choice(*PathCombinationOperation.values().map{ it.name }.toTypedArray())
        .default(PathCombinationOperation.Add.toString())
}

class MaximumFlowAlgorithmOptions (vararg allowedAlgorithms: String) : CommonAlgorithmOptions(*allowedAlgorithms) {
    val source by option("-source", help=Messages.maximumFlowSourceHelp, metavar=Messages.nodeMeta)
        .required()
    val sink by option("-sink", help=Messages.maximumFlowSinkHelp, metavar=Messages.nodeMeta)
        .required()
}


object ShortestPath : MyCliktCommand (help=Messages.shortestPathHelp) {

    private val algorithmOptions by ShortestPathAndTraversalAlgorithmOptions(*ShortestPathAlgorithmName.values().map{ it.name }.toTypedArray())
    private val outputOptions by OutputOptions()

    override fun run () {
        val graph = Grapp.getGraph(algorithmOptions.on)
        val result = AlgorithmsUtil.runOptimalPathAlgorithm(
            algoName = ShortestPathAlgorithmName.valueOf(algorithmOptions.algorithm),
            graph = graph,
            node = Grapp.getNode(graph, algorithmOptions.from))
        Grapp.graphsAndResults.add(result)
        if (!outputOptions.silent)
            fecho(OutputFormatting.prettyPrint(result))
        else
            fecho(Messages.shortestPathOk)
        if (outputOptions.csv != null)
            fecho(outputOptions.writeCsv(result))
    }
}

object OptimalPath : MyCliktCommand (help = Messages.optimalPathHelp) {

    private val algorithmOptions by OptimalPathAlgorithmOptions(*BellmanAlgorithmName.values().map{ it.name }.toTypedArray())
    private val outputOptions by OutputOptions()

    override fun run () {
        val graph = Grapp.getGraph(algorithmOptions.on)
        val result = AlgorithmsUtil.runOptimalPathAlgorithm(
            algoName = BellmanAlgorithmName.valueOf(algorithmOptions.algorithm),
            graph = graph,
            node = Grapp.getNode(graph, algorithmOptions.from),
            combinationOperator = PathCombinationOperation.valueOf(algorithmOptions.operator),
            optimalityCriterion = OptimalityCriterion.valueOf(algorithmOptions.optimize))

        Grapp.graphsAndResults.add(result)
        if (!outputOptions.silent)
            fecho(format(OutputFormatting.prettyPrint(result)))
        else
            fecho(Messages.optimalPathOk)
        if (outputOptions.csv != null)
            fecho(outputOptions.writeCsv(result))
    }
}

object Traversal : MyCliktCommand(help=Messages.traversalHelp) {

    private val algorithmOptions by ShortestPathAndTraversalAlgorithmOptions(*TraversalAlgorithmName.values().map{it.name}.toTypedArray())

    private val outputOptions by OutputOptions()
    override fun run () {
        val graph = Grapp.getGraph(algorithmOptions.on)
        val result = AlgorithmsUtil.runGraphTraversalAlgorithm(
            algoName = TraversalAlgorithmName.valueOf(algorithmOptions.algorithm),
            graph = graph,
            node = Grapp.getNode(graph, algorithmOptions.from))

        Grapp.graphsAndResults.add(result)
        if (!outputOptions.silent)
            fecho(OutputFormatting.prettyPrint(result))
        else
            fecho(Messages.traversalOk)
        if (outputOptions.csv != null)
            fecho(outputOptions.writeCsv(result))
    }
}

object FindPath : MyCliktCommand (help=Messages.findPathHelp)  {

    private val on by option("-on", metavar=Messages.graphIdOrNameMeta, help=Messages.graphIdOrNameHelp)
        .required()
    private val from by option("-from", metavar=Messages.nodeMeta, help=Messages.findPathFromNodeHelp)
        .required()
    private val to by option("-to", metavar=Messages.nodeMeta, help=Messages.findPathToNodeHelp)
        .required()
    private val outputOptions by OutputOptions()

    override fun run() {
        if (!on.matches(GraphsAndResults.resultIdentifierFormat))
            throw GrappError(Messages.errorFindPathArgumentNotTree)
        val result = Grapp.getResult(on)
        if (result !is OptimalPathResult && result !is TraversalResult)
            throw GrappError(Messages.errorFindPathArgumentNotTree)
        val treeResult = result as TreeResult
        val fromNode = Grapp.getNode(result.graph, from)
        val toNode = Grapp.getNode(result.graph, to)

        val pathResult = AlgorithmsUtil.runFindPathAlgorithm(treeResult, fromNode, toNode)
        Grapp.graphsAndResults.add(pathResult)
        fecho(OutputFormatting.prettyPrint(pathResult, outputOptions.silent))
        if (outputOptions.csv != null)
            fecho(outputOptions.writeCsv(pathResult))
    }

}

object MaximumFlow : MyCliktCommand(help=Messages.maximumFlowHelp) {

    private val algorithmOptions by MaximumFlowAlgorithmOptions(*MaximumFlowAlgorithmName.values().map{it.name}.toTypedArray())
    private val outputOptions by OutputOptions()

    override fun run () {
        val algo = MaximumFlowAlgorithmName.valueOf(algorithmOptions.algorithm)
        val graph = Grapp.getGraph(algorithmOptions.on)
        val source = Grapp.getNode(graph, algorithmOptions.source)
        val sink = Grapp.getNode(graph, algorithmOptions.sink)
        val result = AlgorithmsUtil.runMaximumFlowAlgorithm(algo, graph, source, sink)
        Grapp.graphsAndResults.add(result)
        fecho(OutputFormatting.prettyPrint(result, outputOptions.silent))
        if (outputOptions.csv != null) outputOptions.writeCsv(result)
    }
}

object Draw : MyCliktCommand(printHelpOnEmptyArgs = true, help=Messages.drawHelp) {
    private val graphOrResult by mutuallyExclusiveOptions(
        option("-graph", help=Messages.graphIdOrNameHelp, metavar = Messages.graphIdOrNameMeta),
        option("-result", help=Messages.drawResultIdHelp, metavar = Messages.resultIdMeta)
    ).required()
    val to by option("-to", help=Messages.drawOutputFileNameHelp, metavar = Messages.fileNameMeta)
        .required()

    override fun run() {
        val dotFileName = "$to.dot"
        val pngFileName = "$to.png"
        val graph : Graph
        var result: Result? = null
        try {
            if (graphOrResult.matches(GraphsAndResults.resultIdentifierFormat)) {
                result = Grapp.getResult(graphOrResult)
                graph = result.graph
            } else {
                graph = Grapp.getGraph(graphOrResult)
            }
            DotIo(graph, result).serialize(dotFileName) // IOException possible
            when (CommandLineUtil.executeDotToPng(dotFileName, pngFileName)) {
                0 -> fecho(Messages.drawOk)
                1 -> throw GrappError(Messages.errorMissingGraphviz)
                2 -> throw GrappError(Messages.errorDotIO)
            }
        } catch (e: IOException) {
            throw GrappError(Messages.errorIO + " ${e.message}")
        }
    }
}

object GenYaml : MyCliktCommand(printHelpOnEmptyArgs = true, help=Messages.genYamlHelp) {
    private val graphType by option("-graphType", help=Messages.genYamlGraphTypeHelp)
        .choice(*GrappType.allowedStrings)
        .required()
    private val nodesAndEdges by argument(Messages.genYamlNodesAndEdges)
        .multiple(required=true)
    private val to by option("-to", help=Messages.genYamlToHelp)
        .required()

    override fun run () {
        val nodes = mutableSetOf<String>()
        val edges = mutableListOf<Pair<String,String>>()

        for (x in nodesAndEdges) {
            if (x.contains("-")) {
                val st = x.split("-")
                if (st.size != 2)
                    throw GrappError("${Messages.errorGenYamlInvalidEdgeDefinition} : $x")
                edges.add(Pair(st[0], st[1]))
            } else {
                nodes.add(x)
            }
        }
        val incorrectNodeName = nodes.firstOrNull { !it.matches(YamlGraph.graphNodesRegex) }
        if (incorrectNodeName != null)
            throw GrappError("${Messages.errorGenYamlIncorrectNodeName} : $incorrectNodeName")

        val incorrectEdge = edges.firstOrNull { ! nodes.contains(it.first) || ! nodes.contains(it.second) }
        if (incorrectEdge != null)
            throw GrappError("${Messages.errorGenYamlInvalidEdgeDefinition} : $incorrectEdge")

        val g = Graphs.graphFromNodeSet(GrappType.fromString(graphType), nodes)
        for (e in edges) {
            if (g.isWeighted())
                Graphs.addEdge(g, e.first, e.second, 0.0)
            else
                Graphs.addEdge(g, e.first, e.second)
        }
        g.setName("A COMPLETER")
        g.setDescr("A COMPLETER")
        val o = StringBuilder()
        YamlSerializer.serialize(g, o)
        val fileName = if (to.endsWith(".yaml")) to else "$to.yaml"
        try {
            File(fileName).writeText(o.toString()) // IOException possible
            fecho(Messages.genYamlOk)
        } catch (e: IOException) {
            throw GrappError(Messages.errorIO + " ${e.message}")
        }
    }
}

class GrappError(message: String) : RuntimeException("Erreur: $message")

class CliktConsoleOnSocket (val ss: ServerSocket): CliktConsole {
    override val lineSeparator: String = "\n"

    override fun print(text: String, error: Boolean) {
        println(text)
    }

    override fun promptForLine(prompt: String, hideInput: Boolean): String {
        print(prompt)
        val s = ss.accept()
        val inputStream = s.getInputStream().bufferedReader()
        val command = inputStream.readLine()
        inputStream.close()
        s.close()
        return command
    }
}

fun main(vararg args: String) {
    if (args.size == 1 && args[0] == "server") {
        // On démarre le serveur
        try {
            val ss = ServerSocket(12345)
            println(Messages.grappIntro)
            Grapp.run(CliktConsoleOnSocket(ss))
            ss.close()
        } catch (e: java.net.BindException) {
            println(Messages.errorTwoServers)
        }
    } else if (args.size == 1 && args[0] == "standalone") {
        println(Messages.grappIntro)
        Grapp.isStandalone = true
        Grapp.run(defaultCliktConsole())
    } else if (args.size >= 1){
        // On véhicule la commande vers le serveur
        try {
            val s = Socket("localhost", 12345)
            val outputWriter = BufferedWriter(OutputStreamWriter(s.getOutputStream()))
            outputWriter.write(args.joinToString(separator = " "))
            outputWriter.flush()
            outputWriter.close()
            // Petite attente pour que le prompt du terminal ne s'affiche pas tout de suite
            Thread.sleep(500)
        } catch (e: java.net.ConnectException) {
            println(Messages.errorNoServer)
        }
    } else {
        println("""
            Usage: 
                1. Lancer le serveur : Grapp server &
                2. Exécuter des commandes : Grapp <commande> <arguments>
        """.trimIndent())
    }
}
