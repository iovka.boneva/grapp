/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.choice
import fr.ulille.grapp.io.YAMLFormatError
import fr.ulille.grapp.io.LatexSerializer
import fr.ulille.grapp.io.YamlParser
import fr.ulille.grapp.io.YamlSerializer
import java.io.File

object Convert : MyCliktCommand (help=Messages.convertHelp, printHelpOnEmptyArgs = true) {

    const val inputFileExtension = ".yaml"

    val toFormat by option("-f", help=Messages.convertOutFormatHelp)
        .choice("tikz", "yaml", "tex-standalone")
        .required()

    val ofn by option("-to", help=Messages.convertOutFileNameHelp)
    val inputFile by argument(help=Messages.convertInputHelp)

    override fun run() {
        try {
            if (! inputFile.endsWith(inputFileExtension))
                throw YAMLFormatError("Input file name must end with $inputFileExtension")
            val outFileExtension = when (toFormat) {
                "tikz" -> "tex"
                "yaml" -> "yaml"
                else -> "txt"
            }
            val outFileName = ofn?: "${inputFile.removeSuffix(inputFileExtension)}.$outFileExtension"

            val graph = YamlParser.parse(File(inputFile).inputStream())
            val o = StringBuilder()
            when (toFormat) {
                "tikz" -> LatexSerializer.serializeToTikz(graph, o)
                "yaml" -> YamlSerializer.serialize(graph,o)
                "tex-standalone" -> {
                    o.append(
                        """
                        \documentclass{standalone}
                        \usepackage{tikz}
                        \usetikzlibrary{arrows,automata,chains,matrix,positioning,scopes,arrows.meta}
                        \begin{document}                        
                    """.trimIndent()
                    )
                    LatexSerializer.serializeToTikz(graph, o, false)
                    o.append("\\end{document}")
                }
            }
            File(outFileName).writeText(o.toString())
        } catch (e: Exception) {
            echo("Erreur (${e.javaClass}): ${e.message}", err=true)
        }
    }
}

fun main(args: Array<String>) {
    Convert.main(args)
}