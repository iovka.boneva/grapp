/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import com.github.ajalt.clikt.output.Localization
import de.m3y.kformat.Table
import de.m3y.kformat.table
import fr.ulille.grapp.algorithms.PathCombinationOperation
import fr.ulille.grapp.graph.Graph
import fr.ulille.grapp.graph.ImplicitGraph
import fr.ulille.grapp.graph.Vertex
import fr.ulille.grapp.io.Util
import java.io.File

object Messages {
    // Documentation and bug related
    val grappIntro = """
  ------------------------------------------------------------------------------
  | Grapp : introduction aux graphes en BUT Informatique                       |
  | Exécuter des commandes sous la forme 'Grapp <commande> <arguments>'        |              
  | Exécuter 'Grapp help' pour la liste des commandes disponibles              |
  | Documentation : https://gitlab.univ-lille.fr/iovka.boneva/grapp            |
  ------------------------------------------------------------------------------
    """//.trimIndent()
    const val grappYamlDocUrl = "https://gitlab.univ-lille.fr/iovka.boneva/grapp/-/blob/master/doc/grapp-yaml.md"
    const val implicitGraphDocUrl = "https://gitlab.univ-lille.fr/iovka.boneva/grapp/-/blob/master/doc/implicit-graph.md"
    const val grappUsageDocUrl = "https://gitlab.univ-lille.fr/iovka.boneva/grapp/-/blob/master/doc/usage.md"
    const val bugReportUrl = "https://gitlab.univ-lille.fr/iovka.boneva/grapp/-/issues"

    // Commands' descriptions for help
    const val helpHelp =  "Afficher la liste des commandes disponibles"
    const val exitHelp = "Treminer le programmme (arrêt du serveur)"
    const val loadHelp =  "Charger un graphe depuis un fichier"
    const val listHelp = "Afficher les graphes chargés et les résultats déjà calculés"
    const val drawHelp ="Dessiner un graphe ou résultat"
    const val shortestPathHelp = "Calculer des plus courts chemins dans un graphe valué"
    const val optimalPathHelp = "Calculer des chemins optimaux dans un graphe valué"
    const val traversalHelp = "Calculer un parcours dans un graphe"
    const val findPathHelp = "Trouver un chemin dans un arbre résultat (de parcours, de chemin optimal)"
    const val maximumFlowHelp = "Calculer un flot maximal dans un réseau de transport"
    const val helpEpilog = "L'option --help permet d'obtenir de l'aide sur chaque commande. Par exemple, load --help\n"
    const val convertHelp = "Convertit la sérialisation d'un graphe d'un format à un autre"
    const val genYamlHelp = "Génère un fichier trame au format GrappYAML à partir d'une liste de sommets et d'arêtes. Une arête est donnée de la forme 'a-b' où a et b sont des sommets"

    // Commands' arguments descriptions for help
    const val fileNameMeta = "NOM_DE_FICHIER"
    const val shortestPathOrTraversalGraphNameMeta = "NOM_DE_GRAPHE"
    const val graphIdOrNameMeta = "ID_OU_NOM_DE_GRAPHE"
    const val resultIdMeta = "ID_DE_RÉSULTAT"
    const val nodeMeta = "SOMMET"
    const val loadFilenameHelp = "chemin vers un fichier contenant la définition d'un graphe (format yaml par défaut)"
    const val loadReloadHelp = "(optionnel) si présent, écrase le graphe déjà chargé ayant le même et oublie tous les résultats calculés pour ce graphe"
    val loadImplicitHelp = "(optionnel) si présent, le fichier doit être un fichier .class qui définit une classe implémentant l'${ImplicitGraph::class.java}"
    const val graphIdOrNameHelp = "identifiant ou nom de graphe chargé"
    const val drawResultIdHelp = "identifiant de résultat calculé"
    const val drawOutputFileNameHelp = "nom du fichier à écrire, sans extension"
    const val algorithmNameHelp = "nom de l'algorithme"
    const val shortestPathOrTraversalStartNodeHelp = "le sommet de départ"
    val optimalPathOperatorHelp = "l'opération utilisée pour combiner les chemins, ${PathCombinationOperation.Add} par défaut"
    const val optimalPathCriterionHelp = "le critère à optimiser"
    const val findPathInHelp = "Arbre de résultat où chercher le chemin"
    const val findPathFromNodeHelp = "Sommet début du chemin"
    const val findPathToNodeHelp = "Sommet fin du chemin"
    const val maximumFlowSourceHelp = "l'origine du réseau de transport"
    const val maximumFlowSinkHelp = "le puits du réseau de transport"
    const val transportationNetworkDescriptionHelp = "Un réseau de transport est un graphe orienté et valué avec des poids positifs.\n"+
        "Il possède un sommet origine (source) sans arêtes entrant, et un sommet puits (sink) sans arêtes sortant."
    const val convertInputHelp = "le nom du fichier en entrée; doit se terminer par ${Convert.inputFileExtension}"
    const val convertOutFormatHelp = "le format de sortie"
    const val convertOutFileNameHelp = "nom du fichier de sortie"
    const val csvFileHelp = "(optionnel) chemin vers un fichier csv où le résultat de l'algorithme sera écrit"
    const val silentHelp = "(optionnel) si présent, le résultat n'est pas affiché dans la console"
    const val genYamlGraphTypeHelp = "type de graphe"
    const val genYamlToHelp = "chemin de fichier où la trame du graphe sera écrite "
    const val genYamlNodesAndEdges = "<liste de sommets et d'arêtes>"

    // Prompt and other information messages
    const val loadReloadConfirm = "Étes vous sur·e de vouloir recharger le graphe? Tous les résultats déjà calculés seront perdus"
    const val loadOk = "Graphe chargé."
    const val drawOk = "Dessin du graphe sauvegardé."
    const val genYamlOk = "Trame du graphe générée."
    const val algorithmCsvOk = "Résultat écrit au format CSV."
    const val optimalPathOk = "Chemin optimal calculé."
    const val traversalOk = "Parcours calculé."
    const val shortestPathOk = "Plus court chemin calculé."
    const val maximumFlowOk = "Flot maximal calculé."
    const val findPathOk = "Chemin trouvé de longueur "
    const val availableCommands = "Commandes disponibles dans Grapp:\n"
    const val listKnownGraphsWithResults = "Graphes chargés et résultats calculés:"
    const val noLoadedGraphs = "Aucun graphe chargé. Utilisez load pour charger des graphes"
    const val listHeaderId = "Id"
    const val listHeaderName = "Nom de graphe"
    const val listHeaderDescription = "Description graphe / résultat"

    // Error messages
    const val errorToTreeGraphExists = "Impossible de sauvegarder le graphe car un graphe de ce nom existe déjà"
    const val errorLoadGraphExistsUseReload = "Impossible de charger le graphe car un graphe de ce nom existe déjà.\nUtiliser l'option -reload pour l'écraser"
    const val errorNoTransportationNetwork = "Le graphe n'est pas un réseau de transport"
    const val errorShortestPathUnweightedGraph = "Le graphe n'est pas valué. Impossible de calculer un plus court chemin"
    const val errorMissingGraphviz = "Impossible d'exécuter dot. Vérifiez que Graphviz (https://graphviz.org) est installé et la commande dot est sur le PATH"
    const val errorDotIO = "Impossible d'exécuter dot. Vérifiez que le chemin indiqué est autorisé en écriture."
    const val errorIO = "Impossible d'écrire le fichier"
    const val errorUnknownGraphOrResult = "Graphe ou résultat inconnu. Vérifiez la liste des graphes et des résultats"
    const val errorUnknownNode = "Sommet inconnu dans ce graphe"
    const val errorFileNotFound = "Fichier introuvable"
    const val errorLoadAborted = "Graphe non chargé"
    const val errorGenYamlIncorrectNodeName = "Nom de sommet non autorisé"
    const val errorGenYaml = "Trame non générée."
    const val errorFindPathArgumentNotTree = "Ce graphe n'est ni un arbre de parcours ni un arbre de chemins optimalaux"
    const val errorGenYamlInvalidEdgeDefinition = "Définition d'arête invalide (format incorrect ou sommet inexistant)"
    const val errorNoServer = "Serveur non atteignable. Assurez-vous d'avoir démarré le serveur"
    const val errorTwoServers = "Un serveur est déjà lancé. Utilisez la commande Grapp exit si vous voulez l'arrêter"

    // Output messages
    const val flowValue = "Valeur du flot"
    const val flowMinimalCut = "Coupe minimale"
    const val flowFlowTitle = "Flot"
    const val algorithmEdge = "arête"
    const val algorithmNode = "sommet"
    const val flowFlow = "flot"
    const val flowCapacity = "capacité"
    const val shortestPathDistance = "distance"
    const val optimalPathValue = "valeur optimale"
    const val predecessorInTree = "prédécesseur"
    const val findPathNoPath = "Aucun chemin entre ces sommets."

    // Used by clikt
    const val cliktHelpOptionMessage = "Afficher ce message d'aide"
    const val cliktUsageTitle = "Utilisation:"
}

object GrappFrenchLocalization : Localization {
    override fun commandsTitle(): String = Messages.availableCommands
    override fun helpOptionMessage(): String = Messages.cliktHelpOptionMessage
    override fun usageTitle(): String = Messages.cliktUsageTitle
}

object DescriptionFactory {

    fun shortestPathOrTraversalResultDescription(graph: Graph, startNode: Vertex, algoName: String) : String {
        return "$algoName on ${graph.getName()} from $startNode"
    }
    fun findPathDescription(result: Result, startNode: Vertex, endNode: Vertex) : String {
        val typeKind = when (result) {
            is OptimalPathResult -> result.optimalPathKind.descr
            is TraversalResult -> "Traversal path"
            else -> throw IllegalStateException("Not implemented")
        }
        return "$typeKind from $startNode to $endNode in ${result.graph.getName()}"
    }
    fun maximumFlowResultDescription(graph: Graph, source: Vertex, sink: Vertex, algoName: String): String {
        return "$algoName on ${graph.getName()} from source $source to sink $sink"
    }
}

object ListCommandOutputFormatting {

    fun prettyPrint (map: Map<String, Pair<Graph, Map<String, Result>>>) : String {
        val s = StringBuilder()
        table {
            header(Messages.listHeaderId, Messages.listHeaderName, Messages.listHeaderDescription)
            for (graphWithResults in map.entries) {
                val graph = graphWithResults.value.first
                row(graphWithResults.key, graph.getName(), graph.getDescr())
                for (result in graphWithResults.value.second) {
                    row(result.key, "", result.value.descr)
                }
                line()
            }
            hints {
                alignment(Messages.listHeaderId, Table.Hints.Alignment.LEFT)
                alignment(Messages.listHeaderName, Table.Hints.Alignment.LEFT)
                alignment(Messages.listHeaderDescription, Table.Hints.Alignment.LEFT)
                borderStyle = Table.BorderStyle.SINGLE_LINE
            }
        }.render(s)
        return s.toString()
    }
}

fun Result.textArray () : Array<Array<String>> {
    return when (this) {
        is OptimalPathResult -> {
            val rowTitle = when (this.optimalPathKind) {
                OptimalPathKind.SHORTEST_PATH -> Messages.shortestPathDistance
                OptimalPathKind.LONGEST_PATH, OptimalPathKind.SHORTEST_MULT_PATH, OptimalPathKind.LONGEST_MULT_PATH -> Messages.optimalPathValue
            }
            arrayOf(
                arrayOf(Messages.algorithmNode, *this.graph.vertexSet().toTypedArray()),
                arrayOf(
                    rowTitle,
                    *this.graph.vertexSet().map { "${Util.weightToString(this.distancesMap[it]!!)}" }.toTypedArray()
                ),
                arrayOf(
                    Messages.predecessorInTree,
                    *this.graph.vertexSet().map { "${this.predecessorsMap[it] ?: ""}" }.toTypedArray()
                )
            )
        }
        is TraversalResult -> arrayOf(
            arrayOf(Messages.algorithmNode, *this.graph.vertexSet().toTypedArray()),
            arrayOf(
                Messages.predecessorInTree,
                *this.graph.vertexSet().map { "${this.predecessorsMap[it] ?: ""}" }.toTypedArray()
            )
        )
        is PathResult -> {
            if (this.pathNodes == null)
                arrayOf(arrayOf(Messages.findPathNoPath))
            else
                arrayOf(
                    arrayOf("${Messages.findPathOk}${this.pathNodes.size-1}"),
                    arrayOf(this.pathNodes.joinToString (prefix = "[", postfix = "]", separator=", "))
                )
        }
        is MaximumFlowResult -> {
            val srcPart = this.sourcePartition.joinToString(prefix = "{", postfix = "}")
            val sinkPart = this.sinkPartition.joinToString(prefix = "{", postfix = "}")
            arrayOf(
                arrayOf("${Messages.flowValue} : ${this.flowValue}"),
                arrayOf("${Messages.flowMinimalCut} : $srcPart / $sinkPart"),
                arrayOf("${Messages.flowFlowTitle} :"),
                arrayOf(
                    Messages.algorithmEdge,
                    *this.graph.edgeSet().map(transform = Util.edgeToStringNoWeight).toTypedArray()
                ),
                arrayOf(
                    Messages.flowFlow,
                    *this.graph.edgeSet().map { Util.weightToString(this.flowMap[it]!!) }.toTypedArray()
                ),
                arrayOf(
                    Messages.flowCapacity,
                    *this.graph.edgeSet().map { Util.weightToString(it.weight) }.toTypedArray()
                )
            )
        }
        else -> throw IllegalStateException("No such result type : ${this.javaClass}")
    }
}

object OutputFormatting {

    fun format (message: String) : String {
        return "  |  ${message.replace("\n", "\n  |  ")}"
    }

    fun resultTable (textArray : Array<Array<String>>) : Table {
        return table {
            header(*textArray[0])
            for (i in 1 until textArray.size)
                row(*textArray[i])
            hints {
                borderStyle = Table.BorderStyle.SINGLE_LINE
            }
        }
    }

    fun prettyPrint(r: TreeResult) : String {
        val s = StringBuilder()
        resultTable(r.textArray()).render(s)
        return s.toString()
    }

    fun prettyPrint(r: PathResult, silent: Boolean) : String {
        val s = StringBuilder()
        val a = r.textArray()
        s.appendLine(a[0][0])
        if (r.pathNodes != null && !silent)
            s.appendLine(r.textArray()[1][0])
        return s.toString()
    }

    fun prettyPrint(r: MaximumFlowResult, silent: Boolean) : String {
        val s = StringBuilder()
        val a = r.textArray()
        s.appendLine(a[0][0])
        s.appendLine(a[1][0])
        if (!silent) {
            s.appendLine(a[2][0])
            resultTable(a.sliceArray(3..5)).render(s)
        }
        return s.toString()
    }
}

object CsvIo {

    private const val CSVSEP = ";"

    /** IOException possible */
    fun csvPrint (r: Result, fileName: String) {
        val s = StringBuilder()
        for (row in r.textArray())
            s.appendLine(row.joinToString(separator= CSVSEP))
        File(fileName).appendText(s.toString())
    }
}
