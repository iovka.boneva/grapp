/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import fr.ulille.grapp.algorithms.*
import fr.ulille.grapp.graph.*
import org.jgrapht.alg.shortestpath.BFSShortestPath

interface OptimalPathAlgorithmName {
    val name: String
}

enum class ShortestPathAlgorithmName : OptimalPathAlgorithmName {
    Dijkstra, BellmanFord
}

enum class BellmanAlgorithmName : OptimalPathAlgorithmName {
    Bellman
}

enum class TraversalAlgorithmName {
    BFS, DFS
}

enum class MaximumFlowAlgorithmName {
    FordFulkerson
}

/** Runs the algorithms and produces the results as needed by the Grapp application. */
object AlgorithmsUtil {

    fun runOptimalPathAlgorithm(
        algoName: OptimalPathAlgorithmName,
        graph: Graph,
        node: Vertex,
        combinationOperator: PathCombinationOperation = PathCombinationOperation.Add,
        optimalityCriterion: OptimalityCriterion = OptimalityCriterion.Min
    ): OptimalPathResult {
        if (!graph.isWeighted())
            throw GrappError(Messages.errorShortestPathUnweightedGraph)
        val runner: OptimalPathAlgorithm =
            when (algoName) {
                is ShortestPathAlgorithmName -> when (algoName) {
                    ShortestPathAlgorithmName.Dijkstra -> Dijkstra(graph)
                    ShortestPathAlgorithmName.BellmanFord -> BellmanFord(graph)
                }
                is BellmanAlgorithmName -> when (algoName) {
                    BellmanAlgorithmName.Bellman -> Bellman(graph, combinationOperator, optimalityCriterion)
                }
                else -> throw IllegalStateException("Not implemented")
            }

        val predMapResult = mutableMapOf<Vertex, Vertex>()
        val distMapResult = mutableMapOf<Vertex, Double>()

        runner.compute(node, distMapResult, predMapResult)
        val shortestPathsTree = constructTraversalTree(graph, predMapResult)
        val extendedAlgoName = when (algoName) {
            is BellmanAlgorithmName -> "${algoName.name}(${optimalityCriterion.name}, ${combinationOperator.name})"
            else -> algoName.name
        }

        return OptimalPathResult(
            graph=graph, descr=DescriptionFactory.shortestPathOrTraversalResultDescription(graph, node, extendedAlgoName),
            predecessorsMap = predMapResult, distancesMap = distMapResult, tree = shortestPathsTree,
            optimalPathKind = OptimalPathKind.from(combinationOperator, optimalityCriterion)
        )
    }

    fun runGraphTraversalAlgorithm(
        algoName: TraversalAlgorithmName,
        graph: Graph,
        node: Vertex
    ): TraversalResult {
        val runner: GraphTraversalAlgorithm = when (algoName) {
            TraversalAlgorithmName.BFS -> BreadthFirstSearch(graph)
            TraversalAlgorithmName.DFS -> DepthFirstSearch(graph)
        }
        val pred = mutableMapOf<Vertex,Vertex>()

        runner.compute(node, pred)
        val traversalTree = constructTraversalTree(graph, pred)

        return TraversalResult(
            graph,
            DescriptionFactory.shortestPathOrTraversalResultDescription(graph, node, algoName.name),
            pred,
            traversalTree
        )
    }

    fun runFindPathAlgorithm (result: TreeResult, from: Vertex, to: Vertex) : PathResult {

        val graphPath = BFSShortestPath(result.tree).getPath(from,to)
        val edgesList = (graphPath?.edgeList)?.toMutableList()
        if (edgesList != null && ! result.graph.isDirected()) {
            var previousTgt = from
            for (i in 0 until edgesList.size) {
                if (previousTgt != edgesList[i].src) {
                    val e = edgesList[i]
                    edgesList[i] = Edge(e.tgt, e.src, e.weight)
                }
                previousTgt = edgesList[i].tgt
            }
        }
        return PathResult(
            graph=result.graph,
            descr=DescriptionFactory.findPathDescription(result, from, to),
            pathEdges=edgesList,
            pathNodes=graphPath?.vertexList)
    }

    fun runMaximumFlowAlgorithm(
        algoName: MaximumFlowAlgorithmName,
        graph: Graph,
        source: Vertex,
        sink: Vertex
    ): MaximumFlowResult {
        if (!graph.isWeighted() || !graph.isDirected() || graph.inDegreeOf(source) != 0 || graph.outDegreeOf(sink) != 0)
            throw GrappError(Messages.errorNoTransportationNetwork + "\n${Messages.transportationNetworkDescriptionHelp}")
        val runner = let {
            try {
                EdmondsKarpAlgorithm(graph)
            } catch (e: IllegalArgumentException) {
                throw GrappError("${Messages.errorNoTransportationNetwork}\n${Messages.transportationNetworkDescriptionHelp}")
            }
        }
        val flow = mutableMapOf<Edge, Double>()
        val sourcePart = mutableSetOf<Vertex>()
        val sinkPart = mutableSetOf<Vertex>()

        val value = runner.compute(source, sink, flow, sourcePart, sinkPart)
        return MaximumFlowResult(
            graph,
            DescriptionFactory.maximumFlowResultDescription(graph, source, sink, algoName.name),
            value,
            flow,
            sourcePart,
            sinkPart
        )
    }

    fun constructTraversalTree (graph: Graph, predecessorsMap : Map<Vertex, Vertex>): Graph {
        val result = Graphs.graphFromNodeSet(
            if (graph.isWeighted()) GrappType.diwgraph else GrappType.digraph,
            graph.vertexSet())
        for (pred in predecessorsMap)
            Graphs.addEdge(result, pred.value, pred.key, graph.getEdge(pred.value, pred.key).weight)
        return result
    }
}
