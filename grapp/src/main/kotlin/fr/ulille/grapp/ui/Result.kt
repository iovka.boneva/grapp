/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import fr.ulille.grapp.algorithms.OptimalityCriterion
import fr.ulille.grapp.algorithms.PathCombinationOperation
import fr.ulille.grapp.graph.Edge
import fr.ulille.grapp.graph.Graph
import fr.ulille.grapp.graph.Vertex
import kotlin.collections.List


sealed interface Result  {
    val graph : Graph
    val descr : String
}

data class MaximumFlowResult(
    override val graph: Graph,
    override val descr: String,
    val flowValue: Double,
    val flowMap: Map<Edge, Double>,
    val sourcePartition: Set<Vertex>,
    val sinkPartition: Set<Vertex>) : Result {}

data class PathResult (
    override val graph: Graph,
    override val descr: String,
    val pathEdges: List<Edge>?,
    val pathNodes: List<Vertex>?) : Result {}

interface TreeResult : Result {
    val tree: Graph
    val predecessorsMap : Map<Vertex, Vertex>
}

data class TraversalResult (
    override val graph: Graph,
    override val descr: String,
    override val predecessorsMap: Map<Vertex,Vertex>,
    override val tree: Graph) : TreeResult {}

data class OptimalPathResult (
    override val graph: Graph,
    override val descr: String,
    override val predecessorsMap: Map<Vertex,Vertex>,
    val distancesMap: Map<Vertex, Double>,
    override val tree: Graph,
    val optimalPathKind : OptimalPathKind) : TreeResult  {}

enum class OptimalPathKind (val descr: String) {
    SHORTEST_PATH ("Shortest path"),
    LONGEST_PATH ("Longest path"),
    SHORTEST_MULT_PATH ("Least product path"),
    LONGEST_MULT_PATH ("Greatest product path");

    companion object {
        fun from(
            combinationOperator: PathCombinationOperation,
            optimalityCriterion: OptimalityCriterion
        ): OptimalPathKind {
            if (combinationOperator == PathCombinationOperation.Add && optimalityCriterion == OptimalityCriterion.Min)
                return SHORTEST_PATH
            if (combinationOperator == PathCombinationOperation.Add && optimalityCriterion == OptimalityCriterion.Max)
                return LONGEST_PATH
            if (combinationOperator == PathCombinationOperation.Mult && optimalityCriterion == OptimalityCriterion.Min)
                return SHORTEST_MULT_PATH
            if (combinationOperator == PathCombinationOperation.Mult && optimalityCriterion == OptimalityCriterion.Max)
                return LONGEST_MULT_PATH
            throw IllegalStateException("Not implemented")
        }
    }
}
