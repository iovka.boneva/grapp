/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import java.io.File
import java.util.concurrent.TimeUnit

object CommandLineUtil {

    private val isWindows = System.getProperty("os.name")
        .lowercase().startsWith("windows")
    private val dotCommand = if (isWindows) "cmd.exe /c dot.exe" else "dot"

    /** Executes a command in blocking mode.
     * @param timeout Time in seconds to wait for command execution. No timeout if negative.
     * @return The exit code of the command.
     */
    private fun executeCommand(
        commandLine: String,
        directory: String = System.getProperty("user.dir"),
        timeout: Long = -1
    ): Int {
        val process = ProcessBuilder(commandLine.split(" "))
            .directory(File(directory))
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
        return when {
            timeout < 0 -> process.waitFor()
            process.waitFor(timeout, TimeUnit.SECONDS) -> 0
            else -> 1
        }
    }

    fun executeDotToPng (inputFileName : String, outputFileName: String) : Int {
        return executeCommand("$dotCommand $inputFileName -Tpng -o $outputFileName")
    }
}