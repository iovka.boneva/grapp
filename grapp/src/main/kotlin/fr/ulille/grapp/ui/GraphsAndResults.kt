/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.ui

import fr.ulille.grapp.graph.Graph

/**
 * Stores a collection of Graphs and Results associated with these graphs.
 * Graphs have user defined names (coming from their yaml definition) and there cannot be two graphs with the same name.
 *
 * Graphs and results can be accessed through unique identifiers associated to them when they are added.
 * Graphs can also be accessed through their unique names.
 *
 * The form of the identifiers is fixed:
 * - "g<x>" for graphs, where <x> is a natural and is the graph's unique id within the program
 * - "r<x>.<y>" for results, where <x> is a graph id and <y> is the result's unique id within all results of the same graph
 */
class GraphsAndResults {

    companion object {
        val graphIdentifierFormat = Regex("g\\d+")
        val resultIdentifierFormat = Regex("r\\d+\\.\\d+")

        /** Transforms a graph name for the needs of the application
         * A normalized graph name
         * - does not have spaces (easier manipulation on command line)
         * - does not contain the dot character (so no collision with graph or result identifiers)
         */
        fun normalizeGraphName (givenName: String) : String {
            return givenName.replace(forbiddenSubsequenceInGraphName, "_") // all sequences of non word characters become a single underscore
        }
        val forbiddenSubsequenceInGraphName = Regex("[^\\p{IsAlphabetic}\\d]+")
    }

    /** Associates graphs with their identifiers. */
    private val graphsMap = mutableMapOf<String, Graph>()
    /** Associates results with their identifiers. */
    private val resultsMap = mutableMapOf<String, Result>()

    private var lastGraphId = 0
    private var lastResultId = mutableMapOf<String,Int>()

    /** This invariant is ensured. */
    internal val checkInvariant : () -> Boolean = {
        graphsMap.keys.containsAll(resultsMap.keys.map(getGraphId)) // every result (id) corresponds to a graph (id) that is currently stored
            && graphsMap.values.map { g -> g.getName() }.size == graphsMap.size // no two graphs have the same name
            && graphsMap.keys.containsAll(lastResultId.keys) // the lastResultId map stores ids only for graphs that are currently stored
            && graphsMap.keys.all { it.matches(graphIdentifierFormat) }
            && resultsMap.keys.all { it.matches(resultIdentifierFormat) }
    }

    private val nextGraphId : () -> String = {
        "g${++lastGraphId}"
    }
    private val nextResultId : (String) -> String = { graphId ->
        var i = lastResultId.getOrPut(graphId){0}
        lastResultId[graphId] = ++i
        "r${graphId.substring(1)}.$i"
    }

    private val getGraphId : (String) -> String = { resultId ->
        resultId.substring(0 until resultId.indexOf(".")).replace("r","g")
    }

    /** The identifier for the graph with the given name. */
    private fun getId (graphName: String) : String? {
        return graphsMap.entries.firstOrNull { e -> e.value.getName() == graphName }?.key
    }

    /** Adds a graph if no graph with the same name is currently stored, or if overwrite is true.
     * @return true if the graph was added, false otherwise
     */
    fun add (graph: Graph, overwrite : Boolean) : Boolean {
        val existingGraphId = getId(graph.getName())
        val added = if (existingGraphId == null || overwrite) {
            graphsMap[nextGraphId()] = graph
            true
        } else {
            false
        }
        if (existingGraphId != null && added) {
            graphsMap.remove(existingGraphId)
            resultsMap.entries.removeIf { e -> e.key.startsWith("r${existingGraphId.substring(1)}.") }
            lastResultId.remove(existingGraphId)
        }
        return added
    }

    /** Adds a result.
     *  No effect if the graph of the result is unknown (ie its name is unknown)
     *  @return true iff the result was added
     */
    fun add(result: Result) : Boolean {
        val existingGraphId = getId(result.graph.getName())
        return if (existingGraphId != null) {
            resultsMap[nextResultId(existingGraphId)] = result
            true
        } else
            false
    }

    /** The graph with the associated id or name, or null if no such graph is currently stored.
     * @idOrName a String that is considered as graph identifier, or as graph name if idOrName is not an identifier of any currently stored graph
     */
    fun getGraph(idOrName: String) : Graph? {
        return graphsMap[idOrName]?: graphsMap[getId(idOrName)]
    }

    /** The result with the given identifier */
    fun getResult(id: String) : Result? {
        return resultsMap[id]
    }

    fun graphsAndTheirResults () : Map<String, Pair<Graph, Map<String, Result>>> {
        val map = graphsMap.entries.associateByTo(
            destination = mutableMapOf<String, Pair<Graph, MutableMap<String,Result>>>(),
            keySelector = { e -> e.key },
            valueTransform = { e -> Pair(e.value, mutableMapOf())})
        for (resultEntry in resultsMap.entries)
            map[getGraphId(resultEntry.key)]!!.second[resultEntry.key] = resultEntry.value
        return map
    }

    fun isEmpty () = graphsMap.isEmpty()

    // Useful for testing
    internal fun clear () {
        graphsMap.clear()
        resultsMap.clear()
        lastGraphId = 0
        lastResultId.clear()
    }

}
