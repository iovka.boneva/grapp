/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.graph

import fr.ulille.grapp.io.ImplicitGraphFormatError
import java.lang.IllegalArgumentException

interface ImplicitGraphInterface {
    fun explore(thisGraph: Graph)
}

class ImplicitGraphInterfaceImplementation : ImplicitGraphInterface {
    private var explored = false
    override fun explore(thisGraph: Graph) {
        if (explored) return

        val exploredVertices = mutableSetOf<Vertex>()
        val unexploredVertices = thisGraph.vertexSet().toMutableSet()
        while (unexploredVertices.isNotEmpty()) {
            val vertex = unexploredVertices.first()
            val out = thisGraph.outgoingEdgesOf(vertex)
            exploredVertices.add(vertex)
            for (e in out) {
                if (!exploredVertices.contains(e.tgt))
                    unexploredVertices.add(e.tgt)
            }
            unexploredVertices.remove(vertex)
        }
        explored = true
    }
}

class ImplicitDirectedGraph  (private val ig: ImplicitGraph) : DirectedGraph(), ImplicitGraphInterface by ImplicitGraphInterfaceImplementation() {
    private val completeVertices = mutableSetOf<Vertex>()
    init {
        try {
            ImplicitGraphsUtil.addInitialVertices(this, ig)
        } catch (e: java.lang.NullPointerException) {
            throw ImplicitGraphFormatError(ImplicitGraphsUtil.vertexShouldNotBeNull)
        }
    }
    override fun outgoingEdgesOf(vertex: Vertex): MutableSet<Edge> {
        ImplicitGraphsUtil.computeCompletion(vertex, this, ig, completeVertices)
        return super.outgoingEdgesOf(vertex)
    }
}

class ImplicitDirectedWeightedGraph  (private val ig: ImplicitGraph) : DirectedWeightedGraph(), ImplicitGraphInterface by ImplicitGraphInterfaceImplementation() {
    private val completeVertices = mutableSetOf<Vertex>()
    init {
        try {
            ImplicitGraphsUtil.addInitialVertices(this, ig)
        } catch (e: java.lang.NullPointerException) {
            throw ImplicitGraphFormatError(ImplicitGraphsUtil.vertexShouldNotBeNull)
        }
    }
    override fun outgoingEdgesOf(vertex: Vertex): MutableSet<Edge> {
        ImplicitGraphsUtil.computeCompletionWeighted(vertex, this, ig, completeVertices)
        return super.outgoingEdgesOf(vertex)
    }
}

object ImplicitGraphsUtil {

    const val nameShouldNotBeNull = "Le nom d'un ImplicitGraph ne peut pas être null ni la chaine vide."
    const val typeShouldNotBeNull = "Le type d'un ImplicitGraph ne peut pas être null."
    const val wrongType = "Type incorrect pour ImplicitGraph. Les types autorisés sont digraph et diwgraph."
    const val incorrectFormatWeightedSuccessors = " les successeurs d'un sommet dans un graphe valué doivent être de la forme 'sommet:poids'."
    const val vertexShouldNotBeNull = "Un sommet du graphe ne peut pas être null."
    val missingImplements = "La définition d'un graphe implicite doit implémenter l'${ImplicitGraph::class.java}."
    const val errorImplicitGraphNonPublic = "La classe implémentant un graphe implicite doit être publique."

    fun addInitialVertices (thisGraph: Graph,
                            ig: ImplicitGraph) {
        for (v in ig.initialNodes())
            thisGraph.addVertex(v)

    }

    fun computeCompletion (vertex: Vertex,
                           thisGraph: Graph,
                           ig: ImplicitGraph,
                           completeVertices: MutableSet<Vertex>) {

        try {
            if (! completeVertices.contains(vertex)) {
                for (tgt in ig.successors(vertex)) { // NullPointerException possible
                    thisGraph.addVertex(tgt)
                    Graphs.addEdge(thisGraph, vertex, tgt)
                }
                completeVertices.add(vertex)
            }
        } catch (e: java.lang.NullPointerException) {
            throw ImplicitGraphFormatError(vertexShouldNotBeNull)
        }
    }

    fun computeCompletionWeighted (vertex: Vertex,
                                  thisGraph: Graph,
                                  ig: ImplicitGraph,
                                  completeVertices: MutableSet<Vertex>) {

        try {
            if (!completeVertices.contains(vertex)) {
                for (tgtWeight in ig.successors(vertex)) {
                    val (tgt, weight) = splitWeightedSuccessor(tgtWeight, vertex)
                    thisGraph.addVertex(tgt)
                    Graphs.addEdge(thisGraph, vertex, tgt, weight)
                }
                completeVertices.add(vertex)
            }
        } catch (e: java.lang.NullPointerException) {
            throw ImplicitGraphFormatError(vertexShouldNotBeNull)
        }
    }

    private fun splitWeightedSuccessor (successorString: String, sourceVertex: Vertex) : Pair<Vertex, Double> {
        val p = successorString.split(":")
        if (p.size != 2 || p[0].isEmpty() || p[1].toDoubleOrNull() == null)
            throw ImplicitGraphFormatError(incorrectFormatWeightedSuccessors, "trouvé successeur '$successorString' pour le sommet '$sourceVertex'")
        return Pair(p[0], p[1].toDouble())
    }

    private fun getName (from: ImplicitGraph) : String {
        val n = from.name()
        if (n == null || n.isEmpty())
            throw ImplicitGraphFormatError(nameShouldNotBeNull)
        return n
    }

    private fun getType (from: ImplicitGraph) : GrappType {
        return try {
            GrappType.fromString(from.type())
        } catch (e: IllegalArgumentException) {
            throw ImplicitGraphFormatError(wrongType)
        } catch (e: NullPointerException) {
            throw ImplicitGraphFormatError(typeShouldNotBeNull)
        }
    }

    fun getGraph (from: ImplicitGraph) : Graph {
        val type = getType(from)
        val name = getName(from)
        return when (type) {
            GrappType.ugraph, GrappType.uwgraph -> throw ImplicitGraphFormatError(wrongType)
            GrappType.digraph -> ImplicitDirectedGraph (from)
            GrappType.diwgraph -> ImplicitDirectedWeightedGraph(from)
        }.apply {
            setName(name)
        }
    }
}
