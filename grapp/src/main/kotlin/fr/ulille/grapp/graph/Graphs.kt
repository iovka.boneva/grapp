/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.graph

import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.util.*

data class Edge (val src: String, val tgt: String, val weight: Double = Double.NaN) {
    override fun toString(): String {
        val label = if(weight.isNaN()) "" else "$weight, "
        return("($src, $label$tgt)")
    }
}

typealias Vertex = String

interface Graph : org.jgrapht.Graph<Vertex, Edge>, GrappProperties

enum class GrappType {
    ugraph, digraph, uwgraph, diwgraph;

    fun isDirected(): Boolean = this == GrappType.digraph || this == GrappType.diwgraph
    fun isWeighted(): Boolean = this == GrappType.uwgraph || this == GrappType.diwgraph

    companion object {
        val allowedStrings = arrayOf("ugraph", "digraph", "uwgraph", "wugraph", "diwgraph", "wdigraph")
        fun fromString (s: String) : GrappType {
            return when (s.lowercase(Locale.getDefault())) { //(s.lowercase(Locale.getDefault())) {
                "ugraph" -> ugraph
                "digraph" -> digraph
                "uwgraph", "wugraph" -> uwgraph
                "diwgraph", "wdigraph" -> diwgraph
                else -> throw IllegalArgumentException("Unknown value for GrappType : $s")
            }
        }
    }
}

open class UndirectedGraph : org.jgrapht.graph.DefaultUndirectedGraph<String, Edge>(Edge::class.java),
    GrappProperties by GrappPropertiesImpl(GrappType.ugraph),
    Graph
open class DirectedGraph : org.jgrapht.graph.DefaultDirectedGraph<String, Edge>(Edge::class.java),
    GrappProperties by GrappPropertiesImpl(GrappType.digraph),
    Graph
open class UndirectedWeightedGraph : org.jgrapht.graph.DefaultUndirectedWeightedGraph<String, Edge>(Edge::class.java),
    GrappProperties by GrappPropertiesImpl(GrappType.uwgraph),
    Graph
open class DirectedWeightedGraph : org.jgrapht.graph.DefaultDirectedWeightedGraph<String, Edge>(Edge::class.java),
    GrappProperties by GrappPropertiesImpl(GrappType.diwgraph),
    Graph

interface GrappProperties {
    fun getGrappType() : GrappType
    fun isDirected(): Boolean
    fun isWeighted(): Boolean

    fun getName() : String
    fun setName(name: String)
    fun getDescr() : String
    fun setDescr(descr: String)
    fun setDrawInstructions(drawInstructions: Map<String, String>)
    fun getDrawInstructions() : Map<String, String>?
}

class GrappPropertiesImpl(val type: GrappType) : GrappProperties {
    override fun getGrappType(): GrappType = type
    override fun isDirected(): Boolean = type.isDirected()
    override fun isWeighted(): Boolean = type.isWeighted()

    private var name : String = ""
    private var descr : String = ""
    private var drawInstructions : Map<String, String>? = null

    override fun getName(): String {
        return if (name == "") throw IllegalStateException("Name was not set.") else name
    }
    override fun setName(name: String) {
        this.name = name
    }
    override fun getDescr(): String = descr
    override fun setDescr(descr: String) {
        this.descr = descr
    }

    override fun setDrawInstructions(drawInstructions: Map<String, String>) {
        this.drawInstructions = drawInstructions
    }
    override fun getDrawInstructions() : Map<String,String>? = drawInstructions
}

object Graphs {

    fun emptyGraphFromGrappType(grappType: GrappType): Graph {
        return when (grappType) {
            GrappType.ugraph -> UndirectedGraph()
            GrappType.digraph -> DirectedGraph()
            GrappType.uwgraph -> UndirectedWeightedGraph()
            GrappType.diwgraph -> DirectedWeightedGraph()
        }
    }

    fun graphFromNodeSet(grappType: GrappType, nodes: Collection<Vertex>): Graph {
        val graph = emptyGraphFromGrappType(grappType)
        for (n in nodes)
            graph.addVertex(n)
        return graph
    }

    fun addEdge (graph: Graph, src: Vertex, tgt: Vertex, weight: Double = Double.NaN) : Edge {
        val edge = Edge(src, tgt, weight)
        graph.addEdge(src, tgt, edge)
        if (graph.isWeighted())
            graph.setEdgeWeight(src, tgt, weight)
        return edge
    }
}

