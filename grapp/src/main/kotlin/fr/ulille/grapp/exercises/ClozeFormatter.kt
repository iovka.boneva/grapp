/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises

import java.lang.StringBuilder

object ClozeFormatter {

    const val clozeTableStyle= "style=\"border-style: solid; border-width: 2px; text-align: center\""
    const val clozeThStyle = "style=\"border-bottom: 2px solid; border-right: 1px solid\""
    const val clozeTdStyle = "style=\"border-style:solid; border-width: 1px\""

    fun clozeQuestion (answer: String, nbPoints: Int): String {
        return when {
            answer.toDoubleOrNull() != null -> "{$nbPoints:NUMERICAL:=$answer}"
            answer.isEmpty() -> "{0:SA:=.}"
            else -> "{$nbPoints:SA:=$answer}"
        }
    }

    fun clozeQuestionTable(s: StringBuilder,
                           columnNames: List<String>,
                           table: List<Map<String, String>>,
                           pointsPerQuestion: Int,
                           indicesOfLinesThatAreNotQuestions : Set<Int> = emptySet(),
                           namesOfColumnsThatAreNotQuestions: Set<String> = emptySet()) {
        s.append("<table $clozeTableStyle>\n")
        clozeTableHeader(s, columnNames)
        s.append("<tbody>\n")
        for (lineIndex in table.indices) {
            val line = table[lineIndex]
            s.append("<tr>\n")
            for (colName in columnNames) {
                val value = line[colName]!!
                val valueString =
                    if (indicesOfLinesThatAreNotQuestions.contains(lineIndex) || namesOfColumnsThatAreNotQuestions.contains(colName)) value
                    else clozeQuestion(value, pointsPerQuestion)
                s.append("<td $clozeTdStyle>$valueString</td>\n")
            }
            s.append("</tr>\n")
        }
        s.append("</tbody>\n")
        s.append("</table>")
    }

    private fun clozeTableHeader (s: StringBuilder, columnNames: List<String>) {
        s.append("<thead><tr>\n")
        for (cname in columnNames) {
            s.append("<th $clozeThStyle>$cname</th>\n")
        }
        s.append("</tr></thead>\n")
    }

}