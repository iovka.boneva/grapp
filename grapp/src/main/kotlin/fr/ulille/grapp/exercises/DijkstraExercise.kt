/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises

import fr.ulille.grapp.algorithms.Dijkstra
import fr.ulille.grapp.algorithms.traces.DijkstraTraces
import fr.ulille.grapp.algorithms.traces.ExecutionTrace
import fr.ulille.grapp.graph.*
import fr.ulille.grapp.io.DotIo
import fr.ulille.grapp.io.LatexSerializer
import fr.ulille.grapp.io.Util
import fr.ulille.grapp.ui.*
import org.jgrapht.alg.shortestpath.BFSShortestPath
import java.io.File
import java.lang.IllegalArgumentException
import kotlin.math.roundToInt

class DijkstraExercise (private val outputFolderName: String, private val graphName: String, private val isDirected: Boolean) {

    private val pointsTraceExecution = 300
    private val pointsDistances = 250
    private val pointsPredecessors = 250
    private val pointsChemin = 100

    /** Génère un exercice pour Dijkstra composé de ces fichiers importants (et quelques fichiers inutiles) :
     * - result.png : (opitonnel) dessin du résultat, où on voit l'arbre des plus courts chemins
     * - graph.tex : dessin tikz du graphe en classe standalone (peut être compilé pour générer pdf, puis converti en png)
     * - question.cloze : la question cloze. Comprend une trace d'exécution, une table des distances, une table des prédécesseurs et 2 chemins à déterminer
     */
    fun generate(generateResultPng: Boolean = false) {
        // générer un graphe aléatoire et un sommet de départ
        val possibleExcluded = listOf(setOf("c", "g"), setOf("a", "i"), setOf("g", "i"))
        val gen = GridGraphGenerator(3,3, possibleExcluded.random())
        val graph = gen.generateGraph(graphName,10, if (isDirected) GrappType.diwgraph else GrappType.uwgraph)
        val start = gen.randomVertex()

        // s'assurer que c'est un bon exercice
        val (isAmbiguous, numberInaccessible) = dijkstraIsAmbiguousAndNumberInaccessible(graph, start)
        if (isAmbiguous || numberInaccessible > 1) {
            /* File(fileName("INAPPROPRIATE-graph", "tex"))
                .writeText(GraphOnGridTikzFormatter.standaloneTikzPicture(graph, GraphOnGridTikzFormatter.o_abc_def_ghi_p_drawInstructions)) */
            return
        }

        // calculer le résultat
        val (et, dist, pred) = DijkstraTraces.distSlashPredTrace(graph, start)
        val tableLines = ExecutionTrace.cleanLines(et.getTableLines())
        val result = OptimalPathResult(graph, "exercise", pred, dist, AlgorithmsUtil.constructTraversalTree(graph, pred), OptimalPathKind.SHORTEST_PATH)

        // informations nécessaires pour les questions
        val pointsEt = (pointsTraceExecution.toDouble() / nombreCasesARemplir(tableLines)).roundToInt()
        val pointsDist = (pointsDistances.toDouble() / graph.vertexSet().size).roundToInt()
        val pointsPred = (pointsPredecessors.toDouble() / (graph.vertexSet().size - 1)).roundToInt()
        val (path1, path2) = twoPaths(dist, result, start)

        // produire les fichiers de sortie
        writeGraphTikz(graph)
        if (generateResultPng) writeResultPng(graph, result, start)

        val s = StringBuilder()
        s.append("""
<p>Utilisez l'algorithme de Dijkstra pour calculer des <b>plus courts chemins depuis le sommet $start</b> dans le graphe ci-dessous.</p>
<p>La correction étant automatique, <b>respectez strictement le format de réponse demandé.</b></p>
<h4>Trace d'exécution (${pointsTraceExecution/10}%)</h4>
<i>
<p>Format de la réponse</p>
<ul>
<li>dans la première colonne, donner le sommet de l'itération courante,</li>
<li>dans toutes les autres colonnes, indiquer sous la forme <b>nombre/sommet</b> les nouvelles valeurs pour Dist() et Pred() séparées par une barre oblique, sans espaces. 
Laisser vide si la valeur ne change pas.</li>
</ul>
</i>

IMAGE DU GRAPHE

${executionTraceQuestion(et.getColumnNames(), tableLines, pointsEt)}   

<h4>Distances (${pointsDistances/10}%) et prédécesseurs (${pointsPredecessors/10}%)</h4>

IMAGE DU GRAPHE

<p><i>Pour chaque sommet, donner la distance depuis $start, ou écrire <b>inf</b> si distance infinie.</i></p> 

${distancesQuestion(graph, dist, pointsDist)}

<p><i>Pour chaque sommet, donner son prédécesseur dans l'arbre des plus courts chemins depuis $start, ou écrire <b>aucun</b> si pas de prédécesseur.</i></p>

${predecessorsQuestion(graph, pred, start, pointsPred)}

<h4>Trouver deux plus courts chemins (${pointsChemin/10}% chacun)</h4>

<p><i>Donner chacun des chemins sous la forme <b>[x,y,z]</b> càd entre crochets, sommets séparés par des virgules, sans espaces.</i></p>

<p>Plus court chemin <b>de ${path1.first()} à ${path1.last()}</b> : ${pathQuestion(path1, pointsChemin)}</p>
<p>Plus court chemin <b>de ${path2.first()} à ${path2.last()}</b> : ${pathQuestion(path2, pointsChemin)}</p>
"""
        )
        File(fileName("question", "cloze")).writeText(s.toString())
    }

    private fun fileName (nameComplement: String,
                          extension: String) : String {
            return "$outputFolderName/$graphName-$nameComplement.$extension"
    }

    /** Returns (b,i) with
     * - b iff Dijkstra on this graph is ambiguous, ie choice between two nodes at same distance
     * - i the number of inaccessible nodes when non-ambiguous, -1 when ambiguous
     */
    private fun dijkstraIsAmbiguousAndNumberInaccessible (graph: Graph, vertex: Vertex) : Pair<Boolean, Int> {

        val temp = graph.vertexSet().toMutableSet()
        val dist = graph.vertexSet().associateWith { Double.POSITIVE_INFINITY }.toMutableMap()
        dist[vertex] = 0.0

        while (temp.isNotEmpty()) {
            val minDist = dist.entries.filter { it.key in temp }.minByOrNull { it.value }!!.value
            if (minDist == Double.POSITIVE_INFINITY)
                break
            val nodesWithMinDistance = dist.entries.filter { it.value == minDist }
            if (nodesWithMinDistance.size > 1)
                return Pair(true, -1)

            val s = nodesWithMinDistance.first().key
            temp.remove(s)
            val edgeWeightMap = Dijkstra.nonVisitedSuccessorsWithWeights(
                graph,
                s
            ) { v -> temp.contains(v) }
            for ((successor, weight) in edgeWeightMap) {
                val altDist = dist[s]!! + weight
                if (dist[successor]!! > altDist) {
                    dist[successor] = altDist
                }
            }
        }
        val inaccessibleNodes = dist.entries.filter{ it.value == Double.POSITIVE_INFINITY }
        return Pair(false, inaccessibleNodes.size)
    }

    private fun writeGraphTikz (graph: Graph) {
        File(fileName("graph", "tex"))
            .writeText(GraphOnGridTikzFormatter.standaloneTikzPicture(graph, GraphOnGridTikzFormatter.o_abc_def_ghi_p_drawInstructions))
    }
    private fun writeResultPng (graph: Graph, result: Result, start: Vertex) {
        DotIo(graph, result).serialize(fileName("result", "dot"))
        CommandLineUtil.executeDotToPng(fileName("result", "dot"), fileName("result-from-$start", "png"))
    }

    companion object {
        private fun executionTraceQuestion (columnNames: List<String>,
                                            tableLines: List<Map<String, String>>,
                                            points: Int) : String {
            val s = StringBuilder()
            ClozeFormatter.clozeQuestionTable(s,
                columnNames,
                tableLines,
                points,
                setOf(0))
            return s.toString()
        }
        private fun distancesQuestion(graph: Graph, dist: Map<Vertex, Double>, points: Int) : String {
            val s = StringBuilder()
            val vertices = graph.vertexSet().sorted()
            val columnNames = vertices.map{ "Dist($it)"}
            val line = vertices.associate{
                val d = dist[it]!!
                Pair("Dist($it)", if (d == Double.POSITIVE_INFINITY) "inf" else Util.weightToString(d))
            }
            ClozeFormatter.clozeQuestionTable(s, columnNames, listOf(line), points)
            return s.toString()
        }
        private fun predecessorsQuestion (graph: Graph, pred: Map<Vertex, Vertex>, start: Vertex, points: Int) : String {
            val s = StringBuilder()
            val vertices = graph.vertexSet().filter{ it != start }.sorted()
            val columnNames = vertices.map{ "Pred($it)" }
            val line =  vertices.associate {
                Pair("Pred($it)", pred[it] ?: "aucun")
            }
            ClozeFormatter.clozeQuestionTable(s, columnNames, listOf(line), points)
            return s.toString()
        }
        private fun twoPaths (dist: Map<Vertex, Double>, result: OptimalPathResult, start: Vertex) : Pair<List<Vertex>,List<Vertex>> {
            val sortedDistances = dist.entries.sortedByDescending { it.value }.toMutableList()
            while (sortedDistances[0].value == Double.POSITIVE_INFINITY)
                sortedDistances.removeAt(0)
            val end1 = sortedDistances[0].key
            val end2 = sortedDistances[1].key
            val bfssp = BFSShortestPath(result.tree)
            val path1 = bfssp.getPath(start, end1).vertexList
            val path2 = bfssp.getPath(start, end2).vertexList
            return Pair(path1, path2)
        }
        fun pathQuestion(path: List<Vertex>, points: Int): String {
            val answer = path.joinToString(prefix = "[", postfix = "]", separator = ",")
            val s = StringBuilder()
            s.appendLine(ClozeFormatter.clozeQuestion(answer, points))
            return s.toString()
        }
        private fun nombreCasesARemplir(lines: List<Map<String, String>>): Int {
            var result = 0
            for (line in lines.subList(1, lines.size)) {
                result += line.entries.count { it.value.isNotEmpty() }
            }
            return result
        }
    }
}

typealias NodeCoord = Pair<Int,Int>
typealias EdgeCoord = Pair<NodeCoord, NodeCoord>

fun NodeCoord.x() = first
fun NodeCoord.y() = second
fun EdgeCoord.src() = first
fun EdgeCoord.tgt() = second

/** Allows to generate a graph which nodes are letters "a", "b", ... and are arranged on a grid. */
class GridGraphGenerator (val nbNodeLines : Int = 3,
                          val nbNodeColumns : Int = 3,
                          excluedNodes : Set<Vertex> = setOf()) {

    private val xrange = IntRange(0, nbNodeLines-1)
    private val yrange = IntRange(0, nbNodeColumns-1)
    private val nodesSet = CharRange('a', 'a' + (nbNodeLines * nbNodeColumns - 1))
        .map { it.toString() }
        .minus(excluedNodes)

    private fun toNode(nodeCoord: NodeCoord): String {
        return "${('a'.code + (nodeCoord.x() * nbNodeColumns + nodeCoord.y())).toChar()}"
    }

    private fun toCoordinates (node: Vertex) : NodeCoord {
        val nodeRank = (node[0] - 'a')
        val x = nodeRank / nbNodeColumns
        val y = nodeRank % nbNodeColumns
        return Pair(x,y)
    }

    private fun outgoingEdges (node: NodeCoord) : List<EdgeCoord> {
        val result = mutableListOf<EdgeCoord>()
        val (x,y) = node
        for (i in -1 .. 1)
            for (j in -1 .. 1)
                if ((i!=0 || j!=0) && x+i in xrange && y+j in yrange) {
                    val targetNodeCoord = NodeCoord(x + i, y + j)
                    if (toNode(targetNodeCoord) in nodesSet) // TODO : test si le sommet fait partie du graphe
                        result.add(EdgeCoord(node, targetNodeCoord))
                }
        return result
    }

    fun generateGraph (graphName: String, nbEdges: Int, grappType: GrappType) : Graph {
        // TODO ceci n'est pas valide si on exclut des sommets
        val maxPossibleNbEdges = nbNodeColumns*(nbNodeLines-1) + (nbNodeColumns-1)*nbNodeLines + (nbNodeColumns-1)*(nbNodeLines-1)
        if (nbEdges > maxPossibleNbEdges)
            throw IllegalArgumentException("Impossible to generate a graph with as many edges.")

        val allPossibleEdges = mutableSetOf<EdgeCoord>()
        for (node in nodesSet)
            allPossibleEdges.addAll(outgoingEdges(toCoordinates(node)))

        val edges = mutableListOf<EdgeCoord>()

        while (edges.size != nbEdges) {
            val edge = allPossibleEdges.random()
            allPossibleEdges.remove(edge)
            if (canBeAdded(edge, edges))
                edges.add(edge)
        }

        val values = if (grappType.isWeighted()) (1..nbEdges).map {it.toDouble()}.toMutableSet() else mutableSetOf()
        val randomValue : () -> Double = {
            val result = values.random()
            values.remove(result)
            result
        }

        val graph = Graphs.graphFromNodeSet(grappType, nodesSet.map{ "$it" }.toList())
        graph.setName(graphName)
        for (edge in edges) {
            if (grappType.isWeighted())
                Graphs.addEdge(graph, toNode(edge.src()), toNode(edge.tgt()), randomValue())
            else
                Graphs.addEdge(graph, toNode(edge.src()), toNode(edge.tgt()))
        }
        return graph
    }

    fun randomVertex () : Vertex {
        return nodesSet.random().toString()
    }

    fun firstVertex () : Vertex {
        return nodesSet.first()
    }

    companion object {
        private fun canBeAdded (edge: EdgeCoord, to: Collection<EdgeCoord>) : Boolean {
            val isOpposite : (EdgeCoord, EdgeCoord) -> Boolean = { e1, e2 ->
                e1.src() == e2.tgt() && e1.tgt() == e2.src()
            }
            for (e in to)
                if (e == edge || isOpposite(edge,e) || areCrossing(edge,e))
                    return false
            return true
        }

        private fun areCrossing (edge: EdgeCoord, anotherEdge: EdgeCoord) : Boolean {

            val topToBottom : (EdgeCoord) -> EdgeCoord = { e ->
                if (e.src().x() > e.tgt().x())
                    Pair(e.tgt(),e.src())
                else
                    Pair(e.src(), e.tgt())
            }

            val isDiagonalLeftToRight : (EdgeCoord) -> Boolean = { e ->
                e.src().x() + 1 == e.tgt().x() && e.src().y() + 1 == e.tgt().y()
            }
            val isDiagonalRightToLeft : (EdgeCoord) -> Boolean = { e ->
                e.src().x() + 1 == e.tgt().x() && e.src().y() == e.tgt().y() + 1
            }

            val sourcesAreSameLineNeighboursLeftAndRight : (EdgeCoord, EdgeCoord) -> Boolean = { e1, e2 ->
                e1.src().x() == e2.src().x() && e1.src().y() + 1 == e2.src().y()
            }

            val edge = topToBottom(edge)
            val anotherEdge = topToBottom(anotherEdge)

            return sourcesAreSameLineNeighboursLeftAndRight(edge, anotherEdge) &&
                isDiagonalLeftToRight(edge) && isDiagonalRightToLeft(anotherEdge)
                ||
                sourcesAreSameLineNeighboursLeftAndRight(anotherEdge, edge) &&
                isDiagonalLeftToRight(anotherEdge) && isDiagonalRightToLeft(edge)
        }
    }
}

object GraphOnGridTikzFormatter {

    fun standaloneTikzPicture (graph: Graph,
                               drawInstructions : Map<String, String>,
                               edgeLabel: (Edge) -> String = LatexSerializer.defaultEdgeLabel(graph.isWeighted())) : String {
        val s = StringBuilder()
        s.append(
            """
\documentclass{standalone}
\usepackage{tikz}
\usetikzlibrary{arrows,automata,chains,matrix,positioning,scopes,arrows.meta}
\begin{document}
"""
        )
        graph.setDrawInstructions(drawInstructions)
        LatexSerializer.serializeToTikz(graph, s, false, edgeLabel)
        s.appendLine("\\end{document}")
        return s.toString()
    }

    /** Pour graphe normal ou réseau de transport avec source 'o' et puits 'p' */
    val o_abc_def_ghi_p_drawInstructions = mutableMapOf<String,String>().apply {
        this["o"] = "-1,-1"
        this["p"] = "3,-1"
        for (c in 'a' .. 'i') {
            val rank = c - 'a'
            this["$c"] = "${rank % 3},${-rank / 3}"
        }
        val positions : Map<String, List<String>> = mapOf(
            "above" to listOf("ab", "bc", "de", "ef"),
            "above right" to listOf("ae", "bf", "dh", "ei"),
            "above left" to listOf("bd", "ce", "eg", "fh"),
            "below" to listOf("gh", "hi"),
            "left" to listOf("ad", "dg"),
            "right" to listOf("cf", "fi"))
        for ((pos, nodes) in positions) {
            for (node in nodes) {
                this["${node[0]}/${node[1]}"] = ";$pos"
                this["${node[1]}/${node[0]}"] = ";$pos"
            }
        }
    }

    /** Pour graphe normal ou potentiel-tâches avec alpha = 'q' et omega = 'w' */
    val q_abc_def_ghi_w_coordinates : (String) ->  Pair<Int, Int> = { c ->
        when (c) {
            "q" -> Pair(-1,-1)
            "w" -> Pair(3,-1)
            else -> {
                val rank = c[0] - 'a'
                Pair(rank % 3, -rank / 3)
            }
        }
    }



    /** Pour graphe avec 7 sommets */
    val abc_defg_coordinates : (String) ->  Pair<Int, Int> = { c ->
        when (c) {
            "g" -> Pair(3,-1)
            else -> {
                val rank = c[0] - 'a'
                Pair(rank % 3, -rank / 3)
            }
        }
    }

}