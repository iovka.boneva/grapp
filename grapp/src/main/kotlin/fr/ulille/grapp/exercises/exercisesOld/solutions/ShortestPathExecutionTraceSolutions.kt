package fr.ulille.grapp.exercises.exercisesOld.solutions

import fr.ulille.grapp.algorithms.Bellman
import fr.ulille.grapp.algorithms.BellmanFord
import fr.ulille.grapp.algorithms.Dijkstra
import fr.ulille.grapp.algorithms.observation.*
import fr.ulille.grapp.graph.Graph
import fr.ulille.grapp.graph.Vertex
import java.lang.IllegalArgumentException

object ShortestPathExecutionTraceSolutions {

    /** Runs the dijkstra algorithm and returns an observer from which we can retrieve the execution trace table and the list of columns.
     * The string parameters <something>ObservedName allow to indicate the names of the columns that corresponds to the observed objects.
     * If such string starts with underscore, then this column would not be included in the execution trace table.
     */
    fun runDijkstraForExecutionTraceTable (graph: Graph,
                                           startNode : Vertex,
                                           tempObservedName : String = "Temp",
                                           distObservedName : String = "dist",
                                           predObservedName : String = "predecessor",
                                           currentVertexObservedName : String = "s") : ExecutionTraceInTableObserver {

        if (tempObservedName.isEmpty() || distObservedName.isEmpty() || predObservedName.isEmpty()
                || currentVertexObservedName.isEmpty())
            throw IllegalArgumentException("the observed names cannot be empty")

        val algorithm = Dijkstra(graph)
        val observer = ExecutionTraceInTableObserver()

        val temp = OSet<Vertex>()
        val dist = OMap<Vertex, Double>()
        val pred = OMap<Vertex, Vertex>()
        val sommetCourant = OVar<Vertex>()
        val sommetSecondaire = Var<Vertex>()

        algorithm.initObservation(algorithm, observer, "Dijkstra algorithm")
        initObserver(observer, graph,
                Triple(ObservableOutputKind.TO_STRING, currentVertexObservedName, sommetCourant),
                Triple(ObservableOutputKind.NODE_DISTANCE, distObservedName, dist),
                Triple(ObservableOutputKind.TO_STRING, predObservedName, pred),
                Triple(ObservableOutputKind.TO_STRING, tempObservedName, temp));

        algorithm.run(startNode, dist, pred, temp, sommetCourant, sommetSecondaire)
        return observer
    }



    /** {@see runDijkstraForExecutionTraceTable} */
    fun runBellmanForExecutionTraceTable (graph: Graph,
                                          startNode : Vertex,
                                          distObservedName : String = "dist",
                                          predObservedName : String = "predecessor",
                                          currentVertexObservedName : String = "s") : ExecutionTraceInTableObserver {

        if (distObservedName.isEmpty() || predObservedName.isEmpty()
                || currentVertexObservedName.isEmpty())
            throw IllegalArgumentException("the observed names cannot be empty")

        val algorithm = Bellman(graph)
        val observer = ExecutionTraceInTableObserver()

        val dist = OMap<Vertex, Double>()
        val pred = OMap<Vertex, Vertex>()
        val sommetCourant = OVar<Vertex>()

        algorithm.initObservation(algorithm, observer, "Bellman Algorithm")
        initObserver(observer, graph,
                Triple(ObservableOutputKind.TO_STRING, currentVertexObservedName, sommetCourant),
                Triple(ObservableOutputKind.NODE_DISTANCE, distObservedName, dist),
                Triple(ObservableOutputKind.TO_STRING, predObservedName, pred))

        algorithm.run(startNode, dist, pred, sommetCourant)
        return observer
    }


    /** {@see runDijkstraForExecutionTraceTable} */
    fun runBellmanFordForExecutionTraceTable (graph: Graph,
                                              startNode : Vertex,
                                              distObservedName : String = "dist",
                                              predObservedName : String = "predecessor",
                                              currentIterationObservedName : String = "iteration") : ExecutionTraceInTableObserver {

        if (distObservedName.isEmpty() || predObservedName.isEmpty()
                || currentIterationObservedName.isEmpty())
            throw IllegalArgumentException("the observed names cannot be empty")

        val algorithm = BellmanFord(graph)
        val observer = ExecutionTraceInTableObserver()

        val dist = OMap<Vertex, Double>()
        val pred = OMap<Vertex, Vertex>()
        val iteration = OVar<Int>()

        algorithm.initObservation(algorithm, observer, "Bellman-Ford Algorithm")
        initObserver(observer, graph,
                Triple(ObservableOutputKind.TO_STRING, currentIterationObservedName, iteration),
                Triple(ObservableOutputKind.NODE_DISTANCE, distObservedName, dist),
                Triple(ObservableOutputKind.TO_STRING, predObservedName, pred))

        algorithm.run(startNode, dist, pred, iteration)
        return observer
    }

    private enum class ObservableOutputKind {NODE_DISTANCE, TO_STRING}

    private fun initObserver (observer: ExecutionTraceInTableObserver,
                              graph: Graph,
                              vararg observablesInfo: Triple<ObservableOutputKind, String, Observable>) {

        for ((kind, name, observable) in observablesInfo) {
            observable.initObservation(observable, observer, name)
            if (! name.startsWith("_")) {
                when (kind) {
                    ObservableOutputKind.NODE_DISTANCE ->
                        for (s in graph.vertexSet()) {
                            val (name, valueExtractor) = nodeDistanceValueExtractor(name)(s)
                            observer.addColumn(name, valueExtractor)
                        }
                    ObservableOutputKind.TO_STRING ->
                        observer.addColumn(name, toStringValueExtractor(name))
                }
            }
        }
    }

    private fun nodeDistanceValueExtractor (distanceObservableName : String) : (Vertex) -> Pair<String, (Observer)->String> =
            { node: Vertex ->
                Pair("$distanceObservableName(${node})") { o: Observer ->
                    val d = (o.getObserved(distanceObservableName) as Map<Vertex, Double>)[node];
                    if (d == Double.POSITIVE_INFINITY) "inf" else d.toString()
                }
            }

    private fun toStringValueExtractor (observedName : String) : (Observer) -> String =
            { o ->
                val result = o.getObserved(observedName).toString()
                if (result == "null") "" else result
            }
}