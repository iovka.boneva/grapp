package fr.ulille.dutinfo.graphes.exercises.io

import fr.ulille.grapp.graph.Edge
import fr.ulille.grapp.graph.Graph


// TODO: should be done with the usual tikz serializer, by only setting the draw instructions for nodes on a grid
object TikzNodesOnGridSerializer {
    val DEFAULT_NODE_STYLE = "main node"
    val DEFAULT_TIKZ_STYLE: (Boolean) -> String = { isDirected ->
        when (isDirected) {
            true -> "graphe oriente, scale=3"
            false -> "graphe nonoriente, scale=3"
        }
    }

    fun serialize(graph: Graph,
                  nodeCoordinates: (String) -> Pair<Int, Int>,
                  tikzStyle: String = DEFAULT_TIKZ_STYLE(graph.isDirected()),
                  nodeStyle: String = DEFAULT_NODE_STYLE,
                  edgeValueToString : (Double) -> String = { value ->  if (value != Double.NaN) "$value" else ""}) : String {


        val nodesString = graph.vertexSet().joinToString(separator = "") { node ->
            "\\node[$nodeStyle] ($node) at ${nodeCoordinates(node)} {$node}};\n"
        }

        // Needed for undirected graph
        val drawnEdges = mutableSetOf<Pair<String, String>>();
        val edgeText: (Edge) -> String = { edge ->
            if (drawnEdges.contains(Pair(edge.tgt, edge.src))) ""
            else {
                drawnEdges.add(Pair(edge.src, edge.tgt))
                "(${edge.src}) edge ${edgeValueToString(edge.weight)} (${edge.tgt})\n"
            }
        }
        val edgesString = graph.edgeSet().joinToString(separator = "") { edge -> edgeText(edge) }

        return """\begin{tikzpicture}[$tikzStyle]
            $nodesString
            \path
            $edgesString;
            \end{tikzpicture}
        """.trimIndent()
    }
}