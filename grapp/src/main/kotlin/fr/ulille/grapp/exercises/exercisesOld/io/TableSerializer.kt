package fr.ulille.grapp.exercises.exercisesOld.io

import java.lang.StringBuilder

// TODO: à quoi ça sert ?
object TableSerializer {

    val clozeTableStyle= "style=\"border-style: solid; border-width: 2px; text-align: center\""
    val clozeThStyle = "style=\"border-bottom: 2px solid; border-right: 1px solid\""
    val clozeTdStyle = "style=\"border-style:solid; border-width: 1px\""

    /** Serialise en tableau latex. */
    fun latexTable(columnNames: List<String>, table: List<Map<String, String>>): String {
        val s = StringBuilder()
        s.append("\\begin{tabular}")
        s.append(columnNames.joinToString(separator = "|", prefix = "{|", postfix = "|}") { "l" })
        s.append("\n\\hline\n")
        s.append(columnNames.joinToString(separator = "&") { latexify(it) })
        s.append("\\\\\\hline\n")
        for (line in table) {
            s.append(columnNames.map { line[it] }
                    .joinToString(separator = "&") { if (it == null) "" else latexify(it) })
            s.append("\\\\\\hline\n")
        }
        s.append("\\end{tabular}")
        return s.toString()
    }

    /** Serialize en tableau cloze où les cases sont des questions (sauf la ligne des titres et les cases sur les lignes ou colonnes exclues) */
    fun clozeQuestionTable(columnNames: List<String>,
                             table: List<Map<String, String>>,
                             indicesOfLinesThatAreNotQuestions : Set<Int> = setOf(),
                             namesOfColumnsThatAreNotQuestions: Set<String> = setOf()): String {
        val s = StringBuilder()
        s.append("<table $clozeTableStyle>\n")
        s.append(clozeTableHeader(columnNames))
        s.append("<tbody>\n")
        for (lineIndex in table.indices) {
            val line = table[lineIndex]
            s.append("<tr>\n")
            for ((colName, value) in line) {
                val valueString =
                        if (indicesOfLinesThatAreNotQuestions.contains(lineIndex) || namesOfColumnsThatAreNotQuestions.contains(colName)) value
                        else clozeQuestion(value)
                s.append("<td $clozeTdStyle>$valueString</td>\n")
            }
            s.append("</tr>\n")
        }
        s.append("</tbody>\n")
        s.append("</table>")

        return s.toString()
    }

    fun clozeQuestion(answer: String): String {
        when {
            answer.isEmpty() -> return "{0:SA:=.}"
            answer.toIntOrNull() != null -> return "{1:NM:=$answer}"
            else -> return "{1:SA:=$answer}"
        }
    }

    fun clozeTableHeader (columnNames: List<String>) : String {
        val s = StringBuilder()
        s.append("<thead><tr>\n")
        for (cname in columnNames) {
            s.append("<th $clozeThStyle>$cname</th>\n")
        }
        s.append("</tr></thead>\n")
        return s.toString()
    }


    fun latexify(s: String): String {
        return s.replace("{", "\\{").replace("}", "\\}");
    }

}