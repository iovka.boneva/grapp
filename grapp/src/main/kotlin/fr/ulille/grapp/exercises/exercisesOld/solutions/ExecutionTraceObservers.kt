package fr.ulille.grapp.exercises.exercisesOld.solutions

import fr.ulille.grapp.algorithms.observation.*


/** Allows to construct a table for the execution trace.
 * Usages must specify the columns of the table, then running the algorithm will add information in these tables. */
open class ExecutionTraceInTableObserver : Observer() {

    /** column name -> function that allows to get a string representation of the value of the corresponding column during the execution of the algorithm
     * Typically a column is dedicated to some observed object. For instance, it can be the currently best known distance to some node.
     * The observer knows these objects, and the functions stored in this map define how to ask the observer for the particular value. */
    private val columns = mutableMapOf<String, (Observer)->String>()

    /** The lines of the table being generated.
     * Contains the value of each column for each line of the table. */
    private val table = mutableListOf<Map<String,String>>()

    /** Adds a column name and a function that allows to extract a value for this column at every point of the execution. */
    fun addColumn (name: String, valueExtractor: (Observer) -> String)  {
        columns[name] = valueExtractor
    }

    /** Typically, a new line is being generated for every iteration of the main loop. */
    override fun process(e: Event)  {
        super.process(e)

        val line = mutableMapOf<String,String>()
        if (e is LoopCompleteEvent || e is InitializationCompleteEvent) {
            for (c in columns.entries) {
                line[c.key] = c.value(this)
            }
            table.add(line)
        }
    }

    fun getColumnNames () : List<String> {
        return columns.keys.toList()
    }

    /** Returns the lines of the table and for any line, the value for any of the columns. */
    fun getTableLines () : List<Map<String,String>> {
        return table
    }
}

/** The columns are exactly the data structures that are being observed. */
class DefaultExecutionTraceInTableObserver : ExecutionTraceInTableObserver() {

    /** Adds all the registered observed objects as columns of the table.
     * Uses the names of the registered observed as column names.
     * To be called after all observed have been registered. */
    fun addTableColumns () {
        for ((name, observed) in observed.filter { it.value is ODataStructure })
            addColumn(name) { observed.toString() }
    }
}

