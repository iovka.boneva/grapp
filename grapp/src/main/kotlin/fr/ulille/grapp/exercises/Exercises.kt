/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises

import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.int
import fr.ulille.grapp.ui.Messages
import fr.ulille.grapp.ui.MyCliktCommand

object Exercises : MyCliktCommand(help= Messages.convertHelp, printHelpOnEmptyArgs = true) {

    val algorithm by option("-algorithm", help=Messages.convertOutFormatHelp)
        .choice("Dijkstra", "Pert", "Flow")
        .required()
    val nb by option("-nb", help="Nombre de graphes à générer")
        .int()
        .required()
    val name by option("-name")
        .required()
    val directed by option("-directed")
        .flag()

    override fun run () {
        when (algorithm) {
            "Dijkstra" -> for (i in 1 .. nb)
                DijkstraExercise(".", "$name$i", directed).generate()
            "Pert" -> PertExercises(".", name).generate(nb)
            "Flow" -> {
                /*val genMaxflowProperties = MaximumFlowExercise(".", name)
                for (i in 1 .. nb)
                    genMaxflowProperties.generateMaximumFlowPropertiesQuestion(i)*/
                val genAugmentFlow = MaximumFlowExercise(".", name)
                for (i in 1 .. nb)
                    genAugmentFlow.generateAugmentFlowQuestion(i)
            }
        }
    }
}

fun main(args: Array<String>) {
    Exercises.main(args)
}