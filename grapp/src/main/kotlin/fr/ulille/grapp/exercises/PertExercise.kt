/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises


import fr.ulille.grapp.algorithms.Bellman
import fr.ulille.grapp.algorithms.OptimalityCriterion
import fr.ulille.grapp.algorithms.PathCombinationOperation
import fr.ulille.grapp.graph.*
import fr.ulille.grapp.io.DotIo
import fr.ulille.grapp.io.YamlParser
import fr.ulille.grapp.io.YamlSerializer
import fr.ulille.grapp.ui.AlgorithmsUtil
import fr.ulille.grapp.ui.CommandLineUtil
import fr.ulille.grapp.ui.OptimalPathKind
import fr.ulille.grapp.ui.OptimalPathResult
import org.jgrapht.alg.shortestpath.AllDirectedPaths
import org.jgrapht.alg.shortestpath.BFSShortestPath
import java.io.File
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import kotlin.io.path.extension
import kotlin.io.path.inputStream
import kotlin.io.path.nameWithoutExtension
import kotlin.random.Random

/** With each task associates an int duration and a list of predecessor tasks. */
data class TaskInfo (val duration: Int, val predecessors: List<String>)
typealias PertProblem = Map<String, TaskInfo>
/** Represents a solution of the PertProblem */

/** Information about the acceptable solutions of the problem.
 * @param graph is the Pert graph
 * @param additionalEdges corresponds to the edges that are induced by the problem definition, but should be removed from the Pert graph because belong to the transitive closure for the minimal graph
 */
data class PertGraphAcceptableSolutions (val graph: DirectedWeightedGraph, val additionalEdges: Set<Edge>)

data class PertSolution (val graph: PertGraphAcceptableSolutions,
                         val dureeMinimale: Int,
                         val datesPlusTot: Map<String, Int>,
                         val datesPlusTard: Map<String, Int>,
                         val cheminCritique : List<String>)

/** Pert exercices are generated in a batch, as we want to guarantee that two different exercices
 * would have different sets of tasks (used for automatic grading). */
class PertExercises(private val outputFolderName: String, private val graphNamePrefix: String) {

    val pointsDureeMinimale = 5
    val pointsParDatePlusTot = 2
    val pointsParDatePlusTard = 2
    val pointsCheminCritique = 7

    fun generate (nbExercises : Int) {

        // Generate as many problems as requestednecessary with different sets of nodes
        val nodeSets = mutableSetOf<String>()

        while (nodeSets.size < nbExercises) {
            val ns = randomString()
            if (nodeSets.contains(ns))
                continue

            val problem = PertProblemGenerator.generate(ns.toCharArray().map { it.toString() })
            val solution = computePert(problem) ?: continue
            nodeSets.add(ns)
            print(".")

            val fileNameBase = "$outputFolderName/$graphNamePrefix-${nodeSets.size}"

            // Write the graph
            solution.graph.graph.setName("pert $ns")
            val x = StringBuilder()
            YamlSerializer.serialize(solution.graph.graph,x)
            File("$fileNameBase-pertgraph.yaml").writeText(x.toString())

            // Write the graph annotated with the dates
            val solutionGraphToDraw = solutionGraphToDraw(solution)
            solutionGraphToDraw.setName("pert solution $ns")
            DotIo(solutionGraphToDraw, null).serialize("$fileNameBase-graph-with-dates.dot")
            CommandLineUtil.executeDotToPng("$fileNameBase-graph-with-dates.dot", "$fileNameBase-graph-with-dates.png")

            // Write the cloze file
            val s = StringBuilder()
            s.append("""
<p>Voici les tâches d'un projet:</p>
${problemDefinitionTable(problem)}
<p>Vous devez:</p>
<ul>
<li>déterminer la durée minimale du projet, les dates au plus tôt et les dates au plus tard des tâches pour 70% de la note,</li>
<li>donner le graphe potentiel-tâches associé au projet, au format Grapp YAML, pour 30% de la note.</li>
</ul>
<p>Quelle est la durée minimale du projet ? ${ClozeFormatter.clozeQuestion(solution!!.dureeMinimale.toString(), pointsDureeMinimale)}</p>
<br><p>Pour chaque tâche, donner sa date au plus tôt.</p>
${datesTable(solution!!.datesPlusTot, pointsParDatePlusTot, "Tôt")}
<br><p>Pour chaque tâche, donner sa date au plus tard.</p>
${datesTable(solution!!.datesPlusTard, pointsParDatePlusTard, "Tard")}
<br><p>Donner un chemin critique sous la forme <b>[$alpha,A,B,C,$omega]</b> càd entre crochets, sommets séparés par des virgules, sans espaces, en incluant les sommets $alpha et $omega.</p> 
<p><i>La correction étant automatique, <b>respectez strictement le format de réponse demandé.</b></i></p> 
<p>Chemin critique: ${DijkstraExercise.pathQuestion(solution.cheminCritique, pointsCheminCritique)}</p>
"""
            )
            File("$fileNameBase.cloze").writeText(s.toString())
        }
    }

    companion object {
        val alpha = "alpha"
        val omega = "omega"
        val alphaOmegaSet = setOf(alpha, omega)


        private fun datesTable(dates: Map<String, Int>, pointsParDate: Int, columnTitle: String) : String {
            val s = StringBuilder()
            val tasks = dates.keys.sorted().minus(alphaOmegaSet)
            val columnNames = tasks.map{ "$columnTitle($it)"}
            val line = tasks.associate{
                Pair("$columnTitle($it)", dates[it]!!.toString())
            }
            ClozeFormatter.clozeQuestionTable(s, columnNames, listOf(line), pointsParDate)
            return s.toString()
        }

        private fun problemDefinitionTable (problem: PertProblem) : String {
            val s = StringBuilder()
            val columnNames = listOf("Tâche", "Durée", "Tâches antérieures")
            val table = problem.entries.sortedBy { it.key }.map { (task, taskInfo) ->
                mapOf(
                    "Tâche" to task,
                    "Durée" to taskInfo.duration.toString(),
                    "Tâches antérieures" to taskInfo.predecessors.joinToString())
            }.toList()
            ClozeFormatter.clozeQuestionTable(s, columnNames, table, 0,
                (table.indices).toSet(),
                columnNames.toSet())
            return s.toString()
        }

        private fun toPertGraph (problem: PertProblem) : PertGraphAcceptableSolutions {
            val graph = Graphs.graphFromNodeSet(GrappType.diwgraph, problem.keys.union(alphaOmegaSet))
            for ((succ, taskInfo) in problem.entries) {
                for (pred in taskInfo.predecessors) {
                    Graphs.addEdge(graph, pred, succ, problem[pred]!!.duration.toDouble())
                }
            }
            for (vertex in graph.vertexSet().minus(alphaOmegaSet))
                if (graph.incomingEdgesOf(vertex).isEmpty())
                    Graphs.addEdge(graph, alpha, vertex, 0.0)
                else if (graph.outgoingEdgesOf(vertex).isEmpty())
                    Graphs.addEdge(graph, vertex, omega, problem[vertex]!!.duration.toDouble())

            // Identify and remove the unnecessary edges
            val unnecessaryEdges = mutableSetOf<Edge>()
            val allPaths = AllDirectedPaths(graph)
            for (edge in graph.edgeSet())
                if (allPaths.getAllPaths(edge.src, edge.tgt, true, null).size > 1)
                    unnecessaryEdges.add(edge)
            for (edge in unnecessaryEdges)
                graph.removeEdge(edge)
            return PertGraphAcceptableSolutions(graph as DirectedWeightedGraph, unnecessaryEdges)
        }

        val allPossibleTasks = ('A'..'Z').joinToString(separator="")
        private fun randomString () : String {
            val indices = mutableSetOf<Int>()
            for (i in 1..PertProblemGenerator.nbTasks)
                indices.add(Random.nextInt(allPossibleTasks.length))
            while (indices.size != PertProblemGenerator.nbTasks)
                indices.add(Random.nextInt(allPossibleTasks.length))

            return indices.sorted().joinToString(separator="") { allPossibleTasks[it].toString() }
        }

        private fun reverse (graph: DirectedWeightedGraph) : DirectedWeightedGraph {
            val result = Graphs.graphFromNodeSet(GrappType.diwgraph, graph.vertexSet())
            for (edge in graph.edgeSet())
                Graphs.addEdge(result, edge.tgt, edge.src, edge.weight)
            return result as DirectedWeightedGraph
        }

        /** Computes the solution of a pert problem
         * Returns null if the problem does not have the desired properties : one critical path and all tasks are on a path from alpha to omega.
         */
        private fun computePert(problem: PertProblem) : PertSolution? {
            val pertGraph = toPertGraph(problem)
            val graph = pertGraph.graph
            val bellman = Bellman(graph, PathCombinationOperation.Add, OptimalityCriterion.Max)
            val dist = mutableMapOf<Vertex, Double>()
            val pred = mutableMapOf<Vertex, Vertex>()
            bellman.compute(alpha, dist, pred)
            val tot = dist.entries.associate { Pair(it.key, it.value.toInt()) }
            val dureeMinimale = dist[omega]!!.toInt()

            val reverseGraph = reverse(graph)
            val reverseBellman = Bellman(reverseGraph, PathCombinationOperation.Add, OptimalityCriterion.Max)
            val reverseDist = mutableMapOf<Vertex, Double>()
            reverseBellman.compute(omega, reverseDist, mutableMapOf())
            val tard = reverseDist.entries.associate { Pair(it.key, dureeMinimale - it.value.toInt())}

            val allPaths = AllDirectedPaths(graph).getAllPaths(alpha, omega, true, null)
            val criticalPaths = allPaths.filter { path -> path.vertexList.all { vertex -> tot[vertex]!! == tard[vertex]!! }}
            if (criticalPaths.size != 1)
                return null
            val allNodesOnPaths = allPaths.flatMap { path -> path.vertexList }.toSet()
            if (problem.keys.minus(allNodesOnPaths).isNotEmpty())
                // There is a node non-accessible from alpha or non co-accessible from omega
                return null

            val sptree = OptimalPathResult(graph, "exercise", pred, dist, AlgorithmsUtil.constructTraversalTree(graph, pred), OptimalPathKind.LONGEST_PATH)
            val bfssp = BFSShortestPath(sptree.tree)
            val criticalPath = bfssp.getPath(alpha, omega).vertexList

            return PertSolution(pertGraph,
                dureeMinimale,
                tot,
                tard,
                criticalPath)
        }

        fun solutionGraphToDraw (solution: PertSolution) : DirectedWeightedGraph {
            val nameWithDates : (Vertex) -> Vertex = {
                "${it}_${solution.datesPlusTot[it]}_${solution.datesPlusTard[it]}"
            }
            val result = Graphs.graphFromNodeSet(GrappType.diwgraph,
                solution.graph.graph.vertexSet().map(nameWithDates))
            for (edge in solution.graph.graph.edgeSet())
                Graphs.addEdge(result, nameWithDates(edge.src), nameWithDates(edge.tgt), edge.weight)
            return result as DirectedWeightedGraph
        }
    }
}

/**
 * @param pointsCorrectNodes : l'ensemble de sommets est correct
 * @param pointsCorrectEdges : l'ensemble des arêtes est correct
 */
class PertExerciseGrading (
    private val studentGraphsFolder: String,
    private val correctGraphsFolder: String,
    private val pointsCorrectNodes: Double,
    private val pointsCorrectEdges: Double) {

    val graphId : (Set<Vertex>) -> String = { vertexSet ->
            vertexSet.filter{ it != "alpha" && it != "omega" }
                .sorted()
                .joinToString(separator="")
    }

    fun grade () {
        // load all correct graphs and store them in a map with the set of vertices as key
        val correctGraphs = mutableMapOf<String, Pair<Int,Graph>>()
        for (file in Files.list(Paths.get(correctGraphsFolder))) {
            if (file.extension == "yaml") {
                val nb = file.nameWithoutExtension.split("-")[1].toInt()
                val graph = YamlParser.parse(file.inputStream())
                correctGraphs[graphId(graph.vertexSet())] = Pair(nb, graph)
            }
        }

        val grades = mutableMapOf<String,Pair<Int,String>>()

        // for every student graph, compare it with the correct graph corresponding to the set of nodes
        for (file in Files.list(Paths.get(studentGraphsFolder))) {
            val name = file.nameWithoutExtension
            try {
                val graph = YamlParser.parse(file.inputStream())
                val (graphNumber, points) = grade(correctGraphs, graph)
                grades[name] = Pair(graphNumber, points.toString())
            } catch (e: Exception) {
                grades[name] = Pair(-1, e.message!!.split("\n")[0])
            }
        }
        println("Nom;Numéro graphe;Note")
        println(grades.entries.joinToString(separator = "\n") { "${it.key};${it.value.first};${it.value.second}" })
    }

    private fun grade (correctGraphs: Map<String, Pair<Int,Graph>>, g: Graph) : Pair<Int, Double> {
        val studentGraph = uppercaseNodes(g)
        val correctGraph = correctGraphs[graphId(studentGraph.vertexSet())] ?: return Pair(-1, 0.0)

        val (nbCorrectEdge, nbCorrectEdgeAndWeight) = sameEdges(correctGraph.second, studentGraph)
        val nbRequiredEdge = correctGraph.second.edgeSet().size

        val pointsPerCorrectEdgeAndWeight = pointsCorrectEdges / nbRequiredEdge.toDouble()
        val pointsCorrectEdgeIncorrectWeight = pointsCorrectEdges / (2.0 * nbRequiredEdge.toDouble())

        val p1 = if (studentGraph.vertexSet().containsAll(PertExercises.alphaOmegaSet)) pointsCorrectNodes else pointsCorrectNodes / 2.0
        val p2 = nbCorrectEdgeAndWeight.toDouble() * pointsPerCorrectEdgeAndWeight
        val p3 = (nbCorrectEdge - nbCorrectEdgeAndWeight).toDouble() * pointsCorrectEdgeIncorrectWeight
        val points = p1 + p2 + p3

        return Pair(correctGraph.first, points)
    }

    private fun uppercaseNodes (graph: Graph) : Graph {
        if (graph.vertexSet().first { it != "alpha" && it != "omega" }[0].isUpperCase())
            return graph
        val upperCaseMap = graph.vertexSet().associateWith { if (it.length == 1) it.uppercase(Locale.getDefault()) else it}
        val g = Graphs.graphFromNodeSet(GrappType.diwgraph, upperCaseMap.values)
        for (e in graph.edgeSet()) {
            Graphs.addEdge(g, upperCaseMap[e.src]!!, upperCaseMap[e.tgt]!!, e.weight)
        }
        return g
    }

    /** Returns the number of correct edges and the number of edges with correct weight. */
    private fun sameEdges (correctGraph: Graph, studentGraph: Graph) : Pair<Int, Int> {
        var correctEdge = 0
        var correctEdgeAndWeigt = 0

        for (e in correctGraph.edgeSet()) {
            val studentGraphEdge = studentGraph.getEdge(e.src, e.tgt)
            if (studentGraphEdge != null) {
                correctEdge++
                if (studentGraphEdge.weight == e.weight)
                    correctEdgeAndWeigt ++
            }
        }

        return Pair(correctEdge, correctEdgeAndWeigt)
    }
}

object PertProblemGenerator {

    private val nbDependencies = 8
    const val nbTasks = 7
    private val minDuration = 5
    private val maxDuration = 25

    /** Generates a PERT problem instance with the given tasks and the given number of dependencies between tasks.
     * @param tasks must be a set, i.e. w/o repetitions. */
    fun generate (tasks: List<String>) : PertProblem {
        if (tasks.toSet().size != nbTasks)
            throw IllegalArgumentException("Le nombre de tâches doit être égal à 7.")

        val pdescr = problem(setOf(1,2,3).random())
        val X = tasks.subList(0, pdescr.cardX)
        val Y = tasks.subList(pdescr.cardX, pdescr.cardX + pdescr.cardY)
        val Z = tasks.subList(pdescr.cardX + pdescr.cardY, nbTasks)

        val XwoSucc = X.toMutableSet()
        val YwoPred = Y.toMutableSet()
        val YwoSucc = Y.toMutableSet()
        val ZwoPred = Z.toMutableSet()
        val durations = (1..nbDependencies).map{ Random.nextInt(minDuration, maxDuration)}.toMutableList()

        val result = mutableMapOf<String,TaskInfo>()
        X.forEach { result[it] = TaskInfo(durations.removeFirst(), emptyList())}

        val addPairToResult : (Pair<String, String>) -> Unit = { (pred, succ) ->
            val ti = result.getOrPut(succ) { TaskInfo(durations.removeFirst(), mutableListOf())}
            (ti.predecessors as MutableList).add(pred)
        }
        randomPairs(pdescr.xz, XwoSucc, X, ZwoPred, Z).forEach { addPairToResult(it) }
        randomPairs(pdescr.xy, XwoSucc, X, YwoPred, Y).forEach { addPairToResult(it) }
        randomPairs(pdescr.yz, YwoSucc, Y, ZwoPred, Z).forEach { addPairToResult(it) }
        randomPairs(pdescr.yy, YwoSucc, Y, YwoPred, Y).forEach { addPairToResult(it) }
        return result.entries.sortedBy { it.key }.associate { Pair(it.key, it.value) }
    }

    /** Selects a random element in 'from' and removes it from 'from'.
     * If from is empty, then selects a random element in otherwise. */
    private fun randomElement (from: MutableSet<String>, otherwise: Collection<String>) : String {
        return if (from.isNotEmpty()) {
            val r = from.random()
            from.remove(r)
            r
        } else
            return otherwise.random()

    }

    private fun randomPairs (nbPairs : Int,
                             first: MutableSet<String>, firstOtherwise: Collection<String>,
                             second: MutableSet<String>, secondOtherwise: Collection<String>) : Set<Pair<String, String>> {
        val result = mutableSetOf<Pair<String,String>>()
        var nb = 0
        while (nb < nbPairs) {
            val f = randomElement(first, firstOtherwise)
            val s = randomElement(second, secondOtherwise)
            val p = Pair(f,s)
            if (! result.contains(p)) {
                result.add(p)
                nb ++
            }
        }
        return result
    }

    data class ProblemTypeDescription (
        val cardX: Int, val cardY: Int, val cardZ: Int,
        val xy : Int, val yz : Int, val xz : Int, val yy: Int
    )

    /** Three different sets of parameters that influence the shape of the problem.
     * Well adapted for nbTasks = 7 and nbDependencies = 8
     * How it works:
     * - nbTasks tasks
     * - nbDependencies total number dependencies between tasks
     * - 3 subsets of tasks : X (w/o predecessor), Y (with predecessor and with successor), Z (w/o successor)
     *   The number of vertices from X to Y is denoted xy, similarly yz, xz and yy
     * Type 1:
     * - |X| = 2, |Z| = 2, |Y| = nbTasks-|X|-|Y|
     * - xz = 1
     * - xy = 3
     * - yy = 1
     * - yz = nbDependencies -xz-xy-yy
     * Type 2:
     * - |X| = 2, |Z| = 3, |Y| = nbTasks-|X|-|Y|
     * - xz = 1 or 2
     * - xy = 3
     * - yy = 0
     * - yz = nbDependencies - xz - xy - yy (so 3 or 4)
     * Type 3:
     * - symmetric to Type 2 w.r.t. X and Z
     */
    private fun problem (type: Int) : ProblemTypeDescription {
        return when (type) {
            1 -> {
                ProblemTypeDescription(
                    cardX=2, cardY=nbTasks-2-2, cardZ=2,
                    xy=3, xz=1, yz=nbDependencies-3-1-1, yy=1)
            }
            2 -> {
                val xz = setOf(1,2).random()
                ProblemTypeDescription(
                    cardX=2, cardY=nbTasks-2-3, cardZ=3,
                    xy=3, xz=xz, yz=nbDependencies-3-xz-0, yy=0)
            }
            3 -> {
                val xz = setOf(1,2).random()
                ProblemTypeDescription(
                    cardX=3, cardY=nbTasks-3-2, cardZ=2,
                    xz=xz, xy=nbDependencies-xz-3-0, yz=3, yy=0)
            }
            else ->
                throw IllegalArgumentException("Le type de problème peut être 1, 2 ou 3.")
        }
    }
}