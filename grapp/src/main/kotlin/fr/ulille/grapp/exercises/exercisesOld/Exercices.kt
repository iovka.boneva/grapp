package fr.ulille.dutinfo.graphes.exercises

// CLEAN
// TODO retester


val latexPrefix = "\n\n\\newpage\n\\large\n\\noindent\n\\hspace{-0.3cm}\n"

// TODO revoir et nettoyer
// -------------------------------------------------------------------------------------
// PARCOURS
// -------------------------------------------------------------------------------------
/*
class ReponseExerciceParcours(val expordre: OTraversalQueue<Vertex>) : Observer() {
    val ordreParcours = mutableListOf<Char>()

    override fun process(e: Event) {
        if (e.source == expordre && e is DSAddEvent)
            ordreParcours.add(e.data as Char)
    }

    fun getOrdreDeParcours () : List<Char> {
        return ordreParcours
    }
}

fun genererExerciceParcours (enLargeur: Boolean) {
    // Générer un graphe aléatoire à 9 sommets et 12 ou 13 arêtes
    val r = RandomGraphGenerator(3, 3)
    val graph = r.generateGraph(nbEdges = Random.nextInt(0,1)+12, isValuated = false, isUndirected = true)

    // Choisir le sommet de départ
    val sommet = graph.nodes().random()

    // Exécuter l'algo de parcours
    val expordre = if (enLargeur) OFifo<Vertex>() else OLifo<Vertex>()

    val observer = ReponseExerciceParcours(expordre)
    val parcours = GraphTraversal(graph)
    expordre.initObservation(expordre, observer, "File ou pile")
    val aretesVisitees = mutableListOf<Edge>()

    parcours.run(vertex = sommet, explorationQueue = expordre, discoveredVertices = aretesVisitees)
    val ordreSommets = observer.getOrdreDeParcours()

    // Générer le graphe résultat
    val reponseSommets = graph.nodes().map { "$it${if (ordreSommets.contains(it)) (ordreSommets.indexOf(it)+1).toString() else "0"}" }
    val reponse = MutableGraphAdjacencyMatrix<String, String>(reponseSommets)
    val sommetDansResultat : (Char) -> String = { c -> reponseSommets.filter { it.startsWith("$c") }.first() }

    for (edge in graph.edges()) {
        val src = sommetDansResultat(edge.origin())
        val tgt = sommetDansResultat(edge.target())

        val valeur = if (aretesVisitees.contains(edge) ) "+" else "-"
        when (valeur) {
            "+" -> {
                reponse.addEdge(src, tgt, valeur)
                reponse.removeEdge(tgt,src)
            }
            "-" -> {
                if (reponse.getEdge(tgt,src) == null)
                    reponse.addEdge(src,tgt,valeur)
            }
        }
    }

    val grapheEntree = TikzNodesOnGridSerializer.serialize(graph, GenerationExercicesUtil.o_adg_beh_cfi_p_coordinates)
    val grapheResultat = TikzNodesOnGridSerializer.serialize(graph, GenerationExercicesUtil.o_adg_beh_cfi_p_coordinates)

    File("graphe-parcours.tex").writeText("$latexPrefix$grapheEntree$latexPrefix$grapheResultat")
}
*/
// -------------------------------------------------------------------------------------
// FLOT
// -------------------------------------------------------------------------------------

/** Écrit toutes les étapes de l'algorithme au format tikz.
 * Retourne le graphe de flot complet et le flot maximal.
 */
/*
fun etapesFordFulkerson (graph: Graph, out: StringBuilder) : Triple<ResidualFlowGraph, Graph, Graph> {
    val observer : Observer = object : Observer() {
        override fun process(e: Event) {
            if (e is LoopCompleteEvent) {
                val residuel = (getObserved("residuel") as OVar<ResidualFlowGraph>).value!!
                val flot = residuel.toFlowGraph()
                val grapheDeFlot = GraphAdjacencyMatrix(flot.nodes(),
                        flot.edges().map { e -> Triple(e.origin(), e.target(), "{\\color{blue}${e.value()}}|${residuel.graph.getEdge(e.origin(),e.target()).value()}") })
                out.append(TikzNodesOnGridSerializer.serialize(grapheDeFlot, GenerationExercicesUtil.o_adg_beh_cfi_p_coordinates))
            } else if (e is PhaseChangeEvent) {
                out.append("\n\n\\vspace{1cm}C'était le flot complet\n")
            } else if (e is DSAddEvent && e.source == getObserved("chaines augm")) {
                out.append("\n\n\\vspace{1cm}Augmentation : ${e.data}\n\n\\vspace{1cm}\n")
            }
        }
    }

    val fordFulkerson = FordFulkerson(graph, 'o', 'p')
    fordFulkerson.initObservation(fordFulkerson, observer, "Algorithme")
    val residuel = OVar<ResidualFlowGraph>()
    residuel.initObservation(residuel, observer, "residuel")
    val aug = OList<Pair<List<Char>, Int>>()
    aug.initObservation(aug, observer, "chaines augm")
    val flotComplet = Var<Graph>()

    fordFulkerson.run(residuel, aug, mutableSetOf(), flotComplet)

    return Triple(residuel.value!!, flotComplet.value!!, residuel.value!!.toFlowGraph())
}
val flowGraphRanksMap = mapOf('o' to 5, 'a' to 1, 'b' to 6, 'c' to 2, 'd' to 7, 'e' to 3, 'f' to 8, 'p' to 9)

fun exerciceFlots (grapheFile: String, etapesFile : String) {

    // Utile
    val flotVersGrapheAvecFlot : (Graph, Graph) -> Graph = { graphe, flot ->
        GraphAdjacencyMatrix(graphe.vertexSet(),
                graphe.edgeSet().map { e -> Triple(e.src, e.tgt, "{\\color{blue}${flot.getEdge(e.origin(), e.target()).value()}}|${e.value()}") })
    }
    val grapheAvecFlotToString : (Graph) -> String = { g ->
        TikzNodesOnGridSerializer.serialize(g, GenerationExercicesUtil.o_adg_beh_cfi_p_coordinates)
    }

    // Parser le graphe
    val graph = SimpleGraphParser.parse(File(grapheFile).readText())

    // Exécuter l'algorithme et ajouter les étapes intermédiaires au format tikz
    // Utile pour les questions de recherche de chaine augmentante et pour déterminer si un flot est maximal
    val s = StringBuilder()
    val (residuel, flotComplet, flotMaximal) = etapesFordFulkerson(graph,s)
    File("$etapesFile.tex").writeText(s.toString())

    // Calcul de la coupe minimale
    val traversal = BreadthFirstSearch(residuel.toCleanResidual())
    println(residuel.toCleanResidual())
    val sommetsNonVisites = mutableSetOf<Char>()
    traversal.compute("o", sommetsBlancs = sommetsNonVisites)
    val coupeMinimale = "X = ${graph.nodes().minus(sommetsNonVisites)}, ComplX = $sommetsNonVisites"

    // Formater le graphe d'origine et efficher le flot complet pour les questions de construction de flot complet
    val grapheString = TikzNodesOnGridSerializer.serialize(graph, GenerationExercicesUtil.o_adg_beh_cfi_p_coordinates)


    // Écrire les graphes en tikz
    File("$grapheFile.tex").writeText("""
        $latexPrefix$grapheString
        
        \vspace{2cm}
        Le graphe d'origine
        $latexPrefix${grapheAvecFlotToString(flotVersGrapheAvecFlot(graph, flotComplet))}
        
        \vspace{2cm}
        Flot complet
        $latexPrefix${grapheAvecFlotToString(flotVersGrapheAvecFlot(graph, flotMaximal))}
        
        \vspace{2cm}
        Flot maximal, coupe : $coupeMinimale
    """.trimIndent())

    /*
    // Créer une question pour recherche de chaine augmentante
    val reponseEstMaximal = if (chaineAugmentante == null) "=oui~non" else "oui~=non"
    val coupeOuChaine =
            if (chaineAugmentante != null) "Chaine augmentante: $chaineAugmentanteString"
            else {
                // Calcul d'une coupe minimale
                val traversal = GraphTraversal(flotComplet)
                val sommetsNonVisites = mutableSetOf<Char>()
                traversal.breadthFirstSearch('o',sommetsBlancs = sommetsNonVisites, aretesVisitees = null)
                val sommetsVisites = graph.nodes().minus(sommetsNonVisites)
                "Coupe minimale : X = $sommetsVisites, ComplX = $sommetsNonVisites"
            }
    val clozeString = """<p>TODO: Insérer image</p>
        |<p>Le le flot ci-dessus est-il maximal ? {:MC:$reponseEstMaximal}</p>
        |<p>
        |Justifiez !
        |</p>
        |<p>
        |{:SA:=est maximal -> donner une coupe minimale ....................................................................}<br>
        |{:SA:=n'est pas maximal -> donner une chaine augmentante ..........................................................}<br>
        |{:SA:=Ici, $coupeOuChaine .........................................................................................}
        |</p>
    """.trimMargin()

    File("$grapheFile.cloze").writeText(clozeString)
     */
}
*/




fun main(vararg args: String) {
//    genererExerciceParcours(enLargeur = false )

   // exerciceFlots("flots-6", "etapes-flot-6")
}