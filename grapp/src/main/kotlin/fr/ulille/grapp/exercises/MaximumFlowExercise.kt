/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.exercises

import fr.ulille.grapp.graph.*
import fr.ulille.grapp.io.Util
import fr.ulille.grapp.ui.AlgorithmsUtil
import fr.ulille.grapp.ui.MaximumFlowAlgorithmName
import fr.ulille.grapp.ui.MaximumFlowResult
import org.jgrapht.alg.shortestpath.AllDirectedPaths
import java.io.File
import kotlin.math.min
import kotlin.random.Random
import kotlin.random.nextInt

class MaximumFlowExercise(private val outputFolderName: String, private val graphNamePrefix: String) {

    val pointsFlowValue = 1
    val pointsSourcePart = 1
    val pointsSinkPart = 1
    val pointsAugmentingPath = 7
    val pointsAugmentingPathCapacity = 2
    val pointsNewFlowValue = 2
    val pointsResidualGraphWeight = 1

    /** Génère un exercice de flot maximal.
     * Donné: le flot maximal. Question: valeur du flot et coupe minimale.
     */
    fun generateMaximumFlowPropertiesQuestion (suffix: Int) {
        val result = generateInterestingMaximumFlowGraph()

        val fileNameBase = "$outputFolderName/$graphNamePrefix-$suffix"
        // Générer le graphe
        val flowGraphLabel : (Edge) -> String = { edge ->
            "[${Util.weightToString(result.flowMap[edge]!!)}\$\\mid\$${Util.weightToString(edge.weight)}]"
        }
        File("$fileNameBase.tex")
                .writeText(GraphOnGridTikzFormatter.standaloneTikzPicture(result.graph, O_ACE_BDF_P_drawInstructions, flowGraphLabel))

        // Générer la question cloze
        val sourcePartOrdered = result.sourcePartition.sorted().toMutableList()
        sourcePartOrdered.remove("O")
        sourcePartOrdered.add(0, "O")
        val sinkPartOrdered = result.sinkPartition.sorted().toMutableList()
        sinkPartOrdered.remove("P")
        sinkPartOrdered.add(0, "P")
        val s = StringBuilder()
        s.append(
            """
<p>Voici un réseau de transport avec un flot maximal.</p>

IMAGE DU GRAPHE

<p><b>Veuillez respecter les majuscules et minuscules dans vos réponses.</b></p>
<p>Quelle est la valeur de ce flot ? ${ClozeFormatter.clozeQuestion(result.flowValue.toString(), pointsFlowValue)}</p>
<p>Donner la coupe minimale saturée.<br>
<i>La réponse est attendue sous la forme <b>O,X,Y,Z</b> ou <b>P,X,Y,Z</b> 
c'est à dire les sommets sont séparés par des virgules, sans espaces, 
on commence par le sommet source ou le sommet puits, les autres sommets sont donnés par ordre alphabétique.</i></p>
<p>Partie incluant la source: ${
                ClozeFormatter.clozeQuestion(
                    sourcePartOrdered.joinToString(separator = ","),
                    pointsSourcePart
                )
            }<br>
Partie incluant le puits: ${
                ClozeFormatter.clozeQuestion(
                    sinkPartOrdered.joinToString(separator = ","),
                    pointsSinkPart
                )
            }</p>
"""
        )
        File("$fileNameBase.cloze").writeText(s.toString())
    }

    fun generateAugmentFlowQuestion (suffix: Int) {
        try {
            val r = generateInterestingMaximumFlowGraph()
            val graph = r.graph
            val flowMap = r.flowMap

            val fileNameBase = "$outputFolderName/$graphNamePrefix-$suffix"
            /* debug */ //writeStandaloneLatexFlowGraph("$fileNameBase.tex", graph, flowMap)
            val (pathEdges, crossingEdge, chain) = chainToReduce(graph, flowMap) // NoSuchElementException possible
            /* debug */ //println("Chain to reduce : $pathEdges + $crossingEdge")
            val (reducedFlowMap, reduceValue) = reduceFlow(pathEdges, crossingEdge, flowMap)
            /* debug */ //println("Reduced flow map : $reducedFlowMap")
            /* debug */ //writeStandaloneLatexFlowGraph("$fileNameBase-reduced.tex", graph, reducedFlowMap)
            val (exerciseGraph, exerciseFlowMap) = removeAugmentingPaths(graph, reducedFlowMap, pathEdges)
            /* debug */ //writeStandaloneLatexFlowGraph("$fileNameBase-saturated.tex", exerciseGraph, exerciseFlowMap)

            if (isUniqueAugmentingPath(exerciseGraph, exerciseFlowMap, pathEdges, crossingEdge)) {
                writeExerciseFilesAugmentFlowQuestion(
                    fileNameBase, exerciseGraph, exerciseFlowMap,
                    chain, reduceValue, r.flowValue
                )
            } else {
                print(".")
                generateAugmentFlowQuestion(suffix)
            }
        } catch (e: NoSuchElementException) {
            print(".")
            generateAugmentFlowQuestion(suffix)
        }
    }


    private fun writeExerciseFilesAugmentFlowQuestion(fileNameBase: String,
                                                      graph: Graph,
                                                      flowMap: Map<Edge,Double>,
                                                      augmentingPath: List<Vertex>,
                                                      capacity: Double,
                                                      newFlowValue: Double) {

        val allGraphEdges = graph.edgeSet().toMutableList()
        lateinit var saturatedEdge : Edge
        lateinit var zeroFlowEdge : Edge
        lateinit var nonZeroNonSaturated : Edge
        try {
            saturatedEdge = allGraphEdges.filter { it.weight == flowMap[it] }.random()
            zeroFlowEdge = allGraphEdges.filter { flowMap[it]!! == 0.0 }.random()
            nonZeroNonSaturated = allGraphEdges.filter { it.weight != flowMap[it] && flowMap[it]!! != 0.0}.random()
        } catch (e: NoSuchElementException) {
            return
        }
        val randomEdges = listOf(saturatedEdge, zeroFlowEdge, nonZeroNonSaturated)
        val residualGraphQuestionColumnNames = mutableListOf<String>()
        val residualGraphQuestionTableLine = mutableMapOf<String, String>()
        for (i in randomEdges.indices) {
            val edge = randomEdges[i]
            val edgeString = "(${edge.src},${edge.tgt})"
            val inverseEdgeString = "(${edge.tgt},${edge.src})"
            residualGraphQuestionColumnNames.add(edgeString)
            residualGraphQuestionTableLine[edgeString] = (edge.weight - flowMap[edge]!!).toString()
            residualGraphQuestionColumnNames.add(inverseEdgeString)
            residualGraphQuestionTableLine[inverseEdgeString] = flowMap[edge].toString()
        }

        writeStandaloneLatexFlowGraph("$fileNameBase.tex", graph, flowMap)
        val sTable = StringBuilder()
        ClozeFormatter.clozeQuestionTable(sTable, residualGraphQuestionColumnNames, listOf(residualGraphQuestionTableLine), pointsResidualGraphWeight)
        val s = StringBuilder()
        s.append(
            """
<p>Vous devez augmenter ce flot.</p>

IMAGE DU GRAPHE

<h4>Construction du graphe de flot résiduel</h4>
<p>Donner le poids de l'arête dans le graphe de flot résiduel pour les six arêtes ci-dessous.</p>
${sTable}
<h4>Chaine améliorante</h4>
<p>Donner une chaine améliorante dans le graphe de flot résiduel.
<i>La chaine doit être donnée sous la forme <b>[O,X,Y,Z,P]</b> 
càd les sommets séparés par des virgules, sans espace, entre crochets. 
<b>Respectez les majuscules et minuscules.</b></i></p>
<p>Chaine améliorante: ${ClozeFormatter.clozeQuestion(augmentingPath.joinToString(prefix="[", postfix="]", separator=","), pointsAugmentingPath)}</p>
<p>Quelle est la capacité résiduelle de cette chaine améliorante ? ${ClozeFormatter.clozeQuestion(capacity.toString(), pointsAugmentingPathCapacity)}</p>
<p>Quelle serait la valeur du flot si on l'améliore le long de cette chaine ? ${ClozeFormatter.clozeQuestion(newFlowValue.toString(), pointsNewFlowValue)}</p>
"""
        )
        File("$fileNameBase.cloze").writeText(s.toString())
    }


    companion object {
        /** Generates a transportation network with a flow.
         * The network has vertices O,P,A,B,C,D,E,F with
         * two "straight" paths [O,A,C,E,P] and [O,B,D,F,P]
         * and four crossing edges between elements of these paths.
         * The flow is considered interesting if the minimum cut is easily seen and
         * at least two crossing edges have non-null flow.
         */
        private fun generateInterestingMaximumFlowGraph(): MaximumFlowResult {
            do {
                // Generate the transportation network
                val graph = Graphs.graphFromNodeSet(GrappType.diwgraph, nodes)
                for (e in fixedEdges)
                    Graphs.addEdge(graph, e[0].toString(), e[1].toString(), randomWeight("bigger"))
                val crossingEdgesCandidates = crossingEdgesEnds.toMutableSet()
                for (i in 1..4) {
                    val d = crossingEdgesCandidates.random()
                    crossingEdgesCandidates.remove(d)
                    val extremes =
                        if (d.length == 2) d
                        else if (randomDirection()) d.substring(0..1) else d.substring(2..3)
                    val (src, tgt) =
                        if (randomDirection()) Pair(extremes[0], extremes[1])
                        else Pair(extremes[1], extremes[0])
                    Graphs.addEdge(graph, src.toString(), tgt.toString(), randomWeight("smaller"))
                }
                graph.setName("tn")
                // Compute maximum flow and decide whether the graph is interesting
                val result = AlgorithmsUtil.runMaximumFlowAlgorithm(
                    MaximumFlowAlgorithmName.FordFulkerson,
                    graph, "O", "P"
                )
                if (!interestingSourcePartitions.contains(result.sourcePartition))
                    continue
                val crossingEdgesWithNonZeroFlow = mutableSetOf<Edge>()
                for (extremities in listOf("AB", "CD", "EF", "BC", "AD", "CF", "DE")) {
                    var edge = result.graph.getEdge(extremities[0].toString(), extremities[1].toString())
                    if (edge == null)
                        edge = result.graph.getEdge(extremities[0].toString(), extremities[1].toString())
                    if (edge != null && result.flowMap[edge] != 0.0)
                        crossingEdgesWithNonZeroFlow.add(edge)
                }
                if (crossingEdgesWithNonZeroFlow.size >= 2)
                    return result
            } while (true)
        }

        private val nodes = setOf("O", "P", "A", "B", "C", "D", "E", "F")
        private val fixedEdges = setOf("OA", "OB", "AC", "BD", "CE", "DF", "EP", "FP")
        private val crossingEdgesEnds = setOf("AB", "CD", "EF", "ADBC", "CFDE")
        private val interestingSourcePartitions = setOf(
            setOf("O", "A", "B"),
            setOf("O", "A", "B", "C"),
            setOf("O", "A", "B", "D"),
            setOf("O", "A", "B", "C", "D")
        )
        private val upperPath = "BDF"
        private val lowerPath = "ACE"

        private val randomWeight: (String) -> Double = { edgeKind -> // "bigger" or "smaller"
            when (edgeKind) {
                "smaller" -> Random.nextInt(2, 18).toDouble()
                "bigger" -> Random.nextInt(8, 25).toDouble()
                else -> Random.nextInt(2, 25).toDouble()
            }
        }
        private val randomDirection: () -> Boolean = { Random.nextBoolean() }

        /** Drawing instructions for the transportation network */
        private val O_ACE_BDF_P_drawInstructions = mutableMapOf<String, String>().apply {
            this["O"] = "-1,0"
            this["P"] = "3,0"
            for (c in 'A'..'F') {
                val rank = c - 'A'
                this["$c"] = "${rank / 2},${rank % 2}"
            }
            val positions: Map<String, List<String>> = mapOf(
                "above" to listOf("B/D", "D/F", "C/D", "D/C"),
                "below" to listOf("O/A", "A/C", "C/E", "E/P"),
                "above left" to listOf("O/B"),
                "above right" to listOf("F/P"),
                "right" to listOf("C/F", "F/C"),
                "left, near start" to listOf("A/B", "D/A", "C/B"),
                "left, near end" to listOf("B/A", "A/D", "B/C"),
                "right, near start" to listOf("E/F", "D/E"),
                "right, near end" to listOf("F/E", "E/D")
            )
            for ((pos, edges) in positions) {
                for (edge in edges) {
                    this[edge] = ";$pos"
                }
            }
        }

        /**  Finds a chain from source to sink which flow will be reduced.
         * The chain
         * - follows the upper (resp. lower) path,
         * - then takes uses a crossing edge with 0 flow in the opposite direction,
         * - finally follows the lower (resp. upper) path
         * @return the set of edges in the right direction along the path, the crossing edge, and the augmenting path as a list
         * @throws NoSuchElementException if no suitable chain
         */
        fun chainToReduce(graph: Graph, flowMap: Map<Edge, Double>): Triple<MutableSet<Edge>, Edge, List<Vertex>> {

            val crossingEdgeWithZeroFlow = graph.edgeSet()
                .filter { !fixedEdges.contains("${it.src}${it.tgt}") }
                .filter { flowMap[it]!! == 0.0 }
                .random()  // NoSuchElementException possible but unlikely

            val indexSrcUpper = upperPath.indexOf(crossingEdgeWithZeroFlow.src[0])
            val indexSrcLower = lowerPath.indexOf(crossingEdgeWithZeroFlow.src[0])
            val indexTgtUpper = upperPath.indexOf(crossingEdgeWithZeroFlow.tgt[0])
            val indexTgtLower = lowerPath.indexOf(crossingEdgeWithZeroFlow.tgt[0])
            val downThenUp = indexTgtLower != -1
            val stringPath =
                if (downThenUp)
                /* Down then up */
                    lowerPath.substring(0..indexTgtLower) + upperPath.substring(indexSrcUpper..upperPath.lastIndex)
                else
                /* Up then down */
                    upperPath.substring(0..indexTgtUpper) + lowerPath.substring(indexSrcLower..lowerPath.lastIndex)

            val chain = "O${stringPath}P".map { it.toString() }
            val otherPathEdges = mutableSetOf<Edge>()
            for (i in 0 until chain.size-1)
                graph.getEdge(chain[i], chain[i+1])?.let {
                    otherPathEdges.add(it)
                }
            return Triple(otherPathEdges, crossingEdgeWithZeroFlow, chain)
        }

        fun writeStandaloneLatexFlowGraph (fileName: String, graph: Graph, flowMap: Map<Edge, Double>, ) {
            val flowGraphLabel : (Edge) -> String = { edge ->
                "[${Util.weightToString(flowMap[edge]!!)}\$\\mid\$${Util.weightToString(edge.weight)}]"
            }
            File(fileName).writeText(GraphOnGridTikzFormatter.standaloneTikzPicture(graph, O_ACE_BDF_P_drawInstructions, flowGraphLabel))
        }

        /** Computes a reduced flow along the given path.
         * The value subtracted from the flow is chosen randomly.
         * Returns the reduced flow and the value by which it was reduced. */
        fun reduceFlow (pathEdges: Set<Edge>,
                        crossingEdge: Edge,
                        originalFlowMap: Map<Edge, Double>) : Pair<MutableMap<Edge, Double>, Double> {
            val maxPossibleReduce = min(
                crossingEdge.weight,
                pathEdges.minOf { originalFlowMap[it]!! }
            )
            val reduceValue = Random.nextInt(1 .. maxPossibleReduce.toInt()).toDouble()

            val reducedFlowMap = originalFlowMap.toMutableMap()
            reducedFlowMap[crossingEdge] = reduceValue
            for (edge in pathEdges)
                reducedFlowMap[edge] = reducedFlowMap[edge]!! - reduceValue
            return Pair(reducedFlowMap, reduceValue)
        }

        fun residualFlowGraph (graph: DirectedWeightedGraph, flowMap: Map<Edge, Double>) : Graph {
            val result = Graphs.graphFromNodeSet(GrappType.digraph, graph.vertexSet())
            for ((edge, flow) in flowMap) {
                val capacity = edge.weight
                if (flow > 0.0)
                    Graphs.addEdge(result, edge.tgt, edge.src)
                if (capacity - flow > 0.0)
                    Graphs.addEdge(result, edge.src, edge.tgt)
            }
            return result
        }

        /** Returns a graph with a flow in which hopefully the given path is a unique augmenting path.
         * The result is obtained from the parameters by reducing some edge's capacities in order to saturate these edges. */
        fun removeAugmentingPaths (originalGraph: Graph,
                                   originalFlowMap: Map<Edge, Double>,
                                   pathEdges: Set<Edge>) : Pair<Graph, Map<Edge, Double>> {

            val edgesToSaturate = mutableListOf<Edge>()
            val oa = originalGraph.getEdge("O", "A")
            val ob = originalGraph.getEdge("O", "B")
            val ep = originalGraph.getEdge("E", "P")
            val fp = originalGraph.getEdge("F", "P")

            edgesToSaturate.add(if (pathEdges.contains(oa)) ob else oa)
            edgesToSaturate.add(if (pathEdges.contains(ep)) fp else ep)

            val resultGraph = Graphs.graphFromNodeSet(GrappType.diwgraph, originalGraph.vertexSet())
            val resultFlowMap = mutableMapOf<Edge, Double>()

            for (edge in originalGraph.edgeSet()) {
                val edgeWeight = if (edge in edgesToSaturate) originalFlowMap[edge] else edge.weight
                val resultEdge = Graphs.addEdge(resultGraph, edge.src, edge.tgt, edgeWeight!!)
                resultFlowMap[resultEdge] = originalFlowMap[edge]!!
            }
            return Pair(resultGraph, resultFlowMap)
        }

        /** Checks whether the given path is the unique augmenting chain in the graph. */
        fun isUniqueAugmentingPath (graph: Graph,
                                    flowMap: Map<Edge, Double>,
                                    pathEdges: Set<Edge>,
                                    crossingEdge: Edge) : Boolean {
            val residualFlowGraph = residualFlowGraph(graph as DirectedWeightedGraph, flowMap)
            val allSourceSinkPaths = AllDirectedPaths(residualFlowGraph).getAllPaths("O", "P", true, null)
            if (allSourceSinkPaths.size != 1)
                return false
            val singlePathEdges = allSourceSinkPaths[0].edgeList.map{
                graph.getEdge(it.src, it.tgt) ?: graph.getEdge(it.tgt, it.src)
            }.toSet()
            val expectedPathEdges = (pathEdges.map { graph.getEdge(it.src, it.tgt) } + graph.getEdge(crossingEdge.src, crossingEdge.tgt)).toSet()
            return singlePathEdges == expectedPathEdges
        }
    }

}