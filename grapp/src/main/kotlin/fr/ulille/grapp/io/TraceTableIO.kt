/*
 * Copyright (c) 2022 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import de.m3y.kformat.Table
import de.m3y.kformat.table
import fr.ulille.grapp.algorithms.traces.ExecutionTrace

object TraceTableIO {

    fun toTableString (et: ExecutionTrace) : String {
        val s = StringBuilder()
        val lines = ExecutionTrace.cleanLines(et.getTableLines())

        table {
            header(*et.getColumnNames().toTypedArray())
            for (line in lines) {
                row(*et.getColumnNames().map { line[it].toString() }.toTypedArray())
            }
            hints {
                borderStyle = Table.BorderStyle.SINGLE_LINE
                for (columnIndex in et.getColumnNames().indices)
                    alignment(columnIndex, Table.Hints.Alignment.LEFT)
            }
        }.render(s)
        return s.toString()
    }

}

