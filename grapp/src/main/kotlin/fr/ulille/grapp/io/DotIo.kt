/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import fr.ulille.grapp.graph.Edge
import fr.ulille.grapp.graph.Graph
import fr.ulille.grapp.graph.Vertex
import fr.ulille.grapp.ui.*
import org.jgrapht.nio.Attribute
import org.jgrapht.nio.AttributeType
import org.jgrapht.nio.DefaultAttribute
import org.jgrapht.nio.dot.DOTExporter
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.math.absoluteValue
import kotlin.random.Random

class DotIo (val graph: Graph, val result: Result? = null) {

    companion object {
        private const val HIGHLIGHT_WIDTH = 3.0
        private const val NORMAL_WIDTH = 1.0
    }

    private val importantEdges = mutableListOf<Edge>()
    private val importantNodes = mutableListOf<Vertex>()
    private val alternativeEdgeLabels = mutableMapOf<Edge, String>()

    init {
        if (result != null) {
            when (result) {
                is OptimalPathResult -> importantEdges.addAll(result.tree.edgeSet())
                is TraversalResult -> importantEdges.addAll(result.tree.edgeSet())
                is MaximumFlowResult -> alternativeEdgeLabels.putAll(result.flowMap.mapValues { (e,v) -> "[${Util.weightToString(v)}|${Util.weightToString(e.weight)}]" })
                is PathResult -> {
                    if (result.pathEdges != null) importantEdges.addAll(result.pathEdges)
                    if (result.pathNodes != null) importantNodes.addAll(result.pathNodes)
                }
                else -> throw IllegalStateException("No such result type : ${this.javaClass}")
            }
        }
    }

    private val defaultGraphIdProvider : (Graph) -> String = {
        _ ->
            var name = graph.getName().filter{ c -> c in 'a'..'z' || c in 'A'..'Z' || c.isDigit()}
            if (name.isEmpty())
                name = "graph${Random.nextInt().absoluteValue}"
            name
    }
    private val defaultGraphAttributeMap = mapOf("rankdir" to DefaultAttribute("LR", AttributeType.STRING))
    private val defaultVertexIdProvider :  (Vertex) -> String = {
            v -> v
    }
    private val defaultVertexAttributeProvider : (Vertex) -> Map<String, Attribute> =
        {
            v -> let {
            val resultMap = mutableMapOf<String,Attribute>(
                "shape" to DefaultAttribute("circle", AttributeType.STRING),
                "penwidth" to DefaultAttribute(
                    if (importantNodes.contains(v)) HIGHLIGHT_WIDTH else NORMAL_WIDTH,
                    AttributeType.DOUBLE
            ))
            resultMap
        }
        }
    private val defaultEdgeAttributeProvider : (Edge) -> Map<String, Attribute> =
        { e -> let {
            val resultMap = mutableMapOf<String, Attribute>(
                "penwidth" to DefaultAttribute(
                    if (importantEdges.contains(e)
                        || !graph.isDirected() && importantEdges.contains(Edge(e.tgt, e.src, e.weight))
                    )
                        HIGHLIGHT_WIDTH
                    else
                        NORMAL_WIDTH,
                    AttributeType.DOUBLE
                )
            )
            if (graph.isWeighted())
                resultMap["label"] = DefaultAttribute(alternativeEdgeLabels[e] ?: Util.weightToString(e.weight), AttributeType.STRING)
            resultMap
        }
        }


    fun serialize (fileName: String) {
        val exp = DOTExporter<Vertex, Edge>()
        exp.setGraphIdProvider{ defaultGraphIdProvider(graph) }
        exp.setGraphAttributeProvider{ defaultGraphAttributeMap }
        exp.setVertexIdProvider(defaultVertexIdProvider)
        exp.setVertexAttributeProvider(defaultVertexAttributeProvider)
        exp.setEdgeAttributeProvider(defaultEdgeAttributeProvider)

        exp.exportGraph(graph, Files.newBufferedWriter(Paths.get(fileName)))
    }



}