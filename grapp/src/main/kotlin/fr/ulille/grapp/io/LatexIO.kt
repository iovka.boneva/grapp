/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import fr.ulille.grapp.graph.Edge
import fr.ulille.grapp.graph.Graph
import java.lang.StringBuilder

fun serializeToTikz2(g: Graph, o: StringBuilder) {
    LatexSerializer.serializeToTikz(g, o, false, LatexSerializer.defaultEdgeLabel(false));
}


object LatexSerializer {

    val defaultEdgeLabel : (Boolean) -> (Edge) -> String = { isWeightedGraph ->
        { edge -> if (isWeightedGraph) "${Util.weightToString(edge.weight)}" else "" }
    }


    fun serializeToTikz(g: Graph,
                        o: StringBuilder,
                        vMacroForNodes: Boolean = true,
                        edgeLabel: (Edge) -> String = defaultEdgeLabel(g.isWeighted())) {
        val TIKZ_OPTIONS = "thick,auto,node/.style={draw,circle},scale=2${if(g.isDirected()) ",->,>=stealth'" else ""}"

        val d = g.getDrawInstructions() ?: throw YAMLFormatError("Drawing instructions required in input graph")

        o.appendLine("\\begin{tikzpicture}[$TIKZ_OPTIONS]")
        for (node in g.vertexSet()) {
            val nodeName = if (node.length == 1 && vMacroForNodes) "\\v$node" else "$node"
            d[node] ?: throw YAMLFormatError("Drawing instructions required for node $node")
            o.appendLine("\\node[node] ($node) at (${d[node]}) {$nodeName};")
        }
        o.appendLine()
        o.appendLine("\\path")
        for (edge in g.edgeSet()) {
            val label = edgeLabel(edge)
            val di = d["${edge.src}/${edge.tgt}"]
            var edgeInstructions = ""
            var labelInstructions = ""
            if (di != null) {
                val parts = di.split(";")
                if (parts[0].isNotEmpty()) edgeInstructions = "[${parts[0]}]"
                if (parts[1].isNotEmpty()) labelInstructions = "[${parts[1]}]"
            }
            o.appendLine("(${edge.src}) edge$edgeInstructions node$labelInstructions {$label} (${edge.tgt})")
        }
        o.appendLine(";")
        o.appendLine("\\end{tikzpicture}")
    }

    fun serializeToFigure (g: Graph, o: StringBuilder) {
        serializeToTikz(g, o)
        o.appendLine("\\hspace{1cm}")
        o.appendLine("\\begin{minipage}{0.3\\linewidth}")
        o.appendLine("\\ifyaml")
        o.appendLine("\\begin{verbatim}")
        YamlSerializer.serialize(g, o, omitNameAndDescription = true)
        o.appendLine("\\end{verbatim}")
        o.appendLine("\\else{}\\fi")
        o.appendLine("\\end{minipage}")
    }
}