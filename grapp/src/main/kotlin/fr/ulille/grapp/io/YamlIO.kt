/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import fr.ulille.grapp.graph.*
import java.io.InputStream
import java.lang.IllegalArgumentException

class YamlGraph {

    var name: String? = null
    var descr: String = ""
    var type: String? = null
    var nodes: List<String>? = null
    var edges: Map<String, List<String>>? = null
    var wedges: Map<String, Map<String, Double>>? = null
    var drawInstructions: Map<String, String>? = null

    override fun toString(): String {
        return "name: $name, type: $type, descr: $descr, nodes: $nodes edges: $edges drawInstructions: $drawInstructions"
    }

    companion object {
        const val name = "name"
        const val descr = "descr"
        const val type = "type"
        const val nodes = "nodes"
        const val edges = "edges"
        const val wedges = "wedges"
        const val drawInstructions = "drawInstructions"
        val graphNodesRegex = Regex("[a-zA-Z][a-zA-Z\\d_]*")
        val graphNodesRegexMessage = "Un nom de sommet doit commencer par une lettre et contenir uniquement des lettres, des chiffres et _."

        val missingNameMessage = "Le graphe n'a pas de nom (manque name:)."
        val missingTypeMessage = "Le graphe n'a pas de type (manque type:)."
        val missingNodesMessage = "Le graphe n'a pas de sommets (manque nodes:)."
        val missingEdgesMessage = "Le graphe n'a pas d'arêtes (manque edges:)."
        val missingWedgesMessage = "Le graphe valué n'a pas d'arêtes (manque wedges:)."
        val inexistantGrappTypeMessage = "Type de graphe inconnu. Les types de graphes autorisés sont ${fr.ulille.grapp.graph.GrappType.allowedStrings.joinToString()}"
        val forbiddenEdgesMessage = "Arêtes non valuées interdites pour graphe valué (edges: interdit)."
        val forbiddenWedgesMessage = "Arêtes valuées interdites pour graphe non valué (wedges: interdit)."
        val differentEdgesKeysNodesSetMessage = "Différence entre l'ensemble de sommets (nodes:) et les sommets utilisés pour la définition des arêtes (edges:)"
        val unknownNodeMessage = "Sommet inconnu dans une liste de successeurs"
        val unknownPropertyMessage = "Propriété inconnue:"
        val incorrectValueTypeMessage = "Valeur inattendue"
        val forbiddenNodeNameMessage = "Nom de sommet non autorisé"
        val yamlSyntaxErrorMessage = "Erreur de syntaxe"
        val unidentifiedSyntaxErrorMessage = yamlSyntaxErrorMessage + " non identifiée. Vérifier le respect de la syntaxe YAML."
    }
}

object YamlParser {

    fun parse (input: InputStream) : Graph {
        val mapper = ObjectMapper(YAMLFactory())
        var yamlGraph: YamlGraph?
        try {
            yamlGraph = mapper.readValue(input, YamlGraph::class.java)
        } catch (e: com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException) {
            throw YAMLFormatError(YamlGraph.unknownPropertyMessage + " ${e.propertyName},", "ligne ${e.location.lineNr}")
        } catch (e: com.fasterxml.jackson.databind.exc.MismatchedInputException) {
            throw YAMLFormatError(YamlGraph.incorrectValueTypeMessage, "ligne ${e.location.lineNr}")
        } catch (e: com.fasterxml.jackson.core.JsonProcessingException) {
            throw YAMLFormatError(YamlGraph.yamlSyntaxErrorMessage, "\nCausé par : ${e.location} ${e.cause}")
        }

        try {
            return GraphFromYamlGraph.convert(yamlGraph)
        } catch (e: YAMLFormatError) {
            throw e
        } catch (e: Exception) {
            throw YAMLFormatError(YamlGraph.unidentifiedSyntaxErrorMessage)
        } finally {
            input.close()
        }
    }
}

object YamlSerializer {
    fun serialize (g: Graph, o: StringBuilder, omitNameAndDescription : Boolean = false){
        if (! omitNameAndDescription) o.appendLine("${YamlGraph.name}: ${g.getName()}")
        if (! omitNameAndDescription && g.getDescr() != "")
            o.appendLine("${YamlGraph.descr}: ${g.getDescr()}")
        o.appendLine("${YamlGraph.type}: ${g.getGrappType()}")
        o.appendLine("${YamlGraph.nodes}: ${g.vertexSet().joinToString(separator=", ", prefix="[",postfix="]")}")
        val edgesKey = if (g.isWeighted()) "${YamlGraph.wedges}:" else "${YamlGraph.edges}:"
        if (g.vertexSet().isEmpty())
            o.appendLine("$edgesKey {}")
        else {
            o.appendLine("$edgesKey")
            for (node in g.vertexSet()) {
                o.appendLine("  $node: ${nodeTargets(g, node)}")
            }
        }
    }

    private fun nodeTargets (g: Graph, node: String) : String {
        return if (g.isWeighted())
            g.outgoingEdgesOf(node)
                .filter {it.src == node}
                .joinToString(separator=", ", prefix="{", postfix="}") { "${it.tgt}: ${Util.weightToString(it.weight)}" }
        else
            g.outgoingEdgesOf(node)
                .filter {it.src == node}
                .joinToString(separator=", ", prefix="[", postfix="]") { it.tgt  }
    }
}

object GraphFromYamlGraph {

    fun convert (yg: YamlGraph) : Graph {
        val type = try {
            GrappType.fromString(yg.type ?: throw YAMLFormatError(YamlGraph.missingTypeMessage))
        } catch (e: IllegalArgumentException) {
            throw YAMLFormatError(YamlGraph.inexistantGrappTypeMessage)
        }
        if (yg.name == null)
            throw YAMLFormatError(YamlGraph.missingNameMessage)
        if (yg.nodes == null)
            throw YAMLFormatError(YamlGraph.missingNodesMessage)
        if (type.isWeighted() && yg.wedges == null)
            throw YAMLFormatError(YamlGraph.missingWedgesMessage)
        if (!type.isWeighted() && yg.edges == null)
            throw YAMLFormatError(YamlGraph.missingEdgesMessage)
        if (type.isWeighted() && yg.edges != null)
            throw YAMLFormatError(YamlGraph.forbiddenEdgesMessage)
        if (!type.isWeighted() && yg.wedges != null)
            throw YAMLFormatError(YamlGraph.forbiddenWedgesMessage)

        val result = when (type) {
            GrappType.ugraph -> UndirectedGraph()
            GrappType.digraph -> DirectedGraph()
            GrappType.uwgraph -> UndirectedWeightedGraph()
            GrappType.diwgraph -> DirectedWeightedGraph()
        } as Graph

        result.setName(yg.name!!)
        result.setDescr(yg.descr)
        initNodes(result, yg.nodes!!)
        if (result.isWeighted())
            initWedges(result, yg.wedges!!, yg.nodes!!)
        else
            initEdges(result, yg.edges!!, yg.nodes!!)

        if (yg.drawInstructions != null)
            result.setDrawInstructions(yg.drawInstructions!!)

        return result
    }

    private fun initNodes(graph: Graph, nodes: List<String>) {
        for (v in nodes) {
            if (! v.matches(YamlGraph.graphNodesRegex))
                throw YAMLFormatError("${YamlGraph.forbiddenNodeNameMessage} ($v). ${YamlGraph.graphNodesRegexMessage}")
            graph.addVertex(v)
        }
    }

    private fun initWedges (graph: Graph, edges: Map<String, Map<String, Double>>, vertexOrder: List<Vertex>) {
        if (graph.vertexSet() != edges.keys)
            throw YAMLFormatError(YamlGraph.differentEdgesKeysNodesSetMessage)
        for ((src, successors) in edges) {
            for (tgt in orderedSuccessors(successors.keys, vertexOrder)) {
                val weight = successors[tgt]!!
                if (!graph.containsVertex(tgt))
                    throw YAMLFormatError(YamlGraph.unknownNodeMessage, "sommet $tgt successeur de $src")
                Graphs.addEdge(graph, src, tgt, weight)
            }
        }
    }

    private fun initEdges (graph: Graph, edges: Map<String, List<String>>, vertexOrder: List<Vertex>) {
        if (graph.vertexSet() != edges.keys)
            throw YAMLFormatError(YamlGraph.differentEdgesKeysNodesSetMessage)
        for ((src, successors) in edges) {
            for (tgt in orderedSuccessors(successors, vertexOrder)) {
                if (!graph.containsVertex(tgt))
                    throw YAMLFormatError(YamlGraph.unknownNodeMessage, "sommet $tgt successeur de $src")
                Graphs.addEdge(graph, src, tgt)
            }
        }
    }

    private fun orderedSuccessors (successors : Collection<Vertex>, vertexOrder: List<Vertex>) : List<Vertex> {
        return successors.sortedBy { vertexOrder.indexOf(it) }
    }
}

