/*
 * Copyright (c) 2021 Iovka Boneva, Université de Lille
 *
 * This file is part of Grapp.
 *
 * Grapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Grapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Grapp.  If not, see <https://www.gnu.org/licenses/>
 */

package fr.ulille.grapp.io

import fr.ulille.grapp.graph.Edge

object Util {
    fun weightToString(w : Double) : String {
        return (if (w.mod(1.0) == 0.0) w.toInt() else w).toString()
    }
    val edgeToStringNoWeight : (Edge) -> String = {
        "(${it.src}, ${it.tgt})"
    }
}

class ImplicitGraphFormatError(message: String, context: String ="")
    : RuntimeException("Format incorrect: $message ${if (context != "") "$context" else ""}")

class YAMLFormatError(message: String, context: String = "")
    : RuntimeException("Format incorrect: $message ${if (context != "") "$context" else ""}")

