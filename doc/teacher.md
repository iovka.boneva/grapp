Exemple d'utilisation de Grapp comme outil pédagogique d'introduction aux graphes
===

Grapp met l'accent sur l'utilisation d'algorithmes sur les graphes (et non sur l'étude des algorithmes et leurs propriétés).
Le cas d'utilisation typique est pour des exercices demandant à modésiler  des problèmes réels ou inspirés du réel en utilisant les graphes.

Voici un exercice que nous utilisons depuis plusieurs années. 
Il requiert plusieurs calculs de plus court chemin, que jusque présent les étudiant·es devaient faire à la main. 
Ce calcul prend du temps et détourne l'attention du vrai intérêt de l'exercice qui est de réfléchir comment les distances entre les couples de sommets dans le graphe peuvent être utilisée pour résoudre les problèms d'optimisation posés.

### Exercice

Le graphe ci-dessous représente les villes a, b, c, ... j, et pour chaque couple de villes x et y reliées par une route directe, le temps typique de parcours entre x et y, en heures.

![Graphe avec sommets a,b,c, ..., j](images/livraison.png)

Une entreprise de vente à distance voudrait implanter son entrepot dans une de ces villes.

1. Quelle ville devrait-elle choisir pour priviligier le critère de rapidité de la livraison ?
2. Quelle ville devrait-elle choisir pour minimiser le cout de transport ? On suppose pour cela que la population des villes est comme dans le tableau ci-dessous (en centaines de milliers) et que le nombre de commandes des différentes villes est proportionnel au nombre d'habitants.

| a   | b   | c   | d   | e   | f   | g   | h   | i   | j   |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 2   | 3   | 2   | 7   | 1   | 1   | 10  | 5   | 2   | 3   |


### Résoudre l'exercice à l'aide de Grapp

Ne connaissant pas d'algorithme de calcul de plus court chemin entre toutes les paires de sommets (tel que Floyd-Warshall), les étudiant·es dovient utiliser l'algorithme de Dijkstra. 
Il faut donc dérouler l'algorithme de Dijkstra 10 fois, une fois par sommet de départ.

On fabrique ensuite un tableau de dix lignes et dix colonnes indexées par les sommets du graphe, où la case (x,y) donne la longueur du plus court chemin entre les sommets x et y.
C'est en exploitant ce tableau qu'on pourra répondre aux questions posées, en choisissant le bon critère pour "rapidité de livraison" et le bon critère pour "cout de transport".

L'apport de Grapp est de permettre de remplir plus facilement la tableau et se concentrer sur les questions de modélisation à proprement parler.

### Définition du graphe 

Voici le contenu du fichier livraison.yaml

```
type: uwgraph
name: livraison
descr: "Villes et réseau routier"
nodes: [a,b,c,d,e,f,g,h,i,j]
wedges:
  a: {b: 11, d: 2}
  b: {c: 4, d: 7, e: 2, g: 3}
  c: {}
  d: {e: 3, j: 2}
  e: {f: 4, j: 4}
  f: {g: 2, i: 7}
  g: {h: 10}
  h: {}
  i: {}
  j: {}
```

Autres fonctionnalités pour enseignants
===

L'outil permet également de 

- générer du code latex au format tikz à partir de la définition yaml du graphe (documentation à venir)
- générer des tableaux avec les traces d'exécution de certains algorithmes (pas intégré à l'interface)
