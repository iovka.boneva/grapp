GrappYAML : Format pour représentation textuelle de graphes
===

On supporte quatre types de graphes.
Le type du graphe doit être spécifié par une de ces valeurs:

- `digraph` : graphe orienté (**di**rected graph)
- `ugraph` : graphe non orienté (**u**ndirected graph)
- `diwgraph` ou `wdigraph` : graphe orient (**di**rected) et valué (**w**eighted)
- `uwgraph` ou `wugraph` : graphe non orienté (**u**ndirected) et valué (**w**eighted)

Les graphes sont donnés par leur ensemble de sommets, et pour chaque sommet sa liste de successeurs.
De plus, chaque graphe doit avoir un nom, et éventuellement une description.

### Graphe orienté non valué (`digraph`)

Voici en exemple le graphe orienté et non valué `G1` et sa représentation textuelle.

![Graphe G1](images/G1.png)

```yaml
type: digraph
name: G1
nodes: [a, b, c, d]
edges:
  a: [b, c]
  b: [c]
  c: [d, a]
  d: []
```

On doit spécifier:

- `type:` le type de graphe, obligatoire. Les valeurs valides sont celles énumérées plus haut.
- `name:` un nom pour le graphe, obligatoire. C'est une chaine de caractères sans espaces.
- `nodes:` l'ensemble des sommets du graphe, obligatoire. Chaque sommet est une chaine de caractères sans espaces, qui commence par un caractère alphabétique. Les sommets sont énumérés entre crochets, séparés par des virgules.
- `edges:` les arêtes par liste d'adjacence, obligatoire. On doit énumérer tous les sommets, et pour chaque sommet on énumère ses successeurs entre crochets.

Éléments de syntaxe importants:

- les deux points collés après chaque mot clé
- l'espace après les deux points
- l'*indentation* pour l'énumération des sommets après `edges:`
- les ensembles (de tous les sommets, des sommets successeurs) sont donnés entre crochets
- on doit énumérer tous les sommets après `edges:`. Si un sommet n'a pas de successeurs, on l'indique par l'ensemble vide `[]`

### Graphe non orienté non valué (`ugraph`)

Se définit de la même manière qu'un graphe orienté non valué, mais les arêtes seront interprétées comme non orientées.

Voici le graphe non orienté et valué `G2` et sa représentation textuelle. 

![Graphe G2](images/G2.png)

```yaml
type: ugraph
name: G2
nodes: [a, b, c, d]
edges:
  a: [b, c]
  b: [c]
  c: [d, a]
  d: []
```

Remarquez que la représentation textuelle est la même que celle de `G1`, à part pour le type du graphe.

Remarquez également qu'il n'est pas interdit de donner une arête dans les deux sens. 
Par exemple l'arête entre `a` et `c` est donnée dans la liste des successeurs de `a` et dans la liste des successeurs de `c`. 

### Graphe valué (`uwgraph` ou `diwgraph`)


Pour les arêtes, en plus des successeurs il faut donner le poids pour chaque arête. 
De ce fait la liste des successeurs est donnée par une liste d'associations (entre *accolades*), et utilise un mot clé différent: **wedges:**

Voici le graphe orienté et valué `G3` et sa représentation textuelle.

![Graphe G3](images/G3.png)

```yaml
type: diwgraph
name: G3
nodes: [a, b, c, d]
wedges:
  a: {b: 1, c: 2}
  b: {c: -3}
  c: {d: 4, a: 5}
  d: {}
```

Remarquez que:

- Les successeurs d'un sommet sont donnés par une liste d'associations entre *accolades*. À chaque successeur on associe le poids de l'arête.
- Le mot clé **wedges** pour introduire les arêtes.
- Dans la liste d'associations, le nom du sommet est immédiatement suivi de deux points, puis d'un espace, avant de donner le poids de l'arête.

Naturellement, un graphe non orienté et valué est représenté de manière similaire, mais en utilisant le type `uwgraph`.


Éléments de YAML
---

Voici quelques informations sur la partie de YAML utilisée pour représenter les graphes, et qui pourraient aider à comprendre les contraintes syntaxiques liées à la représentation des graphes.

YAML permet de représenter des associations de clé-valeur. 
Chaque ligne contient une clé suivie immédiatement de deux points, suivie d'une valeur pour la clé.

```yaml
# Simple yaml example. Lines starting with # are comments.
name: Alice
age: 19
friends: [Boris, Cate]
languages: {english: B2, french: C2}
skills:
  programming: confirmed
  testing: beginner
  technicalEnglish: confirmed
  projectManagement: beginner
training: BUT Informatique
```

Les valeurs peuvent être des valeurs simples comme par exemple des chaines de caractères ou des nombres. 
Dans l'exemple ci-dessus on a:

- la clé `name` avec valeur `Alice` 
- la clé `age` avec valeur `19`

Mais on peut aussi avoir des valeurs qui sont des listes, voire des listes d'associations.
Par exemple, la clé `friends` a comme valeur une *liste* dont les éléments sont `Boris` and `Cate`.
Les listes simples sont délimitées par des crochets.

La clé `languages` a comme valeur une liste d'associations clé-valeur. 
Cette liste contient la clé `english` avec valeur `B2`, et la clé `french` avec valeur `A1`.
Les listes d'associations peuvent être représentées de deux manières:

- comme une liste entre accolades, comme pour `languages`
- ou avec un couple clé-valeur par ligne, comme pour `skills`.

En effet, la valeur de `skills` est également une liste d'associations, mais au lieu de la mettre sur une seule ligne entre accolades, on énumère une clé-valeur par ligne. 
Dans ce cas il faut indenter ces lignes, pour pouvoir identifier la fin de liste d'associations.
Dans l'exemple, la valeur de la clé `skills` est une liste d'associations qui contient quatre couples clé-valeur pour les clés `programming`, `testing`, `technicalEnglish` et `projectManagement`.

L'exemple ci-dessus pourrait être représenté de manière équivalente comme ceci:

```yaml
# Simple yaml example. Lines starting with # are comments.
name: Alice
age: 19
friends: [Boris, Cate]
languages: 
  english: B2
  french: C2
skills: {programming: confirmed, testing: beginner, technicalEnglish: confirmed, projectManagement: beginner}
training: BUT Informatique
```






