Grapp : Documentation utilisateur
===

Principe
---

L'outil permet de 

- charger des graphes depuis leur [description textuelle](grapp-yaml.md) ou par une [classe java](implicit-graph.md)
- exécuter des algorithmes sur les graphes et enregistrer les résultats de ces algorithmes
- visualiser les graphes et les résultats calculés

L'interaction avec l'utilisateur se fait à travers un invite de commande.
Voir plus bas comment il faut lancer le programme.

Voici un exemple d'utilisation, où on 

- charge le graphe contenu dans le fichier `livraison.yaml`
- exécute l'algorithme de Dijkstra de recherche de plus court chemin depuis le sommet `a` du graphe
- affiche la liste des graphes et résultats chargés, montrant le graphe nommé `livraison` d'identifiant `g1` et le résultat d'identifiant `r1.1` de l'exécution de l'algorithme de Dijkstra
- dessine le résultat `r1.1` ayant comme effet de produire les fichiers `dijkstra-livraison.dot` et `dijkstra-livraison.png`

```{style="background-color:lightgray"}
grapp:> grapp load livraison.yaml
Graphe chargé.
grapp:> grapp shortest-path -algorithm Dijkstra -on livraison -from a
             | a |  b |  c | d | e | f |  g |  h |  i | j
-------------|---|----|----|---|---|---|----|----|----|--
    distance | 0 | 11 | 15 | 2 | 5 | 9 | 11 | 21 | 16 | 4
prédécesseur |   |  a |  b | a | d | e |  f |  g |  f | d

grapp:> grapp list
Graphes chargés et résultats calculés:

Id   | Nom de graphe | Description graphe / résultat
-----|---------------|------------------------------
g1   | livraison     | Villes et réseau routier
r1.1 |               | Dijkstra on livraison from a

grapp:> grapp draw -result r1.1 -to dijkstra-livraison
Dessin du graphe sauvegardé.
```

Commandes
---

Les commandes disponibles sont listées ci dessous.
La commande `help` affiche la liste des commandes disponibles. 
De plus, l'option `--help` affiche l'aide détaillée de chaque commande.
Par exemple `load --help` affiche l'aide de la commande `load`.

- `help` <br>
  Donne la liste des commandes disponibles.
- `load` <br>
  Charge un graphe depuis un fichier.
- `list`  <br>
  Affiche la liste des graphes déjà chargés et des résultats déjà calculés sur ces graphes.
- `draw` <br>
  Produit un fichier image contenant le dessin d'un graphe ou d'un résultat.
- `exit` <br>
  Quitte le programme. Les graphes chargés et résultats calculés sont perdus (pas de sauvegarde de session).
- `shortest-path` <br>
  Exécute un algorithme de recherche de plus court chemin et stocke le résultat de l'algorithme (table des prédécesseurs, table des distances).
- `optimal-path` <br>
  Exécute un algorithme de recherche de chemin optimal, càd un chemin minimal ou maximal, et stocke le résultat de l'algorithme (table des prédécesseurs, table des distances).
- `traversal`<br>
  Exécute un algorithme de parcours et stocke le résultat (arêtes visitées et ordre de parcours).
- `maximum-flow` <br>
  Exécute un algorithme de recherche de flot maximal et stocke le résultat (valeur du flot et fonction de flot).
- `find-path` <br>
  Effectue une recherche de chemin dans un arbre qui est le résultat d'un parcours ou d'un plus court chemin, ou d'un chemin optimal.
- `gen-yaml` <br>
  Génère un fichier yaml qui contient une trame pour un graphe, à partir du type du graphe et de la liste de ses sommets.

### Options

Toutes les commandes exécutant un algorithme (shortest-path, optimal-path, traversal, maximum-flow) acceptent ces deux options:

- `-csv <NOM_FICHIER>` <br>
  Écrit le tableau des résultats au format CSV dans le fichier donné. L'écriture se fait en concaténation.
- `-silent` <br>
  N'affiche pas le résultat sur la console.


Lancer le programme
---

Le programme est fourni en tant qu'archive jar et des scripts pour la lancer. 
Dans la suite on utilise le script nommé `grapp`.

Le programme fonctionne en mode client-serveur, le client et le serveur s'exécutant dans le même terminal.

1. Lancer le serveur avec la commande `grapp server &` **en tâche de fond**
2. Vous pouvez maintenant exécuter les commandes décrites ci-dessus. Elles seront envoyées au serveur qui affichera les résultats.

