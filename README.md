# Grapp 

Outil pédagogique en ligne de commande permettant d'exécuter simplement des algorithmes classiques sur les graphes.

Exemple d'utilisation:

```
grapp:> load livraison.yaml
Graphe chargé.
grapp:> shortest-path -algorithm Dijkstra -on livraison -from a
             | a |  b |  c | d | e | f |  g |  h |  i | j
-------------|---|----|----|---|---|---|----|----|----|--
    distance | 0 | 11 | 15 | 2 | 5 | 9 | 11 | 21 | 16 | 4
prédécesseur |   |  a |  b | a | d | e |  f |  g |  f | d

grapp:> list
Graphes chargés et résultats calculés:

Id   | Nom de graphe | Description graphe / résultat
-----|---------------|------------------------------
g1   | livraison     | Villes et réseau routier
r1.1 |               | Dijkstra on livraison from a

grapp:> draw -result r1.1 -to dijkstra-livraison
Dessin du graphe sauvegardé.
```

Voici le contenu du fichier chargé `livraison.yaml`
```yaml
type: uwgraph
name: pcc/livraison
nodes: [a,b,c,d,e,f,g,h,i,j]
wedges:
  a: {b: 11, d: 2}
  b: {c: 4, d: 7, e: 2, g: 3}
  c: {}
  d: {e: 3, j: 2}
  e: {f: 4, j: 4}
  f: {g: 2, i: 7}
  g: {h: 10}
  h: {}
  i: {}
  j: {}
```

Ce fichier `dijkstra-livraison.png` est créé par la commande `draw` (dernière commande de l'exemple) et montre le graphe et l'arbre des plus courts chemins depuis `a`.

![Dessin du graphe](doc/images/dijkstra-livraison.png)

Grapp est prévu pour une utilisation dans le cadre des cours d'introduction aux graphes, et plus particulièrement pour faciliter la résolution d'exercices de modélisation avec les graphes.
En effet, Grapp se consentre sur le résultat d'exécution des algorithme plutôt que sur les algorithmes en tant que tels.

# Documentation

* [documentation utilisateur](doc/user.md)
* [format de définition des graphes](doc/grapp-yaml.md)
* [exemple pour enseignants](doc/teacher.md)

# Installation

Grapp tourne sur une dans un terminal.

* S'assurer que java est installé sur la machine et se trouve sur le `PATH`.
* Installer GraphViz et s'assurer qu'il se trouve sur le `PATH`. Graphviz est utile pour la commande `draw`.
* Télécharger [l'archive](binaries/Grapp.zip). Elle contient les binaires (.jar) et des scripts de lancement pour Linux et Windows.
* Lancer en utilisant le script approprié.

# Fonctionnalités à venir, signaler des bugs

Pour la liste des fonctionnalités prévues, consulter [La liste des Issues filtrées par le label Fonctionnalité](https://gitlab.univ-lille.fr/iovka.boneva/grapp/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Fonctionnalit%C3%A9)

L'ajout d'autres algorithmes est également possible:

- idéalement par un pull request (vous pouvez [me contacter](mailto:iovka.boneva@univ-lille.fr) au préalable pour en discuter)
- sinon [me contacter](mailto:iovka.boneva@univ-lille.fr)

Pour signaler un bug, [créez une Issue](https://gitlab.univ-lille.fr/iovka.boneva/grapp/-/issues).


# Licence

Grapp est distribué sous licence GNU Lesser General Public License. 
Voir [le fichier LICENSE](https://gitlab.univ-lille.fr/iovka.boneva/grapp/-/blob/dev/LICENSE) pour plus de détails.



